#!/bin/bash

# NOTE: This is very fragile and might break in ~2 months as fenics is no longer suported in the nia2022a enviroment
# (https://docs.scinet.utoronto.ca/index.php/Modules_specific_to_Niagara), 
# as such the git repository will save the ffc compiled outputs incase the whole fenics pipeline breaks
# as of now the compiled form files can still be generated using the docker image
module load intel/2019u3 intelmpi/2019u3 boost/1.69.0 hdf5/1.8.21 petsc/3.10.5 netcdf/4.6.3 trilinos/12.12.1 intelpython3/2019u3 fenics/2019.1.0