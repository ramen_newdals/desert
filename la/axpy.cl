/* Implements Dense BLAS: AXPY operation in openCL

		AXPY: (A*x + y), A->Scalar, X->Vector, y->Vector
		FOR i in X:
			z[i] = A*x[i] + y[i]
*/

__kernel void axpy(__global float* A, __global float* B, __global float* C) {
	int group_id = get_global_id(0);
	C[group_id] = A[group_id] + B[group_id];
}