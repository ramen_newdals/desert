# Set Up Proc:
## BOILER PLATE CODE
### openCL Setup
- Get the number of platforms {clGetPlatformIDs}
- For each platform get the platform info {clGetPlatformInfo()}
- Get decive IDs {clGetDeviceIDs}
- For each device on the platform get the device info {clGetDeviceInfo()}
- Select the device that is going to be used
- Create a context {clCreateContext()}
### OpenCL Destroy
- destroy all existing buffers {clEnqueueUnmapMemObject()}
- clFinish()
- release all of the existing events {clReleaseEvent()}
- Release all memory for the computation {clReleaseMemObject()}
- Release the command queue {clReleaseCommandQueue()}
- Release the kernel {clReleaseCommandQueue()}
- release the program {clReleaseProgram()}
- release the context {clReleaseContext()}

## Kernel Loading/Configuration
- Select the kernel
- Load the kernel source code into application {char* or string}
- create the kernel program from the source
	for the context {clCreateProgramWithSource()}
- Compile the kernel for the context {clBuildProgram()}
- Create the kernel for the device {clCreateKernel()}
- get the kernel work-group size {clGetWorkGroupInfo()}
- get the total local memory avalible to the device {clGetDeviceInfo()}
- get the used local memory on the device {clGetKernelWorkGroupInfo()}
- compute the local memory size {(total  local memory) - (used local memory)}
- get the max number of compute units {clGetDeviceInfo()}
- Compute kernel local and global work group sizes
	- This is specific to the data structure thats being worked on
## Load the kernel parameters
- clSetKernelArg()
## Execute the kernel
- clEnqueueNDRangeKernel()
## Copy results back to the device
- first wait for all of the kernels to finish {clWaitForEvents()}
- copy the results generated from the kernel back to the host {clEnqueueMapBuffer()}


## Setting up the device command Queue
- Create the command Queue {clCreateCommandQueueWithProperties()}

## Determine the devices parameters
- Get the prefered device allignment to give to posix_memalign() (clGetDeviceInfo)
- Check if out of order processing is supported on the decive

## Allocate memory on device
- knowing the size of the data-structure allocate it onto the device {clCreateBuffer()}
### Copy memory from host into device
- need to unmap the host buffer(s) after it has been copied into the decive {clEnqueueUnmapMemObject()}
 