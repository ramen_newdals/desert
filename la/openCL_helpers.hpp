#include <CL/cl_platform.h>
#include <cstddef>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <string>
#include <vector>
#include <filesystem>
#include <iostream>
#include <sys/stat.h>
#include <CL/cl.h>
#include <sys/types.h>

#define MEMORY_ALLOC_CHECK(_addr, _len, _addrstr) {                                                             \
   posix_memalign((void **) &(_addr), preferred_alignment, _len);                                               \
   if ((_addr) == NULL) {                                                                                       \
      printf("Failed allocation of %lld bytes for %s\n", (unsigned long long) (unsigned int) (_len), _addrstr); \
      exit (EXIT_FAILURE);                                                                                      \
   }                                                                                                            \
}

#define CHECK_RESULT(_string) {                    \
   if (rc != CL_SUCCESS) {                         \
      printf("%s failed. rc = %d\n", _string, rc); \
      exit(EXIT_FAILURE);                          \
   }                                               \
}

typedef struct device_struct {
   cl_device_id id;
   cl_device_type type;
   cl_command_queue ComQ;
   char *name;
} device_struct;

typedef struct platform_struct {
   char *name;
   cl_platform_id id;
   unsigned int num_devices;
   device_struct* device;
   cl_context context;
   cl_program program;
   cl_kernel kernel;
} platform_struct;

class compute_helper {
	public:
	compute_helper() 
	{
		/* Retrieve information about the openCL instalation that the program is
			 running under
			 INFO:
			 	- number of platforms
				- the id for each platform
				- platform name
				- number of devices
				- */
		uint i, j;
		// Get number of platforms the system supports
		rc = clGetPlatformIDs(0, (cl_platform_id*) NULL, &num_platforms);
		CHECK_RESULT("clGetPlatformIDs(num_platforms)")

		// Allocate storage to store each platforms details
		MEMORY_ALLOC_CHECK(platform, num_platforms*sizeof(platform_struct), "platform");

		// Get the id for each platform (cl_platform_id)
		cl_platform_id *temp_platform_id_array;
		MEMORY_ALLOC_CHECK(temp_platform_id_array, num_platforms*sizeof(cl_platform_id), "temp_platform_id_array");
		rc = clGetPlatformIDs(num_platforms, temp_platform_id_array, (cl_uint *) NULL);
		CHECK_RESULT("clGetPlatform IDs(Platform IDs)")
		for (i=0; i<num_platforms; i++) {
			platform[i].id = temp_platform_id_array[i];
		}
		free(temp_platform_id_array);

		// For each platform get all of the information associated with it
		for (i=0; i<num_platforms; ++i) {
			// Size of platform name
			rc = clGetPlatformInfo(platform[i].id, CL_PLATFORM_NAME, (size_t) 0, NULL, (size_t*) &param_value_size_ret);
			CHECK_RESULT("clGetPlatformInfo(size of platform name)")
			// The name of the platform
			MEMORY_ALLOC_CHECK(platform[i].name, param_value_size_ret, "platform name");
			rc = clGetPlatformInfo(platform[i].id, CL_PLATFORM_NAME, param_value_size_ret, platform[i].name, (size_t *) NULL);
			CHECK_RESULT("clGetPlatformInfo(platform name)")

			// Number of devices
			rc = clGetDeviceIDs(platform[i].id, CL_DEVICE_TYPE_ALL, 0, NULL, (cl_uint *) &(platform[i].num_devices));
			CHECK_RESULT("clGetDeviceIDs(number of devices)")
			MEMORY_ALLOC_CHECK(platform[i].device, platform[i].num_devices*sizeof(device_struct), "device structure");

			cl_device_id *tmpdevices;
			MEMORY_ALLOC_CHECK(tmpdevices, platform[i].num_devices*sizeof(cl_device_id), "tmpdevices");
			
			// Get the list of all all device ids
			rc = clGetDeviceIDs(platform[i].id, CL_DEVICE_TYPE_ALL, platform[i].num_devices, tmpdevices, NULL);
			CHECK_RESULT("clGetDeviceIDs(list of device IDs)")
			
			// Go through each device on the current platform
			for (j=0; j<platform[i].num_devices; ++j) {
				platform[i].device[j].id = tmpdevices[j];
				// Store the type of each device
				rc = clGetDeviceInfo(platform[i].device[j].id, CL_DEVICE_TYPE, sizeof(cl_device_type), &platform[i].device[j].type, NULL);
				CHECK_RESULT("clGetDeviceInfo(device type)")
				
				// Get all device names
			  rc = clGetDeviceInfo
					(platform[i].device[j].id, CL_DEVICE_NAME,
					(size_t) 0, NULL, 
					(size_t *) &param_value_size_ret);
				CHECK_RESULT("clGetDeviceInfo(size of CL_DEVICE_NAME)")
				MEMORY_ALLOC_CHECK(platform[i].device[j].name, param_value_size_ret, "device name");
				rc = clGetDeviceInfo(platform[i].device[j].id, CL_DEVICE_NAME, (size_t) param_value_size_ret, platform[i].device[j].name, (size_t *) NULL);
				CHECK_RESULT("clGetDeviceInfo(CL_DEVICE_NAME)")
			}
			free(tmpdevices);
		}
	};
	~compute_helper() {};

	void
	get_num_platforms()
	{
		cl_uint num_platforms;
		clGetPlatformIDs
			(0, NULL,
			&num_platforms);
		std::cout << num_platforms << std::endl;
	}

	void 
	print_openCL_info() 
	{
		std::cout << "===========OpenCL Setup===========" << std::endl;
		for(uint i=0; i<num_platforms; i++){
			std::cout << "Platform " << i << std::endl
				<< "    " << "Platform Name: " << platform[i].name << std::endl
				<< "    " << "Platform Num Dev: " << platform[i].num_devices << std::endl;
				for(uint j=0;j<platform[i].num_devices; j++) {
					std::cout << "        Device " << j << ": " << platform[i].device[j].name << 
										" Type: " << platform[i].device[j].type << std::endl;
				}
		}
	}

	void 
	create_context(cl_uint platform_index, cl_uint device_index) 
	{
		properties[0] = CL_CONTEXT_PLATFORM;
		properties[1] = (const cl_context_properties) platform[platform_index].id;
		properties[2] = 0;
		platform[platform_index].context = clCreateContext((const cl_context_properties *) properties, 1, &(platform[platform_index].device[device_index].id), NULL, NULL, &rc);
		CHECK_RESULT("clCreateContext")
		std::cout << "Created Context " << platform[platform_index].context << std::endl;
	};

	static char* 
	load_program_source(const char *filename)
	{
		struct stat statbuf;

		FILE *fh = fopen(filename, "r");
		if (fh == 0) {
			fprintf(stderr, "Couldn't open %s\n", filename);
			return NULL;
		}

		stat(filename, &statbuf);
		char *source = (char *) malloc(statbuf.st_size + 1);
		if (source == NULL) {
			fprintf(stderr, "malloc failed\n");
			return NULL;
		}

		fread(source, statbuf.st_size, 1, fh);
		source[statbuf.st_size] = '\0';

		return source;
	};

	void 
	create_kernel(std::filesystem::path kernel_source_file, std::string kernel_name,
								cl_uint platform_index, cl_uint device_index) 
	{
		char *kernel_source;

		std::cout << "Loading Kernel from " << kernel_source_file << std::endl;
		
		kernel_source = load_program_source(kernel_source_file.c_str());
		if (kernel_source == NULL) {
			fprintf(stderr, "Error: Failed to load compute program from file!\n");
			exit(EXIT_FAILURE);
		}

		platform[platform_index].program = clCreateProgramWithSource(platform[platform_index].context, 1, (const char **) &kernel_source, NULL, &rc);
		CHECK_RESULT("clCreateProgramWithSource")
		free(kernel_source);

		rc = clBuildProgram(platform[platform_index].program, 1, &(platform[platform_index].device[device_index].id), "", NULL, NULL);
		CHECK_RESULT("clBuildProgram")

		platform[platform_index].kernel = clCreateKernel(platform[platform_index].program, kernel_name.c_str(), &rc);
		CHECK_RESULT("clCreateKernel")
	}

	void 
	create_command_queue(cl_uint platform_index, cl_uint device_index) 
	{
		platform[platform_index].device[device_index].ComQ = 
		clCreateCommandQueueWithProperties(platform[platform_index].context, platform[platform_index].device[device_index].id, NULL, &rc);
		CHECK_RESULT("clCreateCommandQueue")
		std::cout << "Created Command Queue: " << platform[platform_index].device[device_index].ComQ << std::endl;
	};

	void 
	get_device_details(cl_uint platform_index, cl_uint device_index) 
	{
		rc = clGetDeviceInfo(platform[platform_index].device[device_index].id, CL_DEVICE_NAME, (size_t) 0, NULL, (size_t *) &param_value_size_ret);
		CHECK_RESULT("clGetDeviceInfo(size of CL_DEVICE_NAME)")
		MEMORY_ALLOC_CHECK(platform[platform_index].device[device_index].name, param_value_size_ret, "device name");
		
		rc = clGetDeviceInfo(platform[platform_index].device[device_index].id, CL_DEVICE_NAME, (size_t) param_value_size_ret, platform[platform_index].device[device_index].name, (size_t *) NULL);
		CHECK_RESULT("clGetDeviceInfo(CL_DEVICE_NAME)")

		rc = clGetDeviceInfo (platform[platform_index].device[device_index].id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &max_compute_units, NULL);
		CHECK_RESULT("clGetDeviceInfo(CL_DEVICE_MAX_COMPUTE_UNITS)")

		std::cout << "Platform " << platform_index << " Device " << device_index <<
		" has " << max_compute_units << " Compute Units" << std::endl; 
	};

	void 
	get_device_memory_info(cl_uint platform_index, cl_uint device_index) 
	{
		rc = clGetKernelWorkGroupInfo
			(platform[platform_index].kernel, 
			platform[platform_index].device[device_index].id, 
			CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), 
			(void *) &kernel_wg_size, return_size);
   CHECK_RESULT("clGetKernelWorkGroupInfo(CL_KERNEL_WORK_GROUP_SIZE)")

  	rc = clGetDeviceInfo
	 		(platform[platform_index].device[device_index].id,
			CL_DEVICE_LOCAL_MEM_SIZE, sizeof (cl_ulong), 
			(void *) &total_local_mem, NULL);
   CHECK_RESULT("clGetDeviceInfo(CL_DEVICE_LOCAL_MEM_SIZE)")

		rc = clGetKernelWorkGroupInfo
			(platform[platform_index].kernel, 
			platform[platform_index].device[device_index].id, 
			CL_KERNEL_LOCAL_MEM_SIZE, sizeof (cl_ulong), 
			&used_local_mem, NULL);
		CHECK_RESULT("clGetKernelWorkGroupInfo(CL_KERNEL_LOCAL_MEM_SIZE)")

		avalible_local_mem = total_local_mem - used_local_mem;

		std::cout << "Platform " << platform_index << " Device " << device_index <<
		" has " << avalible_local_mem << " Avalible Memory" << std::endl; 
	}

	void 
	get_kernel_details(cl_uint platform_index, cl_uint device_index)
	{
		rc = clGetKernelWorkGroupInfo (platform[platform_index].kernel, platform[platform_index].device[device_index].id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), (void *) &kernel_wg_size, return_size);
		CHECK_RESULT("clGetKernelWorkGroupInfo(CL_KERNEL_WORK_GROUP_SIZE)")
	}

	void
	allocate_kernel_memory(cl_int platform_index, cl_int device_index, unsigned int num_el)
	{
		unsigned int A_buffer_size, B_buffer_size, C_buffer_size;
		
		// Create a buffer for A vector
		A_buffer_size = (num_el * sizeof(float));
		A_buffer = clCreateBuffer
										(platform[platform_index].context, 
										CL_MEM_ALLOC_HOST_PTR,
										A_buffer_size, NULL, &rc);
		CHECK_RESULT("clCreateBuffer(A_buffer)")
		// Map the region of device memory to the host address space
		A_array = (float *) 
							clEnqueueMapBuffer
								(platform[platform_index].device[device_index].ComQ , A_buffer, 
								CL_TRUE, CL_MAP_WRITE, 
								0, (size_t) A_buffer_size,
								0, NULL,
								NULL, &rc);
		CHECK_RESULT("clEnqueueMapBuffer(A_array)")

		// Create a buffer for B vector
		B_buffer_size = (num_el * sizeof(float));
		B_buffer = clCreateBuffer
											(platform[platform_index].context, 
											CL_MEM_ALLOC_HOST_PTR, B_buffer_size, 
											NULL, &rc);
		CHECK_RESULT("clCreateBuffer(matrix_buffer)")
		// Map the region of device memory to the host address space
		B_array = (float *) 
							clEnqueueMapBuffer
								(platform[platform_index].device[device_index].ComQ , B_buffer, 
								CL_TRUE, CL_MAP_WRITE, 
								0, (size_t) B_buffer_size,
								0, NULL,
								NULL, &rc);
		CHECK_RESULT("clEnqueueMapBuffer(B_array)")

		// Create a buffer for C vector
		C_buffer_size = (num_el * sizeof(float));
		C_buffer = clCreateBuffer
											(platform[platform_index].context,
											CL_MEM_ALLOC_HOST_PTR, C_buffer_size,
											NULL, &rc);
		CHECK_RESULT("clCreateBuffer(output_buffer)")
		// Map the region of device memory to the host address space
		C_array = (float *) 
							clEnqueueMapBuffer
								(platform[platform_index].device[device_index].ComQ , C_buffer, 
								CL_TRUE, CL_MAP_WRITE, 
								0, (size_t) C_buffer_size,
								0, NULL,
								NULL, &rc);
		CHECK_RESULT("clEnqueueMapBuffer(B_array)")
	}

	void set_kernel_memory(cl_int platform_index, cl_int device_index, unsigned int num_el)
	{
		cl_event events[3];
		// Load some dummy info into the vectors
		for(uint i=0; i<num_el; i++) {
			A_array[i] = i;
			B_array[i] = i;
			C_array[i] = i;
		}
		rc = clEnqueueUnmapMemObject
					(platform[platform_index].device[device_index].ComQ, A_buffer, 
					A_array, 0, NULL, &events[0]);
   	CHECK_RESULT("clEnqueueUnmapMemObject(A_array)")
		rc = clEnqueueUnmapMemObject
					(platform[platform_index].device[device_index].ComQ, B_buffer, 
					B_array, 0, NULL, &events[1]);
   	CHECK_RESULT("clEnqueueUnmapMemObject(B_array)")
		rc = clEnqueueUnmapMemObject
					(platform[platform_index].device[device_index].ComQ, C_buffer, 
					C_array, 0, NULL, &events[2]);
   	CHECK_RESULT("clEnqueueUnmapMemObject(C_array)")
		clWaitForEvents(3, events);
	}

	void set_kernel_args(cl_int platform_index, cl_int device_index) 
	{
		// FIX: This is hard coded values for the one kernel im testing with
		rc = clSetKernelArg
					(platform[platform_index].kernel, 0, 
					sizeof(cl_mem), &A_buffer);
		CHECK_RESULT("clSetKernelArg(0)")
		rc = clSetKernelArg
					(platform[platform_index].kernel, 1, 
					sizeof(cl_mem), &B_buffer);
		CHECK_RESULT("clSetKernelArg(1)")
		rc = clSetKernelArg
					(platform[platform_index].kernel, 2, 
					sizeof(cl_mem), &C_buffer);
		CHECK_RESULT("clSetKernelArg(2)")
	}

	void run_kernel(cl_int platform_index, cl_int decive_index, unsigned int num_el) 
	{
		cl_event events;
		// FIX: This is hard coded values for the one kernel im testing with
		size_t global_work_size[1] {num_el};
		size_t local_work_size[1] {1};

		rc = clEnqueueNDRangeKernel
					(platform[platform_index].device[decive_index].ComQ, 
					platform[platform_index].kernel, 1, 
					NULL, global_work_size, local_work_size, 0, NULL, &events);
		CHECK_RESULT("clEnqueueNDRangeKernel")
	  C_array = (float *) 
							clEnqueueMapBuffer
								(platform[platform_index].device[decive_index].ComQ, 
                C_buffer, CL_TRUE, 
                (CL_MAP_READ|CL_MAP_WRITE),0, 
                (size_t) (num_el*sizeof(float)), 0, 
								NULL, NULL, 
                &rc);
   CHECK_RESULT("clEnqueueMapBuffer(C_array)")
	 
	 std::cout << "Output array check" << std::endl;
	 for(uint i=0; i<num_el; i++) {
		std::cout << C_array[i]<< std::endl;
	 }
	}

private:
	platform_struct* platform;
	cl_uint num_platforms;
	std::vector<device_struct> device_vector;
	cl_uint preferred_alignment {16};
	cl_int rc; // Return code for openCL calls
	size_t param_value_size_ret;
	size_t return_size[1];
	
	// Device information
	cl_device_type __device_type;
	cl_ulong total_local_mem;
	cl_ulong used_local_mem;
	cl_ulong avalible_local_mem;
	cl_uint max_compute_units;
	std::string dev_name;
	
	// Platform informaiton
	std::string platform_name;
	
	// context information
	cl_context_properties properties[3];

	// program informaiton
	cl_program program;

	// Kernel information	
	cl_kernel kernel;
	size_t kernel_wg_size;

	// REMOVE BELLOW TESTING ONLY
	cl_mem A_buffer;
	cl_mem B_buffer;
	cl_mem C_buffer;
	
	float* A_array;
	float* B_array;
	float* C_array;
};