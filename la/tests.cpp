#include "openCL_helpers.hpp"
#include <CL/cl.h>
#include <cstdlib>
#include <iostream>

int main(int argc, char* argv[]) {
	// test compute_helper to make sure openCL is wokring
	compute_helper cpu_CL;
	int num_elements {11};
	cpu_CL.print_openCL_info();
	cpu_CL.get_num_platforms();
	cpu_CL.create_context(0, 0);
	cpu_CL.get_device_details(0, 0);
	cpu_CL.create_command_queue(0, 0);
	cpu_CL.create_kernel("axpy.cl", "axpy", 0, 0);
	cpu_CL.get_device_memory_info(0, 0);
	cpu_CL.allocate_kernel_memory(0, 0, num_elements);
	cpu_CL.get_device_memory_info(0, 0);
	cpu_CL.set_kernel_memory(0, 0, num_elements);
	cpu_CL.set_kernel_args(0, 0);
	cpu_CL.get_device_memory_info(0, 0);
	cpu_CL.run_kernel(0, 0, num_elements);
	return EXIT_SUCCESS;
}