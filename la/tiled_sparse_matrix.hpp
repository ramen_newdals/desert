#include <cstdlib>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sys/stat.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>

typedef struct _slab_header {
	/* Offset into tiled matrix structure to beginning of this slab, in units of packets */
	cl_uint offset;
	/* Offset into the output vector where this slab's "output region" starts */
	cl_uint outindex;
	/* Number of elements in this slab's "output region" */
	cl_uint outspan;
} slab_header;

typedef struct _packet {
   cl_uint seg_input_offset; 				 //Section of the input vector this packet addresses
   cl_uint future_seg_input_offset;  // next input vector section, so pre-loading can be done
   cl_uint npackets_remaining;       // number of packets remaining in this slab
   cl_uint seg_output_offset;        // in slab's "output region" which set of 16 outputs is this packe addressing? */
   cl_uint pad1;
   cl_uint pad2;
   cl_uint pad3;
   cl_uint pad4;
   cl_ushort input_offset_short[16]; // input values from the identified section of input vector is this packet using? */
   float matdata[16];                /* fp matrix values encoded into this packet */
} packet;
