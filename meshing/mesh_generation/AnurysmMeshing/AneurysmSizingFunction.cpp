#include "AneurysmSizingFunction.hpp"

vtkStandardNewMacro(AnerysmSizingFunction)

AnerysmSizingFunction::AnerysmSizingFunction(){
	this->SizingFunctionArrayName = NULL;
	this->coarseningFactor = 1.0;
}

AnerysmSizingFunction::~AnerysmSizingFunction(){
	if(this->SizingFunctionArrayName){
		delete[] this->SizingFunctionArrayName;
		this->SizingFunctionArrayName = NULL;
	}
}

int AnerysmSizingFunction::RequestData(	vtkInformation *vtkNotUsed(request),
																				vtkInformationVector **inputVector,
																				vtkInformationVector *outputVector){
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkPolyData *input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	// Error Handeling
	if(input->GetNumberOfPoints() < 1){
		vtkErrorMacro(<< "No points in input");
		return 1;
	}
	if(!this->SizingFunctionArrayName){
		vtkErrorMacro(<< "Sizing function array name not set");
		return 1;
	}
	// Buildcells and Buildlinks to speed up get cells and get points, get etc...
	input->BuildCells();
	input->BuildLinks();

	int NumPoints {input->GetNumberOfPoints()}, i, j, NumPointCells;
	double AvgArea, point0[3], point1[3], point2[3];
	vtkDoubleArray *SizingFunctionArray = vtkDoubleArray::New();
	vtkAbstractArray *CurvatureArray = vtkDoubleArray::New();
	vtkAbstractArray *Dist2CenterlinesArray = vtkDoubleArray::New();

	// Initizlize sizing fucntion array
	SizingFunctionArray->SetName(this->SizingFunctionArrayName);
	SizingFunctionArray->SetNumberOfTuples(NumPoints);
	SizingFunctionArray->FillComponent(0, 0.0);

	// Copy the CurvatureArray and Dist2CenetelinesArray
	CurvatureArray = input->GetPointData()->GetAbstractArray(CurvatureArrayName);
	Dist2CenterlinesArray = input->GetPointData()->GetAbstractArray(Dist2CenterlinesArrayName);

	vtkIdList *PointsCells = vtkIdList::New();

	for(i=0; i<NumPoints; i++){
		input->GetPointCells(i, PointsCells);
		input->GetPointData()->GetAbstractArray(i);
		NumPointCells = PointsCells->GetNumberOfIds();
		AvgArea = 0.0;
		if(NumPointCells == 0){
			continue;
		}
		for(j=0; j<NumPointCells; j++){
			vtkTriangle *triangle = vtkTriangle::SafeDownCast(input->GetCell(PointsCells->GetId(j))); 
			if(!triangle){
				vtkErrorMacro(<<"Cell" << input->GetCell(PointsCells->GetId(j)) << "not triangle: skipping cell for sizing fucntion computation");
			}
		}
	}
}