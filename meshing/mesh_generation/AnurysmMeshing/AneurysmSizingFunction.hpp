#ifndef __AnerysmSizingFuntion_h
#define __AnerysmSizingFuntion_h

// VTK headers
#include <vtkPolyDataAlgorithm.h>
#include <vtkIdList.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

class AnerysmSizingFunction : vtkPolyDataAlgorithm {
	public:
		vtkTypeMacro(AnerysmSizingFunction, vtkPolyDataAlgorithm)
		void PrintSelf(std::ostream & os, vtkIndent indent) override;

		static AnerysmSizingFunction *New();
		vtkSetStringMacro(SizingFunctionArrayName)
		vtkGetStringMacro(SizingFunctionArrayName)

	protected:
		AnerysmSizingFunction();
		~AnerysmSizingFunction();
		virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;
		char *SizingFunctionArrayName;
		char *CurvatureArrayName;
		char *Dist2CenterlinesArrayName;
		double coarseningFactor;
};

#endif