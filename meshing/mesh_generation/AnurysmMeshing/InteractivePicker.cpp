#include "InteractivePicker.hpp"

vtkStandardNewMacro(InteractivePicker)

void InteractivePicker::InspectHandeler(){
	std::cout << "Inspecting mesh" << std::endl;
	vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

void InteractivePicker::SelectHandeler(){
	int* clickPos = this->GetInteractor()->GetEventPosition();

	// Pick from this location.
	vtkNew<vtkPointPicker> Picker;
	Picker->Pick(clickPos[0], clickPos[1], 0, this->GetDefaultRenderer());

	double* pos = Picker->GetPickPosition();

	auto pickedActor = Picker->GetActor();
	if(pickedActor == nullptr){
		std::cout << "No actor picked." << std::endl;
		vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
	}
	else{
		// Draw a sphere on the picked point
		DrawSphere(pos);

		// Add the picked point id to PickedPoints
		PickedPoints.push_back(Picker->GetPointId());

		// Forward events
		vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
	}
}

void InteractivePicker::ComputeHandeler(){
	Geodesic = vtkSmartPointer<vtkDijkstraGraphGeodesicPath>::New();

	// Set input for Geodesic calculation
	Geodesic->SetInputData(Mesh);
	SetGeodesicSeeds();
	ComputeGeodesicPaths();

	// Output the path lengths for each section of the geodesic
	std::cout << "Geodesic Paths are: " << std::endl;
	for(int i = 0; i<GeodesicPaths.size(); i++){
		std::cout << "Path " << i << " Has the Following Path";
		for(int j = 0; j<GeodesicPaths[i]->GetNumberOfIds(); j++){
			std::cout << " --> " << GeodesicPaths[i]->GetId(j);
		}
		std::cout << std::endl;
	}

	// Draw the Geodesic path using ribbons
	DrawGeodesicPath();

	// Switch the mode back to inspect
	Mode = INSPECT;
}

void InteractivePicker::SetGeodesicSeeds(bool AutoCloseGeodesic){
	// the list of seeds where for n seeds n-1 geodesic paths need to be generated
	GeodesicSeeds.clear();
	for(int i = 1; i<PickedPoints.size(); i++){
		std::pair<vtkIdType, vtkIdType> GeodesicSeed;
		GeodesicSeed = std::make_pair(PickedPoints[i-1], PickedPoints[i]);
		GeodesicSeeds.push_back(GeodesicSeed);
	}
	// If selected will automatically close the geodesic path
	if(AutoCloseGeodesic){
		std::pair<vtkIdType, vtkIdType> GeodesicSeed;
		GeodesicSeed = std::make_pair(PickedPoints[PickedPoints.size()-1], PickedPoints[0]);
		GeodesicSeeds.push_back(GeodesicSeed);
	}
}

void InteractivePicker::ComputeGeodesicPaths(){
	// Useing the GeodesicSeeds compute a path for each seed
	GeodesicPathARRAY = vtkSmartPointer<vtkIdTypeArray>::New();
	GeodesicPaths.clear();
	for(int i = 0; i<GeodesicSeeds.size(); i++){
		Geodesic->SetStartVertex(GeodesicSeeds[i].first);
		Geodesic->SetEndVertex(GeodesicSeeds[i].second);
		Geodesic->Update();
		// for(int j = 0; j<Geodesic->GetIdList()->GetNumberOfIds(); j++){
		for(int j = (Geodesic->GetIdList()->GetNumberOfIds() -1); j>=0; j--){
			std::cout << "id: " << Geodesic->GetIdList()->GetId(j) << std::endl;
			GeodesicPath.push_back(Geodesic->GetIdList()->GetId(j));
			GeodesicPathARRAY->InsertNextValue(Geodesic->GetIdList()->GetId(j));
		}
		// The list from Geodesic is generated in reverse where the first ID is the EndVertex not the StartVertex
		GeodesicPaths.push_back(Geodesic->GetIdList());
	}
}

// ================ Rendering and interaction methods ================

void InteractivePicker::DrawSphere(double *pos){
	// Draws a sphere on the selected point
	// Create a sphere
	vtkNew<vtkSphereSource> sphereSource;
	sphereSource->SetCenter(pos[0], pos[1], pos[2]);
	sphereSource->SetRadius(0.1);

	// Create a mapper and actor
	vtkNew<vtkPolyDataMapper> mapper;
	mapper->SetInputConnection(sphereSource->GetOutputPort());

	vtkNew<vtkActor> actor;
	actor->SetMapper(mapper);
	actor->GetProperty()->SetColor(colors->GetColor3d("MistyRose").GetData());

	this->GetDefaultRenderer()->AddActor(actor);

}

void InteractivePicker::DrawGeodesicPath(){
	// Create nessecary data for the ribbon to be drawn
	vtkNew<vtkPoints> Points;
	vtkNew<vtkCellArray> Lines;
	vtkNew<vtkPolyData> PolyData;
	vtkNew<vtkRibbonFilter> Ribbon;
	vtkNew<vtkPolyDataMapper> RibbonMapper;
	vtkNew<vtkActor> RibbonActor;

	for(int i = 0; i<GeodesicPathARRAY->GetNumberOfValues(); i++){
		Points->InsertNextPoint(Mesh->GetPoint(GeodesicPathARRAY->GetValue(i)));
	}

	for(int j = 1; j<Points->GetNumberOfPoints(); j+=2){
		vtkNew<vtkLine> Line;
		Line->GetPointIds()->SetId(0, j-1);
		Line->GetPointIds()->SetId(1, j);
		Lines->InsertNextCell(Line);
	}
	// Create polydata
	PolyData->SetPoints(Points);
	PolyData->SetLines(Lines);

	// Set up the Ribon
	Ribbon->SetInputData(PolyData);
	Ribbon->SetWidth(0.2);

	// Set up the mapper and actor
	RibbonMapper->SetInputConnection(Ribbon->GetOutputPort());
	RibbonActor->SetMapper(RibbonMapper);
	RibbonActor->GetProperty()->SetColor(colors->GetColor3d("AliceBlue").GetData());

	this->GetDefaultRenderer()->AddActor(RibbonActor);
}