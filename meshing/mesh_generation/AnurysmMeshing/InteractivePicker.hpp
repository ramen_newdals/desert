#ifndef __AnurysmMeshing_InteractivePicker_SurfacePrep_h
#define __AnurysmMeshing_InteractivePicker_SurfacePrep_h

// VTK Headers
#include <vtkActor.h>
#include <vtkCellArray.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkPlaneSource.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkSphereSource.h>
#include <vtkSmartPointer.h>
#include <vtkDijkstraGraphGeodesicPath.h>
#include <vtkRibbonFilter.h>
#include <vtkIdList.h>
#include <vtkLine.h>

// STD Headers
#include <string>

/// @brief 
class InteractivePicker : public vtkInteractorStyleTrackballCamera{
	public:
		static InteractivePicker *New();
		vtkTypeMacro(InteractivePicker, vtkInteractorStyleTrackballCamera);

		vtkNew<vtkNamedColors> colors;

		virtual void OnLeftButtonDown() override{
			
			switch (Mode){
				case SELECT:{
					SelectHandeler();
					}
				case INSPECT:{
					InspectHandeler();
				}
			}
		}
		
		virtual void OnKeyPress() override{
			vtkRenderWindowInteractor *RWI = this->Interactor;
			key = RWI->GetKeySym();
			std::cout << "Pressed " << key << std::endl;
			
			// Change mode if mode keys are pressed otherwise just report the key press
			if(key == "i"){
					std::cout << "Changing to Inspect mode" << std::endl;
					Mode = INSPECT;
			}
			else if(key == "s"){
					std::cout << "Changing to Select mode" << std::endl;
					Mode = SELECT;
			}
			else if(key == "c"){
				std::cout << "Computing The Geodesic" << std::endl;
				ComputeHandeler();
			}
			else if(key == "r"){
				std::cout << "Clearing the input selection" << std::endl;
				PickedPoints.clear();
			}
		}

		// Input mesh data
		vtkSmartPointer<vtkPolyData> Mesh;

		// Output geodesic data
		vtkSmartPointer<vtkIdTypeArray> GeodesicPathARRAY;
		std::vector<vtkIdType> GeodesicPath;

	private:
		// Mode variables
		enum StateMode {INSPECT, SELECT};
		StateMode Mode {INSPECT};
		std::string key;

		// Data modified in interactive selection
		std::vector<vtkIdType> PickedPoints;

		// Geodesic Data
		std::vector<std::pair<vtkIdType, vtkIdType>> GeodesicSeeds;
		std::vector<vtkIdList*> GeodesicPaths;
		vtkSmartPointer<vtkDijkstraGraphGeodesicPath> Geodesic;
		void SetGeodesicSeeds(bool AutoCloseGeodesic=true);
		void ComputeGeodesicPaths();

		// Different Mode Handeler 
		void InspectHandeler();
		void SelectHandeler();
		void ComputeHandeler();

		// Methods to handle placing a sphere on selected nodes
		void DrawGeodesicPath();
		void DrawSphere(double *pos);
};

#endif