#include "InteractiveRender.hpp"

InteractiveRender::InteractiveRender(vtkPolyData *InputMesh){
	Mesh = InputMesh;
	// Initialize all object
	Colors = vtkSmartPointer<vtkNamedColors>::New();
	Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	MeshActor = vtkSmartPointer<vtkActor>::New();
	Renderer = vtkSmartPointer<vtkRenderer>::New();
	RenderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	RenderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	InteractionStyle = vtkSmartPointer<InteractivePicker>::New();
	InteractionStyle->Mesh = InputMesh;
}

InteractiveRender::~InteractiveRender(){
}

void InteractiveRender::SetMesh(vtkPolyData *InputMesh){
	Mesh = InputMesh;
}

void InteractiveRender::StartRenderWindow(){
	Mapper->SetInputData(Mesh);

	MeshActor->SetMapper(Mapper);
	MeshActor->GetProperty()->SetColor(Colors->GetColor3d("LightGoldenrodYellow").GetData());
	MeshActor->GetProperty()->SetEdgeVisibility(true);

	std::cout << "Actor address: " << MeshActor << std::endl;

	RenderWindow->AddRenderer(Renderer);
	RenderWindow->SetWindowName("the beam");

	RenderWindowInteractor->SetRenderWindow(RenderWindow);

	// Configure the interaction class
	InteractionStyle->SetDefaultRenderer(Renderer);
	RenderWindowInteractor->SetInteractorStyle(InteractionStyle);

	// Add the actors to the scene
	Renderer->AddActor(MeshActor);
	Renderer->SetBackground(Colors->GetColor3d("DodgerBlue").GetData());

	// Start the renderer
	RenderWindow->Render();
	RenderWindowInteractor->Initialize();
	RenderWindowInteractor->Start();
}
