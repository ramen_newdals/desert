#ifndef __AnurysmMeshing_InteractiveRender_SurfacePrep_h
#define __AnurysmMeshing_InteractiveRender_SurfacePrep_h

// VTK Headers
#include <vtkPolyData.h>
#include <vtkNamedColors.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkObjectFactory.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>

// Custom Headers
#include "InteractivePicker.hpp"

// STD Headers
#include <vector>
#include <utility>

class InteractiveRender{
private:
	// Rendering Data
	vtkSmartPointer<vtkNamedColors> Colors; //<Color data///<
	vtkSmartPointer<vtkPolyDataMapper> Mapper; //< Mapper ///<
	vtkSmartPointer<vtkActor> MeshActor;	//< Actor for the input mesh ///<
	vtkSmartPointer<vtkRenderer> Renderer; //< Render for the scene ///<
	vtkSmartPointer<vtkRenderWindow> RenderWindow; //< Renderwindow for the scene ///<
	vtkSmartPointer<vtkRenderWindowInteractor> RenderWindowInteractor; //< Interactor for the scene///<
	vtkPolyData *Mesh;
public:
	InteractiveRender(vtkPolyData *InputMesh);
	~InteractiveRender();
	vtkSmartPointer<InteractivePicker> InteractionStyle; //< Interactions to be used with the render window///<
	void StartRenderWindow();
	void SetMesh(vtkPolyData *InputMesh);
};

#endif