#include "generateMesh.hpp"
#include "uqMeshPolyDataCenterlines.hpp"
#include <array>
#include <cmath>
#include <cstdlib>
#include <tuple>
#include <vtkCell.h>
#include <vtkCellLocator.h>
#include <vtkCleanUnstructuredGrid.h>
#include <vtkConnectivityFilter.h>
#include <vtkIntArray.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkType.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLPolyDataReader.h>

GenerateMesh::GenerateMesh
	(std::filesystem::path InputMeshFileName, std::filesystem::path OutputFolder,
	std::filesystem::path InputFolder)
{
	// Initializes all common operation objects for future use
	Reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
	polyDataWriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	unstructuredGridWriter = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
	// SurfaceCapper = vtkSmartPointer<uqMeshCapPolyData>::New();
	// Centerlines = vtkSmartPointer<uqMeshPolyDataCenterlines>::New();
	SourceSeeds = vtkSmartPointer<vtkIdList>::New();
	TargetSeeds = vtkSmartPointer<vtkIdList>::New();
	Cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	TriangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
	SurfaceRemeshing = vtkSmartPointer<uqMeshPolyDataSurfaceRemeshing>::New();

	// Set the strings for debug file save folder
	OutputMeshfolder = OutputFolder;
	InputDataFolder = InputFolder;
	
	// Check that OutputFolder exists and if it does not create it and
	// log its creation to user
	if(!std::filesystem::exists(OutputMeshfolder)){
		std::cout << 
			"===========OutputFolder Does Not Exists Creating It===========" 
			<< std::endl;
		std::filesystem::create_directories
			(OutputMeshfolder);
	}

	// Read input surface mesh
	Reader->SetFileName(InputMeshFileName.c_str());
	Reader->Update();
	inputMesh = Reader->GetOutput();
}

GenerateMesh::~GenerateMesh()
{
	// Empty Destructor beceause used smart pointers
}

void GenerateMesh::savePolyData
			(std::filesystem::path fileDirectory, std::string fileName,
			vtkPolyData *InputData)
{
	// TO DO: Should accept a std::fs::path (Can be converted to .c_str for quick)
	polyDataWriter->SetFileName
		(
			(fileDirectory.replace_filename(fileName).replace_extension
				(".vtp")).c_str());
	polyDataWriter->SetInputData(InputData);
	polyDataWriter->Write();
}

void GenerateMesh::saveUnstructuredGrid
			(std::filesystem::path fileDirectory, std::string fileName, 
			vtkUnstructuredGrid *InputData)
{
	// TO DO: Should accept a std::fs::path (Can be converted to .c_str for quick)
	unstructuredGridWriter->SetFileName(
		(fileDirectory.replace_filename(fileName).replace_extension
			(".vtu")).c_str());
	unstructuredGridWriter->SetInputData(InputData);
	unstructuredGridWriter->Write();
}

int GenerateMesh::CapSurface
			(bool debug, bool writeFile, 
			bool inputData, bool reCompute,
			std::string outputFileName, std::string inputFileName)
{
	LOG_SCOPE_F(INFO, "Capping Surface");
	LOG_F(INFO, "Begining Surface Capping");
	if(std::filesystem::exists
			(OutputMeshfolder.replace_filename
				(outputFileName).replace_extension(".vtp")) && !reCompute)
	{
		LOG_F(INFO, "Capped Surface File Exists delete to force recompute");
		return EXIT_SUCCESS;
	}
	
	int LargestAreaIndex, TargetSeedIndex {0}, i;
	double LargestArea {0};

	// Read input mesh file
	vtkNew<vtkXMLPolyDataReader> Reader;
	if(inputData)
	{
		Reader->SetFileName
			(InputDataFolder.replace_filename
				(inputFileName).replace_extension(".vtp").c_str());
	}
	else
	{
		Reader->SetFileName
			(OutputMeshfolder.replace_filename
				(inputFileName).replace_extension(".vtp").c_str());
	}
	vtkSmartPointer<uqMeshCapPolyData> SurfaceCapper = 
		vtkSmartPointer<uqMeshCapPolyData>::New();
	
	Reader->Update();
	SourceSeeds->Reset();
	TargetSeeds->Reset();
	SurfaceCapper->SetInputData(Reader->GetOutput());
	SurfaceCapper->SetCellEntityIdsArrayName("cellID");
	SurfaceCapper->SetCellEntityIdOffset(WALL_ENTITY_OFFSET);
	SurfaceCapper->Update();
	SourceSeeds->SetNumberOfIds(1); // Inlets
	TargetSeeds->SetNumberOfIds(SurfaceCapper->CapCenterAreas.size()-1); // Outlets

	// Find the largest area to set as source seeds
	for(i = 0; i<SurfaceCapper->CapCenterAreas.size(); i++) {
		if(SurfaceCapper->CapCenterAreas[i] > LargestArea) {
			LargestArea = SurfaceCapper->CapCenterAreas[i];
			LargestAreaIndex = i;
		}
	}
	// Insert seeds into target and soruce seed arrays
	SourceSeeds->InsertId
		(0, SurfaceCapper->GetCapCenterIds()->GetId(LargestAreaIndex));
	for(i = 0; i<SurfaceCapper->CapCenterAreas.size(); i++) {
		if(i != LargestAreaIndex) {
			TargetSeeds->InsertId(TargetSeedIndex, 
														SurfaceCapper->GetCapCenterIds()->GetId(i));
			TargetSeedIndex++;
		}
	}
	vtkIdType cap_center_id;
	double cap_center[3];
	//HACK: Clear vector before assingnment as we need ot genreate multiple caps
	inlet_outlet_centers.clear();
	// Assigin the inlet-outlet centers bassed on the cap-center id
	// This assumes the caps are labeled in order witch they are for now
	for(i=0; i<SurfaceCapper->GetCapCenterIds()->GetNumberOfIds(); i++) {
		cap_center_id = SurfaceCapper->GetCapCenterIds()->GetId(i);
		SurfaceCapper->GetOutput()->GetPoint(cap_center_id, cap_center);
		std::array<double, 3> center{cap_center[0], cap_center[1], cap_center[2]}; 
		inlet_outlet_centers.push_back(center);
		LOG_F(INFO, "Cap ID %d has area %f", i,
					SurfaceCapper->CapCenterAreas[i]);
	}
	LOG_F(INFO, "Cap Center ID %llu is assigined as inlet", 
				SurfaceCapper->GetCapCenterIds()->GetId(LargestAreaIndex));
	if(writeFile) {
		savePolyData(OutputMeshfolder, outputFileName, SurfaceCapper->GetOutput());
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::ComputeCenterlines
			(bool debug, bool writeFile,
			bool inputData, bool reCompute,
			std::string outputFileName, std::string inputFileName)
{
	if(std::filesystem::exists(OutputMeshfolder.replace_filename(outputFileName).replace_extension(".vtp"))
		&& !reCompute) {
		std::cout << 
		"===========" <<
		outputFileName << 
		"Exists. Delete File to force recompute===========" << 
		std::endl;
		return EXIT_SUCCESS;
	}
	LOG_SCOPE_F(INFO, "Centerlines");
	LOG_F(INFO, "Genrating Centerlines");
	
	// Read input mesh
	vtkNew<vtkXMLPolyDataReader> inputReader;
	inputReader->SetFileName
		(OutputMeshfolder.replace_filename
			(inputFileName).replace_extension(".vtp").c_str());
	inputReader->Update();

	vtkSmartPointer<uqMeshPolyDataCenterlines> Centerlines = 
		vtkSmartPointer<uqMeshPolyDataCenterlines>::New();

	Centerlines->SetSourceSeedIds(SourceSeeds);
	Centerlines->SetTargetSeedIds(TargetSeeds);
	Centerlines->SetRadiusArrayName("Radius");
	Centerlines->GenerateVoronoiDiagramOn();
	Centerlines->SetCenterlineResampling(1);
	Centerlines->SetResamplingStepLength(0.1);
	Centerlines->AppendEndPointsToCenterlinesOn();

	Centerlines->SetInputConnection(inputReader->GetOutputPort());
	Centerlines->Update();

	// LOG_F(INFO, "Reseting Source and Target Id arrays");



	if(writeFile)
	{
		savePolyData(OutputMeshfolder, outputFileName, Centerlines->GetOutput());
	}

	return EXIT_SUCCESS;
}

int GenerateMesh::ComputeDist2Centerlines
			(bool debug, bool writeFile,
			bool inputData, bool reCompute,
			std::string outputFileName, std::string inputFileName,
			std::string centerlinesFileName)
{
	if(std::filesystem::exists
		(OutputMeshfolder.replace_filename
			(outputFileName).replace_extension(".vtp")) && !reCompute)
	{
		std::cout << "===========" <<
		outputFileName << 
		"Exists. Delete File to force recompute===========" <<
		std::endl;
		return EXIT_SUCCESS;
	}
	
	// Read input file that will have dist2Centerlines mapped ot each node
	vtkNew<vtkXMLPolyDataReader> inputReader;
	inputReader->SetFileName
		(OutputMeshfolder.replace_filename
			(inputFileName).replace_extension(".vtp").c_str());
	inputReader->Update();

	// read centerlines file to use for dist2Centerlines
	vtkNew<vtkXMLPolyDataReader> centerlinesReader ;
	centerlinesReader->SetFileName
		(OutputMeshfolder.replace_filename
			(centerlinesFileName).replace_extension(".vtp").c_str());
	centerlinesReader->Update();

	// read the input mesh as a filter beceuase this is a hack
	// just need connections for the vtkDemandDrivenPipeline
	vtkNew<vtkXMLPolyDataReader> InputMesh;
	InputMesh->SetFileName
		(OutputMeshfolder.replace_filename
			(inputFileName).replace_extension(".vtp").c_str());

	Dist2Centerlines = vtkSmartPointer<uqMeshPolyDataDistanceToCenterlines>::New();
	Dist2Centerlines->SetInputConnection(InputMesh->GetOutputPort());
	Dist2Centerlines->SetCenterlines(centerlinesReader ->GetOutput());
	Dist2Centerlines->SetDistanceToCenterlinesArrayName("DistanceToCenterlines");
	Dist2Centerlines->SetCenterlineRadiusArrayName("Radius");
	Dist2Centerlines->Update();

	if(debug)
	{
		std::cout <<
		"===========Distance2Centerlines DEBUG Info===========" << 
		std::endl;
	}

	if(writeFile){
		savePolyData(OutputMeshfolder, outputFileName, Dist2Centerlines->GetOutput());
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::SpecifySizingRegions
			(bool debug, bool writeFile,
			bool inputData, bool reCompute,
			std::string outputFileName, std::string inputFileName,
			std::string sizingRegionFileName,	int sizingRegionId) 
{
	vtkSmartPointer<vtkPolyData> sizingSurface =
		vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkPolyData> inputSurface = 
		vtkSmartPointer<vtkPolyData>::New();

	// read sizing region file
	vtkNew<vtkXMLPolyDataReader> sizingRegionFile;
	sizingRegionFile->SetFileName
		(InputDataFolder.replace_filename
			(sizingRegionFileName).replace_extension(".vtp").c_str());
	sizingRegionFile->Update();
	sizingSurface = sizingRegionFile->GetOutput();

	// read input surface
	vtkNew<vtkXMLPolyDataReader> inputSurfaceFile;
	
	if(inputData)
	{
		inputSurfaceFile->SetFileName
			(InputDataFolder.replace_filename
				(inputFileName).replace_extension(".vtp").c_str());
	}
	else
	{
		inputSurfaceFile->SetFileName
			(OutputMeshfolder.replace_filename
				(inputFileName).replace_extension(".vtp").c_str());
	}
	inputSurfaceFile->Update();
	inputSurface = inputSurfaceFile->GetOutput();

	// Holds the sizingRegionId
	vtkSmartPointer<vtkIntArray> sizingRegionIdArray = vtkIntArray::New();
	sizingRegionIdArray->SetName("sizingRegionId");
	sizingRegionIdArray->SetNumberOfTuples(inputSurface->GetNumberOfPoints());
	sizingRegionIdArray->FillComponent(0, 0);

	// Create the tree
	vtkNew<vtkOctreePointLocator> octree;
	octree->SetDataSet(inputSurface);
	octree->BuildLocator();

	double *point;
	vtkIdType pointId;
	vtkSmartPointer<vtkIdList> refinementIds = vtkSmartPointer<vtkIdList>::New();

	for(int i = 0; i<sizingSurface->GetNumberOfPoints(); i++)
	{
		point = sizingSurface->GetPoint(i);
		pointId = octree->FindClosestPoint(point);
		sizingRegionIdArray->SetValue(pointId, sizingRegionId);
	}

	inputSurface->GetPointData()->AddArray(sizingRegionIdArray);

	if(debug)
	{
		std::cout << 
		"===========SpecifySizingRegion DEBUG Info===========" << 
		std::endl;
	}

	if(writeFile){
		savePolyData(OutputMeshfolder, outputFileName, inputSurface);
	}

	return EXIT_SUCCESS;
}

int GenerateMesh::GenerateFlowExtensions
			(bool debug, bool writeFile,
			bool inputData, bool recompute,
			std::string outputFileName, std::string inputFileName,
			std::string centerlinesFileName)
{
	LOG_SCOPE_F(INFO, "Flow Extensions");
	LOG_F(INFO, "Genrating Flow Extensions");
	vtkNew<vtkXMLPolyDataReader> surfaceReader;
	vtkNew<vtkXMLPolyDataReader> centerlinesReader;
	vtkSmartPointer<vtkPolyData> output_surface;
	if(!inputData){
		surfaceReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
		centerlinesReader->SetFileName(OutputMeshfolder.replace_filename(centerlinesFileName).replace_extension(".vtp").c_str());
	}
	else {
		surfaceReader->SetFileName(InputDataFolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
		centerlinesReader->SetFileName(InputDataFolder.replace_filename(centerlinesFileName).replace_extension(".vtp").c_str());
	}
	surfaceReader->Update();
	centerlinesReader->Update();

	vtkSmartPointer<uqMeshPolyDataFlowExtensionsFilter> flow_extensions =
		vtkSmartPointer<uqMeshPolyDataFlowExtensionsFilter>::New();

	flow_extensions->SetInputData(surfaceReader->GetOutput());
	flow_extensions->SetCenterlines(centerlinesReader->GetOutput());
	flow_extensions->SetExtensionModeToUseNormalToBoundary();
	flow_extensions->SetAdaptiveNumberOfBoundaryPoints(1);
	flow_extensions->SetExtensionRatio(6.5);
	flow_extensions->Update();

	output_surface = flow_extensions->GetOutput();

	// HACK: Assigin the wall cellID tag
	vtkSmartPointer<vtkIntArray> cellID_array = vtkIntArray::New();
	cellID_array->SetNumberOfValues(flow_extensions->GetOutput()->GetNumberOfCells());
	cellID_array->FillValue(0);
	cellID_array->SetName("cellID");
	output_surface->GetCellData()->AddArray(cellID_array);

	// Clean the output data to remove extra poly-data cells and points
	vtkNew<vtkCleanPolyData> polydata_cleaner;
	polydata_cleaner->SetInputData(output_surface);
	polydata_cleaner->Update();
	output_surface = polydata_cleaner->GetOutput();

	savePolyData
		(OutputMeshfolder, outputFileName, flow_extensions->GetOutput());
	return EXIT_SUCCESS;
}


int GenerateMesh::SetElSizeFN(
		bool debug, bool writeFile, 
		bool reCompute,
		std::string outputFileName, std::string inputFileName,
		std::string sizingFunctionArrayName, 
		int highFidelityRegionId, float coarseningFactor, 
		int maxNumElem, int minNumElem, 
		float targetEdgeLength, float highFidelityEdgeLength, 
		float stepSize, int numIters)
{
	if (std::filesystem::exists
				(OutputMeshfolder.replace_filename(outputFileName).replace_extension(".vtp")) 
			&& not reCompute)
	{
		std::cout << "===========" << outputFileName << 
		"Exists. Delete File to force recompute===========" << std::endl;
		return EXIT_SUCCESS;
	}

	if(debug)
	{
		std::cout << 
		"===========SizingFunctionCoarse DEBUG Info===========" << std::endl;
	}

	vtkSmartPointer<vtkPolyData> inputSurface;
	vtkSmartPointer<vtkDoubleArray> SizingFunctionArray = 
		vtkDoubleArray::New();
	vtkSmartPointer<vtkIntArray> sizingRegionId = 
		vtkIntArray::New();
	vtkSmartPointer<vtkDoubleArray> dist2CLArray =
		vtkDoubleArray::New(); 

	// Manually set sizing fucntion array names
	char *RegionIdArrayName = "sizingRegionId";
	char *dist2CLArrayName = "DistanceToCenterlines";

	//reading input files to store each of the relevant mesh sizing parameters
	vtkNew<vtkXMLPolyDataReader> inputFile;
	inputFile->SetFileName
		(OutputMeshfolder.replace_filename
			(inputFileName).replace_extension(".vtp").c_str());
	inputFile->Update();
	inputSurface = inputFile->GetOutput();

	sizingRegionId = 
		vtkIntArray::SafeDownCast
			(inputSurface->GetPointData()->GetAbstractArray(RegionIdArrayName));

	dist2CLArray = 
		vtkDoubleArray::SafeDownCast
			(inputSurface->GetPointData()->GetAbstractArray(dist2CLArrayName));

	// Error Handeling
	if(inputSurface->GetNumberOfPoints() < 1) 
	{
		std::cout << "No points in input" << std::endl;
		return EXIT_FAILURE;
	}
	
	if(dist2CLArray->GetNumberOfValues() == 0 ||
		sizingRegionId->GetNumberOfValues() == 0)
	{
		std::cout <<
		"ERROR: Sizing Region or dist2CLArray not in input file" << std::endl;
		return EXIT_FAILURE;
	}

	if(sizingFunctionArrayName.empty())
	{
		std::cout << 
		"Sizing function array name not set" << std::endl;
		return EXIT_FAILURE;
	}
	
	// Buildcells and Buildlinks to speed up get cells and get points, get...
	inputSurface->BuildCells();
	inputSurface->BuildLinks();

	vtkIdType numPts {inputSurface->GetNumberOfPoints()}, i;
	int regionId, numEl, numIter;
	// target area is computed assuming equilateral triangles
	double highFidelityArea{0.433*highFidelityEdgeLength*highFidelityEdgeLength}, 
				 targetArea{0.433*targetEdgeLength*targetEdgeLength},
				 dist2CL, 
				 tempEl;

	// Initizlize sizing fucntion array
	SizingFunctionArray->SetName(sizingFunctionArrayName.c_str());
	SizingFunctionArray->SetNumberOfTuples(numPts);
	SizingFunctionArray->FillComponent(0, 0.0);

	for(i=0; i<numPts; i++) {
		tempEl = targetEdgeLength;
		regionId = sizingRegionId->GetValue(i);
		dist2CL = dist2CLArray->GetValue(i);
		numEl = (dist2CL*2)/tempEl;
		numIter = 0;

		if(regionId == highFidelityRegionId) {
			SizingFunctionArray->SetValue(i, highFidelityArea);
			continue;
		}
		if(numEl < minNumElem) {
			while((numEl < minNumElem) && (numIter < 100)) {
				tempEl -= stepSize;
				numEl = (dist2CL*2)/tempEl;
				numIter++;
			}
			SizingFunctionArray->SetValue(i, (0.433*tempEl*tempEl));
			continue;
		}
		if((numEl > maxNumElem) && (numIter < 100)) {
			while(numEl > maxNumElem) {
				tempEl += stepSize;
				numEl = (dist2CL*2)/tempEl;
				numIter++;
			}
			SizingFunctionArray->SetValue(i, (0.433*tempEl*tempEl));
			continue;
		}
		SizingFunctionArray->SetValue(i, targetArea);
		continue;
	}

	inputSurface->GetPointData()->AddArray(SizingFunctionArray);

	// Write out the new file for testing
	if(writeFile){
		savePolyData(OutputMeshfolder, outputFileName, inputSurface);
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::HACK_SetVolElSizeFN(
		bool debug, bool writeFile, 
		bool reCompute,
		std::string outputFileName, std::string inputFileName,
		std::string sizingFunctionArrayName, 
		int highFidelityRegionId, float coarseningFactor, 
		int maxNumElem, int minNumElem, 
		float targetEdgeLength, float highFidelityEdgeLength, 
		float stepSize, int numIters)
{
	if (std::filesystem::exists
				(OutputMeshfolder.replace_filename(outputFileName).replace_extension(".vtp")) 
			&& not reCompute)
	{
		std::cout << "===========" << outputFileName << 
		"Exists. Delete File to force recompute===========" << std::endl;
		return EXIT_SUCCESS;
	}

	if(debug)
	{
		std::cout << 
		"===========SizingFunctionCoarse DEBUG Info===========" << std::endl;
	}

	vtkSmartPointer<vtkPolyData> inputSurface;
	vtkSmartPointer<vtkDoubleArray> SizingFunctionArray = 
		vtkDoubleArray::New();
	vtkSmartPointer<vtkIntArray> sizingRegionId = 
		vtkIntArray::New();
	vtkSmartPointer<vtkDoubleArray> dist2CLArray =
		vtkDoubleArray::New(); 

	// Manually set sizing fucntion array names
	char *RegionIdArrayName = "sizingRegionId";
	char *dist2CLArrayName = "DistanceToCenterlines";

	//reading input files to store each of the relevant mesh sizing parameters
	vtkNew<vtkXMLPolyDataReader> inputFile;
	inputFile->SetFileName
		(OutputMeshfolder.replace_filename
			(inputFileName).replace_extension(".vtp").c_str());
	inputFile->Update();
	inputSurface = inputFile->GetOutput();

	sizingRegionId = 
		vtkIntArray::SafeDownCast
			(inputSurface->GetPointData()->GetAbstractArray(RegionIdArrayName));

	dist2CLArray = 
		vtkDoubleArray::SafeDownCast
			(inputSurface->GetPointData()->GetAbstractArray(dist2CLArrayName));

	// Error Handeling
	if(inputSurface->GetNumberOfPoints() < 1) 
	{
		std::cout << "No points in input" << std::endl;
		return EXIT_FAILURE;
	}
	
	if(dist2CLArray->GetNumberOfValues() == 0 ||
		sizingRegionId->GetNumberOfValues() == 0)
	{
		std::cout <<
		"ERROR: Sizing Region or dist2CLArray not in input file" << std::endl;
		return EXIT_FAILURE;
	}

	if(sizingFunctionArrayName.empty())
	{
		std::cout << 
		"Sizing function array name not set" << std::endl;
		return EXIT_FAILURE;
	}
	
	// Buildcells and Buildlinks to speed up get cells and get points, get...
	inputSurface->BuildCells();
	inputSurface->BuildLinks();

	vtkIdType numPts {inputSurface->GetNumberOfPoints()}, i;
	int regionId, numEl, numIter;
	// target area is computed assuming equilateral triangles
	double highFidelityArea{highFidelityEdgeLength}, 
				 targetArea{targetEdgeLength},
				 dist2CL, 
				 tempEl;

	// Initizlize sizing fucntion array
	SizingFunctionArray->SetName(sizingFunctionArrayName.c_str());
	SizingFunctionArray->SetNumberOfTuples(numPts);
	SizingFunctionArray->FillComponent(0, 0.0);

	for(i=0; i<numPts; i++) {
		tempEl = targetEdgeLength;
		regionId = sizingRegionId->GetValue(i);
		dist2CL = dist2CLArray->GetValue(i);
		numEl = (dist2CL*2)/tempEl;
		numIter = 0;

		if(regionId == highFidelityRegionId) {
			SizingFunctionArray->SetValue(i, highFidelityArea);
			continue;
		}
		if(numEl < minNumElem) {
			while((numEl < minNumElem) && (numIter < 100)) {
				tempEl -= stepSize;
				numEl = (dist2CL*2)/tempEl;
				numIter++;
			}
			SizingFunctionArray->SetValue(i, tempEl);
			continue;
		}
		if((numEl > maxNumElem) && (numIter < 100)) {
			while(numEl > maxNumElem) {
				tempEl += stepSize;
				numEl = (dist2CL*2)/tempEl;
				numIter++;
			}
			SizingFunctionArray->SetValue(i, tempEl);
			continue;
		}
		SizingFunctionArray->SetValue(i, targetArea);
		continue;
	}

	inputSurface->GetPointData()->AddArray(SizingFunctionArray);

	// Write out the new file for testing
	if(writeFile){
		savePolyData(OutputMeshfolder, outputFileName, inputSurface);
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::ComputeSurfRemesh
			(bool debug, bool writeFile, 
			bool inputData, bool reCompute,
			bool rmshCapsOnly, 
			std::string outputFileName, std::string inputFileName,
			std::string sizingArrayName, float targetEdgeLength,
			int iterations)
{
	LOG_SCOPE_F(INFO, "Surface Re-mesh");
	LOG_F(INFO, "Begining Surface Re-mesh");
	if (std::filesystem::exists
						(OutputMeshfolder.replace_filename
							(outputFileName).replace_extension(".vtp")) 
			&& 
			not (reCompute))
	{
		LOG_F(INFO, "Re-mesh file exists delete to force recompute");
		return EXIT_SUCCESS;
	}

	vtkSmartPointer<vtkPolyData> inputSurface = 
		vtkSmartPointer<vtkPolyData>::New();

	vtkSmartPointer<vtkPolyData> outputSurface = 
		vtkSmartPointer<vtkPolyData>::New();

	// Specify the cells to exclude on the boundarys
	vtkSmartPointer<vtkIdList> excludedEntityIds = 
		vtkSmartPointer<vtkIdList>::New();

	vtkNew<vtkXMLPolyDataReader> inputFile;
	inputFile->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	inputFile->Update();
	inputSurface = inputFile->GetOutput();

	double targetArea, targetAreaFactor, triangleSplitFactor, 
				 targetEdgeLengthFactor, maxArea, minArea,
				 maxEdgeLength, minEdgeLength, numConnectivityOptimizationIterations,
				 relaxation, minAreaFactor, aspectRatioThreshold, 
				 internalAngleTolerance, normalAngleTolerance, collapseAngleThreshold;
	
	int preserveBoundaryEdges;

	// Using some default params for now there should be specified by a .json for a clean API call and storage
	triangleSplitFactor = 5.0; targetEdgeLengthFactor = 1.0; 
	maxArea = 1E16; minArea=0.0; minAreaFactor = 0.5; targetAreaFactor = 1;
	maxEdgeLength=1E16; minEdgeLength=0.0; 
	numConnectivityOptimizationIterations = 20;
	aspectRatioThreshold = 1.2; internalAngleTolerance = 0.0;
	normalAngleTolerance = 0.2; collapseAngleThreshold = 0.2; 
	
	relaxation = 0.5;
	preserveBoundaryEdges = 0;

	std::string cellEntityIdsArrayName {"cellID"};


	targetArea = 0.433012701892*(std::pow(targetEdgeLength, 2));
	maxArea = 0.25*std::sqrt(3.0)*std::pow(maxEdgeLength, 2);
	minArea = 0.25*std::sqrt(3.0)*std::pow(minEdgeLength, 2);

	vtkSmartPointer<uqMeshPolyDataSurfaceRemeshing> surfRmsh =
		vtkSmartPointer<uqMeshPolyDataSurfaceRemeshing>::New();

	surfRmsh->SetInputData(inputSurface);

	// Sizing fucntion set-up
	surfRmsh->SetElementSizeModeToTargetAreaArray();
	surfRmsh->SetTargetAreaArrayName(sizingArrayName.c_str());

	surfRmsh->SetTargetArea(targetArea);
	surfRmsh->SetTargetAreaFactor(targetAreaFactor);
	surfRmsh->SetTriangleSplitFactor(triangleSplitFactor);
	surfRmsh->SetMaxArea(maxArea);
	surfRmsh->SetMinArea(minArea);
	surfRmsh->SetNumberOfIterations(iterations);
	surfRmsh->SetNumberOfConnectivityOptimizationIterations(numConnectivityOptimizationIterations);
	surfRmsh->SetRelaxation(relaxation);
	surfRmsh->SetMinAreaFactor(minAreaFactor);
	surfRmsh->SetAspectRatioThreshold(aspectRatioThreshold);
	surfRmsh->SetInternalAngleTolerance(internalAngleTolerance);
	surfRmsh->SetCollapseAngleThreshold(collapseAngleThreshold);
	surfRmsh->SetPreserveBoundaryEdges(preserveBoundaryEdges);

	// Preserve the edges that belong to the surface, and just remesh the endcaps
	if(rmshCapsOnly) {
		LOG_F(INFO, "Re-mesh will exclude walls");
		excludedEntityIds->InsertNextId(0);
		surfRmsh->SetElementSizeModeToTargetArea();
		surfRmsh->SetTargetArea(targetArea/6);
		surfRmsh->SetPreserveBoundaryEdges(preserveBoundaryEdges);
		surfRmsh->SetCellEntityIdsArrayName(cellEntityIdsArrayName.c_str());
		surfRmsh->SetExcludedEntityIds(excludedEntityIds);
	}

	// perform the actual surface remeshing operation
	surfRmsh->Update();
	outputSurface = surfRmsh->GetOutput();

	// HACK: Assigin the wall cellID tag
	if(!rmshCapsOnly) {
		vtkSmartPointer<vtkIntArray> cellID_array = vtkIntArray::New();
		cellID_array->SetNumberOfValues(surfRmsh->GetOutput()->GetNumberOfCells());
		cellID_array->FillValue(0);
		cellID_array->SetName("cellID");
		outputSurface->GetCellData()->AddArray(cellID_array);
	}

	// Clean the output data to remove extra poly-data cells and points
	vtkNew<vtkCleanPolyData> polydata_cleaner;
	polydata_cleaner->SetInputData(outputSurface);
	polydata_cleaner->Update();
	outputSurface = polydata_cleaner->GetOutput();

	if(writeFile){
		savePolyData(OutputMeshfolder, outputFileName, polydata_cleaner->GetOutput());
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::ComputeBoundaryLayer
			(bool debug, bool writeFile,
			std::string outputMeshFileName, std::string outputSurfFileName, 
			std::string inputFileName, std::string refSurfFileName)
{
	if(std::filesystem::exists(OutputMeshfolder.replace_filename(outputMeshFileName).replace_extension(".vtu")))
	{
		std::cout << "===========" << outputMeshFileName << "Exists. Delete File to force recompute===========" << std::endl;
		return EXIT_SUCCESS;
	}

	// Read in input file
	vtkNew<vtkXMLPolyDataReader> inputReader;
	inputReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	inputReader->Update();

	// Read reference surface file
	vtkNew<vtkXMLPolyDataReader> refSurfReader;
	refSurfReader->SetFileName(OutputMeshfolder.replace_filename(refSurfFileName).replace_extension(".vtp").c_str());
	refSurfReader->Update();

	vtkNew<vtkGeometryFilter> geom;
	geom->SetInputData(inputReader->GetOutput());
	geom->Update();

	// Surface projection
	vtkSmartPointer<uqMeshSurfaceProjection> surfProj;
	surfProj = vtkSmartPointer<uqMeshSurfaceProjection>::New();
	surfProj->SetInputData(geom->GetOutput());
	surfProj->SetReferenceSurface(refSurfReader->GetOutput());
	surfProj->Update();
	
	// Compute surface normals
	vtkNew<vtkPolyDataNormals> surfNorm;
	surfNorm->SetInputData(surfProj->GetOutput());
	surfNorm->SetAutoOrientNormals(true);
	surfNorm->SetFlipNormals(true);
	surfNorm->SetConsistency(true);
	surfNorm->SetComputeCellNormals(false);
	surfNorm->SplittingOff();
	surfNorm->Update();

	// Clean data
	vtkNew<vtkCleanPolyData> cleanData;
	cleanData->SetInputData(surfNorm->GetOutput());
	cleanData->Update();

	// Convert polydata to surface-mesh
	vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter> surf2Mesh =
								vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter>::New();
	surf2Mesh->SetInputData(cleanData->GetOutput());
	surf2Mesh->Update();

	// Boundary layer paramaters
	float thickness, thicknessRatio, maxThickness, subLayersRatio, localCorrectionFactor, relaxation;
	int numSubLayers, numSubsteps, innerSurfaceCellEntityId, outerSurfaceCellEntityId, sideWallCellEntityId, volumeCellEntityId;
	bool useWarpVectorMagnitudeAsThickness, constantThickness, includeSurfaceCells, includeSideWallCells, negateWarpVectors;

	// Setting boundary layer generation properties
	thickness = 1.0; thicknessRatio = 1.0; 
	
	maxThickness = 1E10;

	numSubLayers = 4; subLayersRatio = 0.65;

	numSubsteps = 2000; relaxation = 0.01; localCorrectionFactor  = 0.45;

	useWarpVectorMagnitudeAsThickness = false; constantThickness = false;

	includeSurfaceCells = false; // Include the surface triangles in the output mesh
	includeSideWallCells = true; // Include the surface triangles in the output mesh
	
	negateWarpVectors = false;

	innerSurfaceCellEntityId = 0; sideWallCellEntityId = 42;
	outerSurfaceCellEntityId = -2; 
	volumeCellEntityId = -1;

	// Genreate boundary layer
	BoundaryLayer = vtkSmartPointer<uqMeshBoundaryLayerGenerator>::New();
	BoundaryLayer->SetInputData(surf2Mesh->GetOutput());
	BoundaryLayer->SetWarpVectorsArrayName("Normals");
	BoundaryLayer->SetLayerThickness(thickness);
	BoundaryLayer->SetLayerThicknessArrayName("surfaceBeam");
	BoundaryLayer->SetLayerThicknessRatio(thicknessRatio);
	BoundaryLayer->SetMaximumLayerThickness(maxThickness);
	BoundaryLayer->SetNumberOfSubLayers(numSubLayers);
	BoundaryLayer->SetNumberOfSubsteps(numSubsteps);
	BoundaryLayer->SetRelaxation(relaxation);
	BoundaryLayer->SetLocalCorrectionFactor(localCorrectionFactor);
	BoundaryLayer->SetSubLayerRatio(subLayersRatio);
	BoundaryLayer->SetConstantThickness(constantThickness);
	BoundaryLayer->SetUseWarpVectorMagnitudeAsThickness(useWarpVectorMagnitudeAsThickness);
	BoundaryLayer->SetIncludeSurfaceCells(includeSurfaceCells);
	BoundaryLayer->SetIncludeSidewallCells(includeSideWallCells);
	BoundaryLayer->SetNegateWarpVectors(negateWarpVectors);
	BoundaryLayer->SetCellEntityIdsArrayName("cellID");
	BoundaryLayer->SetInnerSurfaceCellEntityId(innerSurfaceCellEntityId);
	BoundaryLayer->SetOuterSurfaceCellEntityId(outerSurfaceCellEntityId);
	BoundaryLayer->SetSidewallCellEntityId(sideWallCellEntityId);
	BoundaryLayer->SetVolumeCellEntityId(volumeCellEntityId);
	BoundaryLayer->Update();
	if(debug)
	{
		std::cout << "===========BoundaryLayer DEBUG Info===========" << std::endl;
	}

	if(writeFile)
	{
		saveUnstructuredGrid
			(OutputMeshfolder, outputMeshFileName, BoundaryLayer->GetOutput());
		std::filesystem::path FileName {"boundaryLayer.vtp"};

		vtkNew<vtkGeometryFilter> mesh2Surf;
		mesh2Surf->SetInputData(BoundaryLayer->GetInnerSurface());
		mesh2Surf->Update();
		savePolyData(OutputMeshfolder, outputSurfFileName, mesh2Surf->GetOutput());
	}
	return EXIT_SUCCESS;
}

int GenerateMesh::ConvertBL
			(bool debug, bool writeFile,
				bool inputData, bool reCompute,
				std::string inputFileName, std::string outputFileName)
{
	if(std::filesystem::exists(OutputMeshfolder.replace_filename(outputFileName).replace_extension(".vtu")))
	{
		std::cout << "===========" << outputFileName << "Exists. Delete File to force recompute===========" << std::endl;
		return EXIT_SUCCESS;
	}
	// Make data and procesing objects
	vtkSmartPointer<vtkUnstructuredGrid> inputMesh;
	vtkSmartPointer<vtkUnstructuredGrid> outputMesh;

	vtkSmartPointer<uqMeshUGTetra> convertMesh =
		vtkSmartPointer<uqMeshUGTetra>::New();

	// Read input mesh
	vtkNew<vtkXMLUnstructuredGridReader> inputMeshReader;
	inputMeshReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtu").c_str());
	inputMeshReader->Update();
	inputMesh = inputMeshReader->GetOutput();

	// convert the BL mesh to tets not wedges
	convertMesh->SetInputData(inputMesh);
	convertMesh->Update();
	
	if(debug)
	{
		std::cout << "===========CONVERT TO TET DEBUG Info===========" << std::endl;
	}

	if(writeFile)
	{
		saveUnstructuredGrid
			(OutputMeshfolder, outputFileName, convertMesh->GetOutput());
			std::filesystem::path FileName {"boundaryLayer.vtp"};
	}
	return EXIT_SUCCESS;

}

int GenerateMesh::ComputeVolumeMesh(
	bool debug, bool writeFile, std::string outputFileName,
	std::string inputFileName)
{
	if (std::filesystem::exists(OutputMeshfolder.replace_filename(outputFileName).replace_extension(".vtu"))) {
		std::cout << "===========" << outputFileName << "Exists. Delete File to force recompute===========" << std::endl;
		return EXIT_SUCCESS;
	}
	// Computes a tetreaheydral volume mesh from inputData

	Reader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	Reader->Update();
	vtkSmartPointer<vtkPolyData> remeshedSurfaceCappedVolumeSizingFunction = Reader->GetOutput();

	vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter> Surface2Mesh = vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter>::New();
	
	vtkSmartPointer<vtkCleanPolyData> Cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	Cleaner->SetInputData(remeshedSurfaceCappedVolumeSizingFunction);
	Cleaner->Update();
	Surface2Mesh->SetInputData(Cleaner->GetOutput());
	Surface2Mesh->Update();

	vtkSmartPointer<uqMeshTetGenWrapper> volumeMesh = vtkSmartPointer<uqMeshTetGenWrapper>::New();
	volumeMesh->SetInputConnection(Surface2Mesh->GetOutputPort());
	volumeMesh->SetPLC(1);
	volumeMesh->SetRefine(0);
	volumeMesh->SetCoarsen(0);
	volumeMesh->SetNoBoundarySplit(1);
	volumeMesh->QualityOn();
	volumeMesh->SetMinRatio(1.414);
	volumeMesh->SetMinDihedral(10.0);
	volumeMesh->SetMaxDihedral(165.0);
	volumeMesh->VarVolumeOn();
	volumeMesh->FixedVolumeOff();
	volumeMesh->SetMaxVolume(10);
	volumeMesh->SetRegionAttrib(0);
	volumeMesh->NoMergeOff();
	volumeMesh->DetectInterOff();
	volumeMesh->CheckClosureOff();
	volumeMesh->DoCheckOff();
	volumeMesh->SetOrder(1);
	volumeMesh->SetUseSizingFunction(1);
	volumeMesh->SetSizingFunctionArrayName("volSizingFN");
	volumeMesh->SetRemoveSliver(0);
	volumeMesh->SetOutputSurfaceElements(0);
	volumeMesh->SetOutputVolumeElements(1);
	volumeMesh->SetCellEntityIdsArrayName("cellID");
	volumeMesh->SetTetrahedronVolumeArrayName("TetraVolume");
	volumeMesh->Update();

	// Remove all of the wall tags from the volume mesh
	vtkSmartPointer<vtkUnstructuredGrid> mesh = volumeMesh->GetOutput();
	vtkSmartPointer<vtkIntArray> mesh_cell_ids = 
		vtkIntArray::SafeDownCast(mesh->GetCellData()->GetArray("cellID"));

	vtkSmartPointer<vtkCell> current_cell;
	int current_cell_id;

	for(vtkIdType i = 0; i<mesh->GetNumberOfCells(); i++) {
		current_cell = mesh->GetCell(i);
		current_cell_id = mesh_cell_ids->GetValue(i);
		if(current_cell_id == WALL_ENTITY_OFFSET) {
			switch (current_cell->GetCellType()) {
				case VTK_TRIANGLE:
					std::cout << "Cell " << i << 
					"In volume mesh needed to be re tagged" << std::endl;
					mesh_cell_ids->SetValue(i, -1);
					break;
				default:
					break;
			}
		}
	}

	mesh->GetCellData()->AddArray(mesh_cell_ids);	

  // Write file.
  if(writeFile)
	{
		saveUnstructuredGrid
			(OutputMeshfolder, outputFileName, mesh);
	}
	// Print debug info
	if(debug){
		std::cout << "===========VolumeMesh DEBUG Info===========" << std::endl;
	}

	return EXIT_SUCCESS;
}

int GenerateMesh::appendMesh
			(bool debug, bool writeFile,
			std::string BLMeshFileName, std::string volumeMeshFileName,
			std::string wall_msh_file_name, std::string inlet_outlet_msh_file_name,
			std::string outputFileName)			
{
	// Read in input mesh
	vtkNew<vtkXMLUnstructuredGridReader> BLMeshReader;
	BLMeshReader->SetFileName(OutputMeshfolder.replace_filename(BLMeshFileName).replace_extension(".vtu").c_str());
	BLMeshReader->Update();

	vtkNew<vtkXMLUnstructuredGridReader> volumeMeshReader;
	volumeMeshReader->SetFileName(OutputMeshfolder.replace_filename(volumeMeshFileName).replace_extension(".vtu").c_str());
	volumeMeshReader->Update();

	// Read walls and inlet outlet mesh to add the surface triangles to the final mesh
	vtkNew<vtkXMLUnstructuredGridReader> wall_reader;
	wall_reader->SetFileName(OutputMeshfolder.replace_filename(wall_msh_file_name).replace_extension(".vtu").c_str());
	wall_reader->Update();
	vtkNew<vtkXMLUnstructuredGridReader> inlet_outlet_reader;
	inlet_outlet_reader->SetFileName(OutputMeshfolder.replace_filename(inlet_outlet_msh_file_name).replace_extension(".vtu").c_str());
	inlet_outlet_reader->Update();

  vtkNew<vtkAppendFilter> appendFilter;
  appendFilter->AddInputData(BLMeshReader->GetOutput());
  appendFilter->AddInputData(volumeMeshReader->GetOutput());
  appendFilter->AddInputData(wall_reader->GetOutput());
	appendFilter->AddInputData(inlet_outlet_reader->GetOutput());
	appendFilter->Update();

	saveUnstructuredGrid
		(OutputMeshfolder, outputFileName, appendFilter->GetOutput());

	return EXIT_SUCCESS;
}

int GenerateMesh::writeDolfinMesh
			(bool debug, bool writeFile,
			bool inputData, bool reCompute,
			std::string inputFileName, std::string outputFileName)
{
	vtkNew<vtkXMLUnstructuredGridReader> volumeMeshReader;
	volumeMeshReader->SetFileName
		(OutputMeshfolder.replace_filename(inputFileName).replace_extension
			(".vtu").c_str());
	volumeMeshReader->Update();
	
	// Clean the input data to remove duplicate points
	vtkNew<vtkCleanUnstructuredGrid> mesh_cleaner;
	mesh_cleaner->SetInputData(volumeMeshReader->GetOutput());
	mesh_cleaner->Update();

	saveUnstructuredGrid(OutputMeshfolder, "cleaned_final_tet_mesh", mesh_cleaner->GetOutput());

	// Test output for dolfin xml format
	vtkSmartPointer<vtkvmtkDolfinWriter> dolfin_writer =
		vtkSmartPointer<vtkvmtkDolfinWriter>::New();
	dolfin_writer->SetInputData(mesh_cleaner->GetOutput());
	dolfin_writer->SetFileName
		(OutputMeshfolder.replace_filename
			(outputFileName).replace_extension(".xml").c_str());
	dolfin_writer->SetBoundaryDataArrayName("cellID");
	dolfin_writer->Write();

	return EXIT_SUCCESS;
}
int GenerateMesh::Surf2Mesh
			(bool debug, bool writeFile,
			bool inputData, bool reCompute,
			std::string inputFileName, std::string outputFileName)
{
	LOG_SCOPE_F(INFO, "complex_calculation");
	LOG_F(INFO, "Lighting strike!");
	vtkNew<vtkXMLPolyDataReader> surfaceReader;
	if(!inputData){
		surfaceReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	}
	else {
		surfaceReader->SetFileName(InputDataFolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	}
	surfaceReader->Update();
	vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter> surf2Mesh =
		uqMeshPolyDataToUnstructuredGridFilter::New();
	surf2Mesh->SetInputData(surfaceReader->GetOutput());
	surf2Mesh->Update();
	saveUnstructuredGrid(OutputMeshfolder, outputFileName, surf2Mesh->GetOutput());
	return EXIT_SUCCESS;
}

int GenerateMesh::extractInletOutlets
			(bool debug, bool writeFile,
			std::string inputFileName, std::string outputFileName)
{
	LOG_SCOPE_F(INFO, "ExtractInletOutlets");
	LOG_F(INFO, "Hydro Pump!");
	vtkNew<vtkXMLPolyDataReader> surfaceReader;
	surfaceReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtp").c_str());
	surfaceReader->Update();

	vtkSmartPointer<vtkPolyData> inlet_outlet_cellID = vtkPolyData::New();
	inlet_outlet_cellID = surfaceReader->GetOutput();
	vtkNew<vtkCleanPolyData> inlet_outlet_clean_poly_data;
	inlet_outlet_clean_poly_data->SetInputData(inlet_outlet_cellID);
	inlet_outlet_clean_poly_data->Update();
	vtkNew<vtkThreshold> threshold;
  threshold->SetInputData(inlet_outlet_clean_poly_data->GetOutput());
  threshold->SetUpperThreshold(1);
  threshold->SetThresholdFunction(vtkThreshold::THRESHOLD_UPPER);
  threshold->SetInputArrayToProcess(
      0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "cellID");
  threshold->Update();

	saveUnstructuredGrid(OutputMeshfolder, outputFileName, threshold->GetOutput());
	return EXIT_SUCCESS;
}

int GenerateMesh::addBLInletOutlets
			(bool debug, bool writeFile,
			std::string inputFileName, std::string outputFileName,
			std::string inletOutletFileName)
{
	LOG_SCOPE_F(INFO, "Add BL Inlet Outlet Cells to correct CellID Group");
	LOG_F(INFO, "THUNDER!");
	
	vtkSmartPointer<vtkUnstructuredGrid> volume_mesh;
	vtkNew<vtkXMLUnstructuredGridReader> volumeMeshReader;
	volumeMeshReader->SetFileName(OutputMeshfolder.replace_filename(inputFileName).replace_extension(".vtu").c_str());
	volumeMeshReader->Update();
	volume_mesh = volumeMeshReader->GetOutput();

	vtkSmartPointer<vtkCell> cell;
	double* bounds;
	double x_center, y_center, z_center;
	double x_dist, y_dist, z_dist, dist, min_dist {10000}; // min_dist is arbitrary large number
	vtkIdType cellID;
	vtkSmartPointer<vtkIntArray> cellID_array = vtkIntArray::New();
	cellID_array = vtkIntArray::SafeDownCast
		(volume_mesh->GetCellData()->GetAbstractArray("cellID"));
	// HACK: Assign BL inlet/outlet ids to sidewall cells
	for(vtkIdType i=0; i<volume_mesh->GetNumberOfCells(); i++) {
		cellID = cellID_array->GetValue(i);
		if (cellID == 42){
			cell = volume_mesh->GetCell(i);
			bounds = cell->GetBounds();
			x_center = (bounds[0] + bounds[1])/2;
			y_center = (bounds[2] + bounds[3])/2;
			z_center = (bounds[4] + bounds[5])/2;
			// Find the closest inlet/outlet center to the current cell
			// and mapp that inlet/outlet id to the current cellID
			for(vtkIdType j=0; j<inlet_outlet_centers.size(); j++) {
				x_dist = std::sqrt((inlet_outlet_centers[j][0]-x_center)*(inlet_outlet_centers[j][0]-x_center));
				y_dist = std::sqrt((inlet_outlet_centers[j][1]-y_center)*(inlet_outlet_centers[j][1]-y_center));
				z_dist = std::sqrt((inlet_outlet_centers[j][2]-z_center)*(inlet_outlet_centers[j][2]-z_center));
				dist = std::sqrt((x_dist*x_dist)+(y_dist*y_dist)+(z_dist*z_dist));
				if(dist < min_dist) {
					min_dist = dist;
					cellID = (j++);
				}
			}
			cellID_array->SetValue(i, cellID);
		}
	}
	volume_mesh->GetCellData()->AddArray(cellID_array);
	saveUnstructuredGrid(OutputMeshfolder, outputFileName, volume_mesh);
	return EXIT_SUCCESS;
}