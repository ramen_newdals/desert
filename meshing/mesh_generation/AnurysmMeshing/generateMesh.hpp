#ifndef __generateMesh_h
#define __generateMesh_h

// std headers
#include <array>
#include <iostream>
#include <tuple>
#include <vector>
#include <utility>
#include <algorithm>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <loguru.hpp>

// VTK Libraries
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkDataObject.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>
#include <vtkSmartPointer.h>
#include <vtkNew.h>
#include <vtkCellCenters.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkFeatureEdges.h>
#include <vtkGeometryFilter.h>
#include <vtkDelaunay2D.h>
#include <vtkMassProperties.h>
#include <vtkPointLocator.h>
#include <vtkTriangleFilter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkCurvatures.h>
#include <vtkOctreePointLocator.h>
#include <vtkPolyDataToUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkAppendFilter.h>
#include <vtkThreshold.h>
#include <vtkCleanUnstructuredGrid.h>
// uqMesh Headers
#include "uqMeshVoronoiDiagram3D.hpp"
#include "uqMeshSurfaceCapper.hpp"
#include "uqMeshPolyDataCenterlines.hpp"
#include "uqMeshCapPolyData.hpp"
#include "uqMeshPolyDataDistanceToCenterlines.hpp"
#include "uqMeshPolyDataSurfaceRemeshing.hpp"
#include "uqMeshSurfaceProjection.hpp"
#include "uqMeshPolyDataToUnstructuredGridFilter.hpp"
#include "uqMeshFlowExtension.hpp"
#include "uqMeshBoundaryLayerGenerator.hpp"
#include "uqMeshSimpleCapPolyData.hpp"
#include "uqMeshPolyDataSizingFunction.hpp"
#include "uqMeshTetGenWrapper.hpp"
#include "uqMeshUGTetra.hpp"

#include "vtkvmtkDolfinWriter.hpp"
#include "CONSTANTS.hpp"

// Custom defs
#ifndef uqMeshIds
#define uqMeshInletId 1
#define uqMeshOutletId 2
#endif

class GenerateMesh{
private:
	/* data */
	vtkSmartPointer<vtkIdList> TargetSeeds;
	vtkSmartPointer<vtkIdList> SourceSeeds;
	std::filesystem::path OutputMeshfolder;
	std::filesystem::path InputDataFolder;
	std::vector<std::array<double, 3>> inlet_outlet_centers;


	/* Filters */
	vtkSmartPointer<vtkXMLPolyDataReader> Reader;
	vtkSmartPointer<vtkXMLPolyDataWriter> polyDataWriter;
	vtkSmartPointer<vtkXMLUnstructuredGridWriter> unstructuredGridWriter;

	// vtkSmartPointer<uqMeshCapPolyData> SurfaceCapper;
	// vtkSmartPointer<uqMeshPolyDataCenterlines> Centerlines;
	vtkSmartPointer<uqMeshPolyDataDistanceToCenterlines> Dist2Centerlines;
	vtkSmartPointer<vtkCleanPolyData> Cleaner;
	vtkSmartPointer<vtkTriangleFilter> TriangleFilter;
	vtkSmartPointer<vtkCurvatures> CurvatureFilter;
	vtkSmartPointer<uqMeshPolyDataSurfaceRemeshing> SurfaceRemeshing;
	vtkSmartPointer<uqMeshSurfaceProjection> SurfaceProjection;
	vtkSmartPointer<vtkPolyDataNormals> SurfaceNormals;
	vtkSmartPointer<uqMeshPolyDataToUnstructuredGridFilter> Surface2Mesh;
	vtkSmartPointer<uqMeshBoundaryLayerGenerator> BoundaryLayer;
	vtkSmartPointer<vtkGeometryFilter> Mesh2Surface;
	vtkSmartPointer<uqMeshSimpleCapPolyData> CapPolyData;
	vtkSmartPointer<uqMeshPolyDataSizingFunction> SizingFunction;
	vtkSmartPointer<uqMeshTetGenWrapper> tetgen;

public:
	/* temporary storage data for testing  non VTK pipeline methods*/
	vtkSmartPointer<vtkPolyData> inputMesh;
	vtkSmartPointer<vtkPolyData> testPolyData;
	
	GenerateMesh
		(std::filesystem::path InputMeshFileName, 
		std::filesystem::path OutputFolder,
		std::filesystem::path InputFolder);
	
	~GenerateMesh
		();
	
	void savePolyData
				(std::filesystem::path fileDirectory, std::string fileName, 
				vtkPolyData *InputData);
	
	void saveUnstructuredGrid
				(std::filesystem::path fileDirectory, std::string fileName, 
				vtkUnstructuredGrid *InputData);
	
	int CapSurface
				(bool debug, bool writeFile, 
				bool inputData, bool reCompute,
				std::string outputFileName, std::string inputFileName);
	
	int ComputeCenterlines
				(bool debug, bool writeFile, 
				bool inputData, bool reCompute,
				std::string outputFileName, std::string inputFileName);
	
	int ComputeDist2Centerlines
				(bool debug, bool writeFile, 
				bool inputData, bool reCompute,
				std::string outputFileName, std::string inputFileName,
				std::string centerlinesFileName);
		
	int SpecifySizingRegions
				(bool debug, bool writeFile,
				bool inputData, bool reCompute,
				std::string outputFileName, std::string inputFileName,
				std::string sizingRegionFileName,	int sizingRegionId);

	int GenerateFlowExtensions
				(bool debug, bool writeFile,
				bool inputData, bool recompute,
				std::string outputFileName, std::string inputFileName,
				std::string centerlinesFileName);

	int SetElSizeFN
				(bool debug, bool writeFile, bool reCompute, 
				std::string outputFileName, std::string inputFileName, 
				std::string sizingFunctionArrayName, 
				int highFidelityRegionId, float coarseningFactor,
				int maxNumElem, int minNumElem,   
				float targetEdgeLength, float highFidelityEdgeLength, 
				float stepSize, int numIters);

	int HACK_SetVolElSizeFN
				(bool debug, bool writeFile, bool reCompute, 
				std::string outputFileName, std::string inputFileName, 
				std::string sizingFunctionArrayName, 
				int highFidelityRegionId, float coarseningFactor,
				int maxNumElem, int minNumElem,   
				float targetEdgeLength, float highFidelityEdgeLength, 
				float stepSize, int numIters);

	int ComputeSurfRemesh
				(bool debug, bool writeFile, 
				bool inputData, bool reCompute,
				bool rmshCapsOnly, 
				std::string outputFileName, std::string inputFileName,
				std::string sizingArrayName, float targetEdgeLength,
				int iterations=10);

	int appendSurface
				(bool debug, bool writeFile,
				std::string outputFileName, std::string surfaceCapFileName,
				std::string surfaceFileName);
	
	int ComputeVolumeMesh
				(bool debug, bool writeFile, 
				std::string outputFileName, std::string inputFileName);

	int ComputeBoundaryLayer
				(bool debug, bool writeFile,
				std::string outputMeshFileName, std::string outputSurfFileName,
				std::string inputFileName, std::string refSurfFileName);

	int ConvertBL
				(bool debug, bool writeFile,
				bool inputData, bool reCompute,
				std::string inputFileName, std::string outputFileName);

	int appendMesh
				(bool debug, bool writeFile,
				std::string boundaryLayerMesh, std::string volumeMesh,
				std::string wall_msh_file_name, std::string inlet_outlet_msh_file_name,
				std::string outputFileName);

	int writeDolfinMesh
				(bool debug, bool writeFile,
				bool inputData, bool reCompute,
				std::string inputFileName, std::string outputFileName);

	int Surf2Mesh
				(bool debug, bool writeFile,
				bool inputData, bool reCompute,
				std::string inputFileName, std::string outputFileName);

	int extractInletOutlets
				(bool debug, bool writeFile,
				std::string inputFileName, std::string outputFileName);

	int addBLInletOutlets
				(bool debug, bool writeFile,
				std::string inputFileName, std::string outputFileName,
				std::string inletOutletFileName);
};

#endif
