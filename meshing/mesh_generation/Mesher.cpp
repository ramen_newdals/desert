// STD Headders needed
#include <iostream>
#include <string>
#include <filesystem>
#include <vtkCleanPolyData.h>
#include <vtkIdList.h>
#include <vtkNew.h>
#include <vtkRemovePolyData.h>
#include <loguru.hpp>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>

// Custom classes 
#include "generateMesh.hpp"
#include "uqMeshFlowExtension.hpp"
#include "uqMeshVoronoiDiagram3D.hpp"
#include "uqMeshSurfaceCapper.hpp"
#include "uqMeshPolyDataCenterlines.hpp"
#include "uqMeshCapPolyData.hpp"
#include "uqMeshPolyDataDistanceToCenterlines.hpp"
#include "uqMeshPolyDataSurfaceRemeshing.hpp"
#include "uqMeshSurfaceProjection.hpp"
#include "uqMeshPolyDataToUnstructuredGridFilter.hpp"
#include "uqMeshBoundaryLayerGenerator.hpp"
#include "uqMeshSimpleCapPolyData.hpp"
#include "uqMeshPolyDataSizingFunction.hpp"
#include "uqMeshTetGenWrapper.hpp"
#include "uqMeshUGTetra.hpp"
#include "InteractiveRender.hpp"
#include "profile.hpp"

void generatSimpleArteryMesh(
		std::filesystem::path InputMeshFilePath, std::filesystem::path OutputMeshFolder,
		std::filesystem::path InputDataFolder, std::filesystem::path HighFidelityMeshPath, 
		float highFidelityEdgeLength, float targetEdgeLength,
		std::string inputFileName)
{
	int error {0};

	vtkSmartPointer<vtkPolyData> remeshedSurfaceCapped;
	vtkSmartPointer<vtkPolyData> remeshedSurfaceCappedVolumeSizingFunction;
	GenerateMesh meshFunctions(InputMeshFilePath, OutputMeshFolder,
														 InputDataFolder);
	
	// File names for each stage
	std::string capedSurfaceFileName {"cappedSurface"}, centerlinesFileName {"centerlines"}, 
							sizingRegionsFileName {"sizingRegion"}, surfaceSizingFNFileName {"surfSizingFN"}, 
							rmshSurfaceFileName {"RMSHSurface"}, dist2CLFileName {"dist2CL"},
							boundaryLayerFileName {"boundaryLayer"}, BLTetsFileName {"BLTets"},
							boundaryLayerInnerSurfaceFileName {"BLInnerSurf"}, 
							capedBLInnerSurfFileName {"capedBLInnerSurf"}, rmshCapedBLInnerSurfFileName {"RMSHCapedBLInnerSurf"}, 
							volumeSizingFNFileName {"volSizingFN"}, 
							volumeMeshFileName {"volMSH"}, finalMeshFileName {"finalMSH"};

	bool recompute {true}, no_recompute {false};
	bool debug {true}, no_debug {false};
	bool save_output {true}, no_save_output {false};
	bool input_data {true}, output_data {false};
	// Mesh data array names
	std::string sizingFunctionArrayName {"surfaceBeam"};

	error = meshFunctions.CapSurface(debug, save_output, 
																	 input_data, no_recompute,
																	 capedSurfaceFileName, inputFileName);
	error = meshFunctions.ComputeCenterlines(debug, save_output,
																					output_data, no_recompute,
																					centerlinesFileName, capedSurfaceFileName);

	error = meshFunctions.SpecifySizingRegions(debug, save_output, 
																						input_data, no_recompute,
																						sizingRegionsFileName, inputFileName,
																						HighFidelityMeshPath, 69);

	error = meshFunctions.ComputeDist2Centerlines(debug, save_output,
																							 output_data, no_recompute,
																							 dist2CLFileName, sizingRegionsFileName,
																							 centerlinesFileName);

	error = meshFunctions.SetElSizeFN(debug, save_output,
																		no_recompute,
																		surfaceSizingFNFileName, dist2CLFileName, 
																		sizingFunctionArrayName, 
																		69, 1.5,
																		20, 3,
																		targetEdgeLength, highFidelityEdgeLength,
																		0.05, 100);

	error = meshFunctions.ComputeSurfRemesh(debug, save_output, 
																					output_data, no_recompute,
																					false, // HACK - rmsh end caps only 
																					rmshSurfaceFileName, surfaceSizingFNFileName, 
																					sizingFunctionArrayName, targetEdgeLength, 
																					10);

	error = meshFunctions.GenerateFlowExtensions(debug, save_output, output_data, 
																							recompute, "DEBUG_FLOW_EXTENSIONS", rmshSurfaceFileName,
																							centerlinesFileName);

	// Cap flow-extensions surface
	error = meshFunctions.CapSurface(debug, save_output, 
																	output_data, recompute,
																	"DEBUG_CAPPED_FLOW_EXTENSIONS", "DEBUG_FLOW_EXTENSIONS");

	// Generate new centerlines with the flow extensions
	error = meshFunctions.ComputeCenterlines(debug, save_output,
																					output_data, no_recompute,
																					"DEBUG_CAPPED_FLOW_EXTENSION_CENTER_LINES", "DEBUG_CAPPED_FLOW_EXTENSIONS");


	error = meshFunctions.Surf2Mesh(debug, save_output, output_data, no_recompute, "DEBUG_FLOW_EXTENSIONS", "DEBUG_0x6");

	error = meshFunctions.ComputeDist2Centerlines(debug, save_output,
																							 output_data, recompute,
																							 dist2CLFileName, "DEBUG_FLOW_EXTENSIONS",
																							 "DEBUG_CAPPED_FLOW_EXTENSION_CENTER_LINES");

	error = meshFunctions.SpecifySizingRegions(debug, save_output, 
																						output_data, recompute,
																						"rmsh_sz_region", dist2CLFileName,
																						HighFidelityMeshPath, 69);
	// Surface-rmsh uses target triangle area
	error = meshFunctions.SetElSizeFN(debug, save_output, 
																		recompute,
																		"bl_thickness", "rmsh_sz_region", 
																		sizingFunctionArrayName,
																		69, 1.5,
																		20, 3,
																		targetEdgeLength,
																		highFidelityEdgeLength,
																		0.05, 100);

	error = meshFunctions.ComputeBoundaryLayer(debug, save_output,
																						 boundaryLayerFileName, boundaryLayerInnerSurfaceFileName,
																						 "bl_thickness", capedSurfaceFileName);

	error = meshFunctions.CapSurface(debug, save_output,
																	output_data, recompute,
																	capedBLInnerSurfFileName, boundaryLayerInnerSurfaceFileName);

	error = meshFunctions.ComputeSurfRemesh(debug, save_output, 
																					output_data, recompute,
																					true, // HACK: rmsh endcaps only
																					rmshCapedBLInnerSurfFileName, capedBLInnerSurfFileName, 
																					sizingFunctionArrayName, targetEdgeLength, 
																					10);

	error = meshFunctions.extractInletOutlets(debug, save_output, rmshCapedBLInnerSurfFileName, "DEBUG_0x7");

	error = meshFunctions.ComputeDist2Centerlines(debug, save_output,
																							 input_data, recompute,
																							 "remshDist2CL", rmshCapedBLInnerSurfFileName,
																							 centerlinesFileName);

	error = meshFunctions.SpecifySizingRegions(debug, save_output, 
																						output_data, recompute,
																						"dist2CLSizingRegion", "remshDist2CL",
																						HighFidelityMeshPath, 69);

	// tetgen uses target edge length
	error = meshFunctions.HACK_SetVolElSizeFN(debug, save_output, 
																		output_data,
																		"vol_el_size_fn", "dist2CLSizingRegion", 
																		volumeSizingFNFileName, 
																		69, 1.5,
																		20, 3,
																		targetEdgeLength,
																		highFidelityEdgeLength,
																		0.05, 100);

	error = meshFunctions.ComputeVolumeMesh(debug, save_output,
																					volumeMeshFileName, "vol_el_size_fn");

	error = meshFunctions.appendMesh(debug, save_output,
																	boundaryLayerFileName, volumeMeshFileName,
																	"DEBUG_0x6", "DEBUG_0x7",
																	finalMeshFileName);

	error = meshFunctions.ConvertBL(debug, save_output,
																 output_data, recompute,
																 finalMeshFileName, "tetra_volume_mesh");

	error = meshFunctions.addBLInletOutlets(debug, save_output, 
																				 "tetra_volume_mesh", "tagged_tetra_volume_mesh",
																				 "DEBUG_0x7");
	
	error = meshFunctions.writeDolfinMesh(debug, save_output,
																				output_data, recompute ,
																				"tagged_tetra_volume_mesh", "dolfin_mesh");
}

int clean_test_output(std::filesystem::path outputPath)
{
	std::filesystem::remove_all(outputPath);
	std::filesystem::create_directories(outputPath);
	return EXIT_SUCCESS;
}

int main(int argc, char *argv[]){
	loguru::init(argc, argv);
	loguru::add_file("everything.log", loguru::Append, loguru::Verbosity_MAX);
	LOG_F(INFO, "Hello log file!");

	clean_test_output("/home/ramen_newdals/projects/desert/meshing/tests/c0003/output/");

	generatSimpleArteryMesh
		("/home/ramen_newdals/projects/desert/meshing/tests/c0003/input/c0003.vtp",
		"/home/ramen_newdals/projects/desert/meshing/tests/c0003/output/",
		"/home/ramen_newdals/projects/desert/meshing/tests/c0003/input/",
		"/home/ramen_newdals/projects/desert/meshing/tests/c0003/input/c0003_hf_region.vtp",
		0.85,
		1.0,
		"c0003.vtp");
	return EXIT_SUCCESS;
}
