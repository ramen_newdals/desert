#include "triangle_ops.hpp"
#include "profile.hpp"
#include <iostream>

void TEST_VTK_TriangleArea(double v1[3], double v2[3], double v3[3])
{
	double area;
	area = VTK_TriangleArea(v1, v2, v3);
	std::cout << "Area is: " << area << std::endl;
}

int main(int argv, char *argc[]){
	std::cout << "Computing unit triangle area using VTK..." << std::endl;
	Timer t;
	double v1[3] {0, 0 , 0}, 
				 v2[3] {1, 0 , 0},
				 v3[3] {0, 1 , 0};

	TEST_VTK_TriangleArea(v1, v2, v3);
	std::cout << "VTK_TriangleArea Took: " << t.elapsed() << " (s)" << std::endl;
	return 0;
}