#include <math.h>

double triangle_area
	(double v1[3], double v2[3], double v3[3], double n[3])
{
  double ax = v3[0] - v2[0];
  double ay = v3[1] - v2[1];
  double az = v3[2] - v2[2];
  double bx = v1[0] - v2[0];
  double by = v1[1] - v2[1];
  double bz = v1[2] - v2[2];

  n[0] = (ay * bz - az * by);
  n[1] = (az * bx - ax * bz);
  n[2] = (ax * by - ay * bx);

	return std::sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
}

inline void VTK_ComputeNormalDirection(
  const double v1[3], const double v2[3], const double v3[3], double n[3])
{
  // order is important!!! maintain consistency with triangle vertex order
  double ax = v3[0] - v2[0];
  double ay = v3[1] - v2[1];
  double az = v3[2] - v2[2];
  double bx = v1[0] - v2[0];
  double by = v1[1] - v2[1];
  double bz = v1[2] - v2[2];

  n[0] = (ay * bz - az * by);
  n[1] = (az * bx - ax * bz);
  n[2] = (ax * by - ay * bx);
}

static double VTK_Norm(const double v[3])
{
	return std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

inline double VTK_TriangleArea(const double p1[3], const double p2[3], const double p3[3])
{
  double n[3];
  VTK_ComputeNormalDirection(p1, p2, p3, n);

  return 0.5 * VTK_Norm(n);
}
