#include "geoIntrin.hpp"

int IsPointOnBoundary
			(vtkIdType pointId, vtkPolyData* Mesh)
{
	vtkIdType ncells;
	vtkIdType* cells;
	Mesh->GetPointCells(pointId,ncells,cells);
	vtkIdList* cellEdgeNeighbors = vtkIdList::New();
	for(vtkIdType i=0; i<ncells; i++)
	{
		vtkIdType npts;
		const vtkIdType *pts;
		Mesh->GetCellPoints(cells[i],npts,pts);
		for(int j=0; j<npts; j++)
		{
			if(pts[j] == pointId)
			{
				continue;
			}
			if(Mesh->IsEdge(pts[j],pointId))
			{
				Mesh->GetCellEdgeNeighbors(cells[i],pts[j],pointId,cellEdgeNeighbors);
				if(cellEdgeNeighbors->GetNumberOfIds() == 0)
				{
					cellEdgeNeighbors->Delete();
					return 1;
				}
			}
		}
	}
	cellEdgeNeighbors->Delete();
	return 0;
}

int IsPointOnEntityBoundary
			(vtkIdType pointId, vtkPolyData* Mesh,
			vtkIntArray* CellEntityIdsArray)
{
	vtkIdList* ptCells = vtkIdList::New();
	Mesh->GetPointCells(pointId,ptCells);
	vtkIdType nptCells = ptCells->GetNumberOfIds();
	vtkIdType neighborhoodCellEntityId = -1;
	bool uniformCellEntityIds = true;
	for(int i=0; i<nptCells; i++){
		vtkIdType cellEntityId = CellEntityIdsArray->GetValue(ptCells->GetId(i)); 
		if(cellEntityId == -1)
		{
			continue;
		}
		if(neighborhoodCellEntityId == -1)
		{
			neighborhoodCellEntityId = cellEntityId;
			continue;
		}
		if(neighborhoodCellEntityId == cellEntityId)
		{
			continue;
		}
		uniformCellEntityIds = false;
		break;
	}

	if(!uniformCellEntityIds)
	{
		return 1;
	}
	ptCells->Delete();
	return 0;
}

int SplitEdge
			(vtkIdType pt1, vtkIdType pt2,
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray,
			vtkIdList* ExcludedEntityIds, int PreserveBoundaryEdges)
{
	vtkIdType cell1, cell2, pt3, pt4;
	int success = GetEdgeCellsAndOppositeEdge(
									pt1,pt2,
									cell1,cell2,
									pt3,pt4,
									Mesh, CellEntityIdsArray);

	if(IsElementExcluded(cell1, CellEntityIdsArray,ExcludedEntityIds) || 
		 IsElementExcluded(cell2, CellEntityIdsArray, ExcludedEntityIds))
	{
		return EDGE_LOCKED;
	}

	bool proceed = false;

  if(success == SUCCESS){
		proceed = true;
	}

	if(PreserveBoundaryEdges && success == EDGE_ON_BOUNDARY){
		proceed = true;
	}

	if(success == EDGE_BETWEEN_ENTITIES){
		proceed = true;
	}

	if(!proceed){
		return success;
	}

	double point1[3], point2[3];
	Mesh->GetPoint(pt1,point1);
	Mesh->GetPoint(pt2,point2);

	double newPoint[3];
	newPoint[0] = 0.5 * (point1[0] + point2[0]);
	newPoint[1] = 0.5 * (point1[1] + point2[1]);
	newPoint[2] = 0.5 * (point1[2] + point2[2]);

	vtkCellLocator* EntityBoundaryLocator;
	vtkCellLocator* Locator;
	vtkCellLocator* BoundaryLocator;

	if(success != EDGE_ON_BOUNDARY)
	{
		double projectedNewPoint[3];
		vtkIdType cellId;
		int subId;
		double dist2;

		if(success == EDGE_BETWEEN_ENTITIES)
		{
			EntityBoundaryLocator->
				FindClosestPoint(newPoint,projectedNewPoint,cellId,subId,dist2);
		}
		else
		{
			Locator->FindClosestPoint(newPoint,projectedNewPoint,cellId,subId,dist2);
		}
		vtkIdType newpt = Mesh->InsertNextLinkedPoint(projectedNewPoint,2);

		Mesh->ReplaceCellPoint(cell1,pt2,newpt);
		Mesh->ReplaceCellPoint(cell2,pt2,newpt);
  
		Mesh->RemoveReferenceToCell(pt2,cell1);
		Mesh->RemoveReferenceToCell(pt2,cell2);
  
		Mesh->AddReferenceToCell(newpt,cell1);
		Mesh->AddReferenceToCell(newpt,cell2);
  
		vtkIdType tri[3];
		tri[0] = pt3;
		tri[1] = newpt;
		tri[2] = pt2;
		vtkIdType cell3 = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
		CellEntityIdsArray->InsertValue(cell3, CellEntityIdsArray->GetValue(cell1));

		tri[0] = pt2;
		tri[1] = newpt;
		tri[2] = pt4;
		vtkIdType cell4 = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
		CellEntityIdsArray->InsertValue(cell4, CellEntityIdsArray->GetValue(cell2));
	}
	else
	{
		double projectedNewPoint[3];
		vtkIdType cellId;
		int subId;
		double dist2;

		if(BoundaryLocator)
		{
			BoundaryLocator->
				FindClosestPoint(newPoint,projectedNewPoint,cellId,subId,dist2);
		}
		else
		{
			std::cout <<"Something's wrong: point on boundary but no BoundaryLocator is allocated." << std::endl;
			projectedNewPoint[0] = newPoint[0];
			projectedNewPoint[1] = newPoint[1];
			projectedNewPoint[2] = newPoint[2];
		}
		vtkIdType newpt = Mesh->InsertNextLinkedPoint(projectedNewPoint,2);
  
		Mesh->ReplaceCellPoint(cell1,pt2,newpt);
		Mesh->RemoveReferenceToCell(pt2,cell1);
		Mesh->AddReferenceToCell(newpt,cell1);
  
		vtkIdType tri[3];
		tri[0] = pt3;
		tri[1] = newpt;
		tri[2] = pt2;
		vtkIdType cell3 = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
		CellEntityIdsArray->InsertValue(cell3,CellEntityIdsArray->GetValue(cell1));
	}

	return SUCCESS;
}

int GetEdgeCellsAndOppositeEdge
			(vtkIdType pt1, vtkIdType pt2, 
			vtkIdType& cell1, vtkIdType& cell2, 
			vtkIdType& pt3, vtkIdType& pt4, 
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray)
{
	cell1 = cell2 = -1;
	pt3 = pt4 = -1;

	// Check if the two points form an edge
	if (Mesh->IsEdge(pt1,pt2)){
		return NOT_EDGE;;
	}


	vtkIdList* cellIds = vtkIdList::New();
	Mesh->GetCellEdgeNeighbors(-1,pt1,pt2,cellIds);

	int numberOfCellEdgeNeighbors = cellIds->GetNumberOfIds();
	int numberOfNeighborTriangles = 0;

	for(int i=0; i<numberOfCellEdgeNeighbors; i++){
		vtkIdType cellId = cellIds->GetId(i);
		if(Mesh->GetCellType(cellId) == VTK_TRIANGLE){
			if(numberOfNeighborTriangles==0){
				cell1 = cellId;
			}
			else if(numberOfNeighborTriangles==1){
				cell2 = cellId;
			}
			numberOfNeighborTriangles++;
		}
	}

	cellIds->Delete();

	if(numberOfNeighborTriangles == 0){
		return NOT_TRIANGLES;
	}
	else if(numberOfNeighborTriangles == 1){
		vtkIdType npts;
		const vtkIdType *pts;
		Mesh->GetCellPoints(cell1,npts,pts);
		for(int i=0; i<3; i++){
			if ((pts[i]==pt1 && pts[(i+1)%3]==pt2) || (pts[i]==pt2 && pts[(i+1)%3]==pt1)){
				pt3 = pts[(i+2)%3];
				break;
			}
		}
		return EDGE_ON_BOUNDARY;
	}
	else if(numberOfNeighborTriangles > 2){
		return NON_MANIFOLD;
	}

	vtkIdType pt3tmp = -1;
	vtkIdType pt4tmp = -1;
	bool reverse = false;
	vtkIdType npts;
	const vtkIdType *pts;
	Mesh->GetCellPoints(cell1,npts,pts);
	for(int i=0; i<3; i++){
		if((pts[i]==pt1 && pts[(i+1)%3]==pt2) || (pts[i]==pt2 && pts[(i+1)%3]==pt1)){
			pt3tmp = pts[(i+2)%3];
			if (pts[i]==pt2 && pts[(i+1)%3]==pt1){
				reverse = true;
			}
			break;
		}
	}

	Mesh->GetCellPoints(cell2,npts,pts);
	for(int i=0; i<3; i++){
		if((pts[i]==pt1 && pts[(i+1)%3]==pt2) || (pts[i]==pt2 && pts[(i+1)%3]==pt1)){
			pt4tmp = pts[(i+2)%3];
			break;
		}
	}

	if(pt3tmp==-1 || pt4tmp==-1){
		return DEGENERATE_TRIANGLES;
	}

	pt3 = pt3tmp;
	pt4 = pt4tmp;
    
	if(reverse){
		vtkIdType tmp;
		tmp = pt3;
		pt3 = pt4;
		pt4 = tmp;
		tmp = cell1;
		cell1 = cell2;
		cell2 = tmp;
	}

	if(CellEntityIdsArray->GetValue(cell1) != CellEntityIdsArray->GetValue(cell2)){
		return EDGE_BETWEEN_ENTITIES;
	}

	return SUCCESS;
}

int IsElementExcluded
			(vtkIdType cellId, vtkIdList* ExcludedEntityIds,
			vtkIntArray* CellEntityIdsArray)
{
	int isElementExcluded = 0;
	if(ExcludedEntityIds == NULL){
		return isElementExcluded;
	}

	vtkIdType	cellEntityId = CellEntityIdsArray->GetValue(cellId);
	if(ExcludedEntityIds->IsId(cellEntityId) != -1){
		isElementExcluded = 1;
	}
	return isElementExcluded;
}

int CollapseEdge
			(vtkIdType pt1, vtkIdType pt2, 
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray,
			vtkIdList* ExcludedEntityIds, int PreserveBoundaryEdges)
{
	vtkIdType cell1, cell2, pt3, pt4;
	int success = GetEdgeCellsAndOppositeEdge(pt1,pt2,cell1,cell2,pt3,pt4, Mesh, CellEntityIdsArray);

	if(IsElementExcluded(cell1, CellEntityIdsArray, ExcludedEntityIds) ||
		 IsElementExcluded(cell2, CellEntityIdsArray, ExcludedEntityIds))
	{
		return EDGE_LOCKED;
	}

	bool proceed = false;

	if(success == SUCCESS){
		proceed = true;
	}

	if(PreserveBoundaryEdges && success == EDGE_ON_BOUNDARY)
	{
		proceed = true;
	}

	if(success == EDGE_BETWEEN_ENTITIES)
	{
		proceed = true;
	}

	if(!proceed)
	{
		return success;
	}

	if(success != EDGE_ON_BOUNDARY)
	{
		bool swappedForBoundary = false;
		int pt1OnBoundary = IsPointOnBoundary(pt1, Mesh);
		int pt2OnBoundary = IsPointOnBoundary(pt2, Mesh);
		if(!pt1OnBoundary && pt2OnBoundary)
		{
			vtkIdType tmp = pt1;
			pt1 = pt2;
			pt2 = tmp;
			swappedForBoundary = true;
		}
 
		int pt1OnEntityBoundary = IsPointOnEntityBoundary(pt1, Mesh, CellEntityIdsArray);
		int pt2OnEntityBoundary = IsPointOnEntityBoundary(pt2, Mesh, CellEntityIdsArray);
		if(!pt1OnEntityBoundary && pt2OnEntityBoundary)
		{
      if(swappedForBoundary){
				return EDGE_LOCKED;
			}
			vtkIdType tmp = pt1;
			pt1 = pt2;
			pt2 = tmp;
		}

		vtkIdList* pt2Cells = vtkIdList::New();
		Mesh->GetPointCells(pt2,pt2Cells);
		vtkIdType npt2Cells = pt2Cells->GetNumberOfIds();
    
		Mesh->RemoveReferenceToCell(pt1,cell1);
		Mesh->RemoveReferenceToCell(pt1,cell2);
		Mesh->RemoveReferenceToCell(pt3,cell1);
		Mesh->RemoveReferenceToCell(pt4,cell2);
		Mesh->DeletePoint(pt2);
		Mesh->DeleteCell(cell1); 
		CellEntityIdsArray->InsertValue(cell1,-1);
		Mesh->DeleteCell(cell2); 
		CellEntityIdsArray->InsertValue(cell2,-1);
  
		Mesh->ResizeCellList(pt1,npt2Cells-2);
  
		for(int i=0; i<npt2Cells; i++)
		{
			vtkIdType pt2Cell = pt2Cells->GetId(i);
			if(pt2Cell == cell1 || pt2Cell == cell2){
				continue;
			}
			Mesh->AddReferenceToCell(pt1,pt2Cell);
			Mesh->ReplaceCellPoint(pt2Cell,pt2,pt1);
		}

		pt2Cells->Delete();
	}
	else
	{
		vtkIdList* pt2Cells = vtkIdList::New();
		Mesh->GetPointCells(pt2,pt2Cells);
		vtkIdType npt2Cells = pt2Cells->GetNumberOfIds();
    
		Mesh->RemoveReferenceToCell(pt1,cell1);
		Mesh->RemoveReferenceToCell(pt3,cell1);
		Mesh->DeletePoint(pt2);
		Mesh->DeleteCell(cell1); 
		CellEntityIdsArray->InsertValue(cell1,-1);
  
		Mesh->ResizeCellList(pt1,npt2Cells-1);
  
		for(int i=0; i<npt2Cells; i++)
		{
			vtkIdType pt2Cell = pt2Cells->GetId(i);
			if(pt2Cell == cell1){
				continue;
			}
			Mesh->AddReferenceToCell(pt1,pt2Cell);
			Mesh->ReplaceCellPoint(pt2Cell,pt2,pt1);
		}

		pt2Cells->Delete();
	}

	return SUCCESS;
}

int FlipEdge
			(vtkIdType pt1, vtkIdType pt2,
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray,
			vtkIdList* ExcludedEntityIds)
{
	vtkIdType cell1, cell2, pt3, pt4;
	int success = GetEdgeCellsAndOppositeEdge(pt1, pt2, 
																						cell1, cell2, 
																						pt3, pt4, 
																						Mesh, CellEntityIdsArray);

	if(IsElementExcluded(cell1, CellEntityIdsArray, ExcludedEntityIds) || 
		 IsElementExcluded(cell2, CellEntityIdsArray, ExcludedEntityIds))
	{
		return EDGE_LOCKED;
	}

	if(success != SUCCESS){
		return success;
	}

	Mesh->RemoveCellReference(cell1);
	Mesh->RemoveCellReference(cell2);
	Mesh->DeleteCell(cell1); 
	vtkIdType cell1EntityId = CellEntityIdsArray->GetValue(cell1);
	CellEntityIdsArray->InsertValue(cell1,-1);
	Mesh->DeleteCell(cell2); 
	vtkIdType cell2EntityId = CellEntityIdsArray->GetValue(cell2);
	CellEntityIdsArray->InsertValue(cell2,-1);

	vtkIdType tri[3];
	tri[0] = pt3;
	tri[1] = pt1;
	tri[2] = pt4;
	vtkIdType cell1b = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
	CellEntityIdsArray->InsertValue(cell1b,cell1EntityId);
	tri[0] = pt2;
	tri[1] = pt3;
	tri[2] = pt4;
	vtkIdType cell2b = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
	CellEntityIdsArray->InsertValue(cell2b,cell2EntityId);

	vtkIdType pt3b, pt4b;
	if(GetEdgeCellsAndOppositeEdge(pt3,pt4,
																cell1,cell2,
																pt3b,pt4b,
																Mesh, CellEntityIdsArray) != SUCCESS){
		Mesh->RemoveCellReference(cell1b);
		Mesh->RemoveCellReference(cell2b);
		Mesh->DeleteCell(cell1b);
		CellEntityIdsArray->InsertValue(cell1b,-1);
		Mesh->DeleteCell(cell2b);
		CellEntityIdsArray->InsertValue(cell2b,-1);
		tri[0] = pt1;
		tri[1] = pt2;
		tri[2] = pt3;
		vtkIdType cell1c = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
		CellEntityIdsArray->InsertValue(cell1c,cell1EntityId);
		tri[0] = pt1;
		tri[1] = pt4;
		tri[2] = pt2;
		vtkIdType cell2c = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
		CellEntityIdsArray->InsertValue(cell2c,cell2EntityId);
	}

	return SUCCESS;
}

int SplitTriangle
			(vtkIdType cellId, vtkPolyData* Mesh,
			vtkIntArray* CellEntityIdsArray, vtkIdList* ExcludedEntityIds)
{
	if(Mesh->GetCellType(cellId) != VTK_TRIANGLE){
		return NOT_TRIANGLES;
	}

	if(IsElementExcluded(cellId, CellEntityIdsArray, ExcludedEntityIds)){
		return TRIANGLE_LOCKED;
	}

	vtkIdType npts;
	const vtkIdType *pts;
	Mesh->GetCellPoints(cellId,npts,pts);
	vtkIdType pt1 = pts[0];
	vtkIdType pt2 = pts[1];
	vtkIdType pt3 = pts[2];

	vtkIdType cellEntityId = CellEntityIdsArray->GetValue(cellId);

	double point1[3], point2[3], point3[3], newPoint[3];
	Mesh->GetPoint(pt1,point1);
	Mesh->GetPoint(pt2,point2);
	Mesh->GetPoint(pt3,point3);
	vtkTriangle::TriangleCenter(point1,point2,point3,newPoint);

	double projectedNewPoint[3];
	vtkIdType closestCellId;
	int subId;
	double dist2;
	vtkCellLocator* Locator;
	Locator->FindClosestPoint(newPoint,projectedNewPoint,closestCellId,subId,dist2);
 
	vtkIdType newpt = Mesh->InsertNextLinkedPoint(projectedNewPoint,1);

	Mesh->ReplaceCellPoint(cellId,pt3,newpt);
	Mesh->RemoveReferenceToCell(pt3,cellId);
	Mesh->AddReferenceToCell(newpt,cellId);

	vtkIdType tri[3];
	tri[0] = pt2;
	tri[1] = pt3;
	tri[2] = newpt;
	vtkIdType cell1 = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
	CellEntityIdsArray->InsertValue(cell1,cellEntityId);

	tri[0] = pt3;
	tri[1] = pt1;
	tri[2] = newpt;
	vtkIdType cell2 = Mesh->InsertNextLinkedCell(VTK_TRIANGLE,3,tri);
	CellEntityIdsArray->InsertValue(cell2,cellEntityId);

	//cout<<cell1<<" "<<cell2<<" "<<numberOfCells<<endl;

	return SUCCESS;
}

int CollapseTriangle(vtkIdType cellId){
	std::cout << "ERROR: CollapseTriangle not yet implemented." << std::endl;
	return -1;
}

double ComputeTriangleTargetArea(vtkIdType cellId, vtkPolyData* Mesh,
																 int ElementSizeMode, double TargetArea,
																 double MinArea, double MaxArea,
																 double TargetAreaFactor, vtkDoubleArray* TargetAreaArray)
{
	double targetArea = 0.0;
	vtkCellLocator* Locator;
	if(ElementSizeMode == TARGET_AREA)
	{
		targetArea = TargetArea;
	}
	else if(ElementSizeMode == TARGET_AREA_ARRAY)
	{
		vtkIdType npts;
		const vtkIdType *pts;
		Mesh->GetCellPoints(cellId,npts,pts);
		double point1[3], point2[3], point3[3];
		Mesh->GetPoint(pts[0],point1);
		Mesh->GetPoint(pts[1],point2);
		Mesh->GetPoint(pts[2],point3);
		double center[3];
		vtkTriangle::TriangleCenter(point1,point2,point3,center);
		double projectedCenter[3];
		vtkIdType centerCellId;
		int subId;
		double dist2;
		Locator->FindClosestPoint(center,projectedCenter,centerCellId,subId,dist2);
		vtkTriangle* centerCell = vtkTriangle::SafeDownCast(
															vtkPolyData::SafeDownCast(
																Locator->GetDataSet())->
																	GetCell(centerCellId));
		double pcoords[3], weights[3];
		centerCell->EvaluatePosition(projectedCenter,NULL,subId,pcoords,dist2,weights);
		for(int i=0; i<3; i++)
		{
			targetArea += weights[i] * TargetAreaArray->GetTuple1(centerCell->GetPointId(i));
		}
		targetArea *= TargetAreaFactor;
	}
	else
	{
		std::cout << "ERROR: ElementSizeMode specified is unknown" << std::endl;
		return 0.0;
	}
	if(targetArea > MaxArea)
	{
		targetArea = MaxArea;
	}
	if(targetArea < MinArea)
	{
		targetArea = MinArea;
	}
	return targetArea;
}
