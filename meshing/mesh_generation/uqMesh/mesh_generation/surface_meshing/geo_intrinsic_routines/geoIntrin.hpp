#ifndef __geo_intrinsic_routines_h
#define __geo_intrinsic_routines_h
#include <vtkPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkIntArray.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>
#include <vtkCellLocator.h>

#include <iostream>

// 0D Functions
int IsPointOnBoundary
			(vtkIdType pointId, vtkPolyData* Mesh);
int IsPointOnEntityBoundary
			(vtkIdType pointId, vtkPolyData* Mesh,
			vtkIntArray* CellEntityIdsArray);

// 1D Functions
int SplitEdge
			(vtkIdType pt1, vtkIdType pt2,
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray,
			vtkIdList* ExcludedEntityIds, int PreserveBoundaryEdges);
int CollapseEdge
			(vtkIdType pt1, vtkIdType pt2, 
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray);
int  FlipEdge
			(vtkIdType pt1, vtkIdType pt2);

// 2D Functions
int SplitTriangle
			(vtkIdType cellId);
int CollapseTriangle
			(vtkIdType cellId);
double ComputeTriangleTargetArea
			(vtkIdType cellId);

// Mesh Functions
int GetEdgeCellsAndOppositeEdge
			(vtkIdType pt1, vtkIdType pt2,
			vtkIdType& cell1, vtkIdType& cell2, 
			vtkIdType& pt3, vtkIdType& pt4, 
			vtkPolyData* Mesh, vtkIntArray* CellEntityIdsArray);
int IsElementExcluded
			(vtkIdType cellId, vtkIntArray* CellEntityIdsArray, 
			vtkIdList* ExcludedEntityIds);

enum errCode 
{
  SUCCESS,
  EDGE_ON_BOUNDARY,
  EDGE_BETWEEN_ENTITIES,
  EDGE_LOCKED,
  NOT_EDGE,
  NON_MANIFOLD,
  NOT_TRIANGLES,
  DEGENERATE_TRIANGLES,
  TRIANGLE_LOCKED
};

enum sizeMode
{
  TARGET_AREA,
  TARGET_AREA_ARRAY
};

#endif