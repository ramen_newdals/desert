// #include "remshSurf.hpp"

// int remshSurf
// 			(int sizeMode,
// 			std::vector<float> targetAreaArray, std::vector<long long> cellEntityID)
// {
// 	if(sizeMode == TARGET_AREA_ARRAY)
// 	{
//     if(TargetAreaArrayName.empty())
// 		{
//       std::cout << "TargetAreaArrayName not specified" << std::endl;
//       return 1;
//     }
// 		if(targetAreaArray.empty())
// 		{
// 			std::cout <<"TargetAreaArray with name specified does not exist" << std::endl;
// 			return 1;
// 		}
// 	}

// 	this->CellEntityIdsArray->SetNumberOfValues(input->GetNumberOfCells());
// 	this->CellEntityIdsArray->FillComponent(0,0.0);

// 	vtkPolyDataNormals* normals = vtkPolyDataNormals::New();
// 	normals->SetInputData(input);
// 	normals->ComputePointNormalsOff();
// 	normals->ComputeCellNormalsOn();
// 	normals->AutoOrientNormalsOn();
// 	normals->ConsistencyOn();
// 	normals->SplittingOff();
// 	normals->Update();

// 	vtkDataArray* cellNormals = normals->GetOutput()->GetCellData()->GetNormals();

// 	this->Mesh = vtkPolyData::New();
// 	vtkPoints* workingPoints = vtkPoints::New();			// original points
// 	vtkCellArray* workingCells = vtkCellArray::New(); // original cells
// 	workingPoints->DeepCopy(input->GetPoints());

// 	double cellNormal[3], orientedCellNormal[3]; // current cell normal,
// 																							 // oriented cell normal?
// 	double point1[3], point2[3], point3[3]; // points for each node of the cell
// 	for(int i=0; i<input->GetNumberOfCells(); i++){
// 		vtkCell* cell = input->GetCell(i);
// 		cell->GetPoints()->GetPoint(0,point1);
// 		cell->GetPoints()->GetPoint(1,point2);
// 		cell->GetPoints()->GetPoint(2,point3);
// 		vtkTriangle::ComputeNormal(point1,point2,point3,cellNormal); 
// 		cellNormals->GetTuple(i,orientedCellNormal);
// 		workingCells->InsertNextCell(3);

// 		if(vtkMath::Dot(cellNormal,orientedCellNormal) > 0.0){
// 			workingCells->InsertCellPoint(cell->GetPointId(0));
// 			workingCells->InsertCellPoint(cell->GetPointId(1));
// 			workingCells->InsertCellPoint(cell->GetPointId(2));
// 		}
// 		else{
// 			workingCells->InsertCellPoint(cell->GetPointId(1));
// 			workingCells->InsertCellPoint(cell->GetPointId(0));
// 			workingCells->InsertCellPoint(cell->GetPointId(2));
// 		}
// 	}

// 	this->Mesh->SetPoints(workingPoints);
// 	this->Mesh->SetPolys(workingCells);
// 	workingPoints->Delete();
// 	workingCells->Delete(); 
// 	normals->Delete();
// 	this->Mesh->BuildCells();
// 	this->Mesh->BuildLinks();

// 	if(this->Locator){
// 		this->Locator->Delete();
// 		this->Locator = NULL;
// 	}

// 	this->Locator = vtkCellLocator::New();
// 	this->Locator->SetDataSet(input);
// 	this->Locator->SetNumberOfCellsPerBucket(5);
// 	this->Locator->CacheCellBoundsOn();
// 	this->Locator->BuildLocator();

// 	if(this->InputBoundary){
// 		this->InputBoundary->Delete();
// 		this->InputBoundary = NULL;
// 	}

// 	uqMeshPolyDataBoundaryExtractor* boundaryExtractor = uqMeshPolyDataBoundaryExtractor::New();
// 	boundaryExtractor->SetInputData(input);
// 	boundaryExtractor->Update();

// 	this->InputBoundary = vtkPolyData::New();
// 	this->InputBoundary->DeepCopy(boundaryExtractor->GetOutput());

// 	boundaryExtractor->Delete();

// 	if(this->BoundaryLocator){
// 		this->BoundaryLocator->Delete();
// 		this->BoundaryLocator = NULL;
// 	}

// 	if(this->InputBoundary->GetNumberOfCells() > 0){
// 		this->BoundaryLocator = vtkCellLocator::New();
// 		this->BoundaryLocator->SetDataSet(this->InputBoundary);
// 		this->BoundaryLocator->BuildLocator();
// 	}

// 	if(this->InputEntityBoundary){
// 		this->InputEntityBoundary->Delete();
// 		this->InputEntityBoundary = NULL;
// 	}

// 	this->InputEntityBoundary = vtkPolyData::New();
// 	this->BuildEntityBoundary(this->Mesh,this->InputEntityBoundary);

// 	if(this->EntityBoundaryLocator){
// 		this->EntityBoundaryLocator->Delete();
// 		this->EntityBoundaryLocator = NULL;
// 	}

// 	if(this->InputEntityBoundary->GetNumberOfCells() > 0){
// 		this->EntityBoundaryLocator = vtkCellLocator::New();
// 		this->EntityBoundaryLocator->SetDataSet(this->InputEntityBoundary);
// 		this->EntityBoundaryLocator->BuildLocator();
// 	}

// 	int relocationSuccess = RELOCATE_SUCCESS;
// 	for(int n=0; n<this->NumberOfIterations; n++){
// 		cout<<"Iteration "<<n+1<<"/"<<this->NumberOfIterations<<endl;
// 		this->EdgeCollapseIteration();
// 		this->EdgeFlipIteration();
// 		this->EdgeSplitIteration();
// 		this->EdgeFlipIteration();
// 		relocationSuccess = this->PointRelocationIteration();
// 		if(relocationSuccess == RELOCATE_FAILURE){
// 			break;
// 		}
// 		this->EdgeFlipIteration();

// 		for(int i=0; i<this->NumberOfConnectivityOptimizationIterations; i++){
// 			this->EdgeFlipConnectivityOptimizationIteration();
// 		}
// 	}

// 	cout<<"Final mesh improvement"<<endl;
// 	bool projectToSurface = false;
// 	for(int i=0; i<this->NumberOfIterations; i++){
// 		if(i == this->NumberOfIterations/2){
// 			projectToSurface = true;
// 		}
// 		relocationSuccess = this->PointRelocationIteration(projectToSurface);
// 		if(relocationSuccess == RELOCATE_FAILURE){
// 			break;
// 		}
// 	}

// 	if(relocationSuccess == RELOCATE_FAILURE){
// 		return 1;
// 	}
 
// 	this->TriangleSplitIteration();
 
// 	vtkPoints* newPoints = vtkPoints::New();
// 	vtkCellArray* newCells = vtkCellArray::New();
// 	vtkIntArray* newCellEntityIdsArray = vtkIntArray::New();
// 	newCellEntityIdsArray->SetName(this->CellEntityIdsArrayName);

// 	newPoints->DeepCopy(this->Mesh->GetPoints());

// 	int numberOfFinalCells = this->Mesh->GetNumberOfCells();
// 	for(int i=0; i<numberOfFinalCells; i++){
// 		if(this->Mesh->GetCellType(i) == VTK_EMPTY_CELL){
// 			continue;
// 		}
// 		if(this->GetNumberOfBoundaryEdges(i) > 1){
// 			continue;
// 		}
// 		newCells->InsertNextCell(this->Mesh->GetCell(i));
// 		newCellEntityIdsArray->InsertNextValue(this->CellEntityIdsArray->GetValue(i));
// 	}

// 	output->SetPoints(newPoints);
// 	output->SetPolys(newCells);
// 	output->GetCellData()->AddArray(newCellEntityIdsArray);
 
// 	newPoints->Delete();
// 	newCells->Delete();
// 	newCellEntityIdsArray->Delete();

// 	return 1;
// }