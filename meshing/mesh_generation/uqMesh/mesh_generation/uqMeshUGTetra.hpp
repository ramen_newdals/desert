/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshUGTetra.h,v $
Language:  C++
Date:      $Date: 2006/07/17 09:53:14 $
Version:   $Revision: 1.1 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
  // .NAME uqMeshUGTetra - Convert the elements of a mesh to linear tetrahedra.
  // .SECTION Description
  // .

#ifndef __uqMeshUGTetra_h
#define __uqMeshUGTetra_h

#include "vtkUnstructuredGridAlgorithm.h"
#include "vtkCellData.h"
#include "vtkCellLinks.h"
#include "vtkCellType.h"
#include "vtkGenericCell.h"
#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkOrderedTriangulator.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnsignedCharArray.h"

class vtkOrderedTriangulator;
class vtkIdList;

class uqMeshUGTetra : public vtkUnstructuredGridAlgorithm
{
public:
  
  vtkTypeMacro(uqMeshUGTetra,vtkUnstructuredGridAlgorithm);
  static uqMeshUGTetra *New();
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

  vtkSetMacro(TetrahedraOnly, int);
  vtkGetMacro(TetrahedraOnly, int);
  vtkBooleanMacro(TetrahedraOnly, int);

protected:
  uqMeshUGTetra();
  ~uqMeshUGTetra();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

  int TriangulateQuad(vtkUnstructuredGrid* output, vtkIdList *quadPtIds, vtkIdList *ptIds);

  vtkOrderedTriangulator *Triangulator;

  void Execute(vtkUnstructuredGrid *, vtkUnstructuredGrid *);

  int TetrahedraOnly;

private:
  uqMeshUGTetra(const uqMeshUGTetra&);  // Not implemented.
  void operator=(const uqMeshUGTetra&);  // Not implemented.
};

#endif

