/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshTetGenWrapper.cxx,v $
Language:  C++
Date:      $Date: 2006/04/06 16:47:48 $
Version:   $Revision: 1.8 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "uqMeshTetGenWrapper.hpp"

vtkStandardNewMacro(uqMeshTetGenWrapper);

uqMeshTetGenWrapper::uqMeshTetGenWrapper(){
	this->PLC = 0;                // -p switch, 0
	this->Refine = 0;             // -r switch, 0
	this->Coarsen = 0;            // -R switch, 0
	this->Quality = 0;            // -q switch, 0
	this->NoBoundarySplit = 0;    // -Y switch, 0
	this->VarVolume = 0;          // -a switch without number, 0
	this->FixedVolume = 0;        // -a switch with number, 0
	this->MaxVolume = -1.0;       //    number after -a, -1.0
	this->RemoveSliver = 0;       // -s switch, 0
	this->MinRatio = 2.0;         //    number after -q, 2.0
	this->MinDihedral = 5.0;      //    number after -qq, 5.0
	this->MaxDihedral = 165.0;    //    number after -qqq, 165.0
	this->RegionAttrib = 0;       // -A switch, 0
	this->Epsilon = 1.0e-8;       // number after -T switch, 1.0e-8
	this->NoMerge = 0;            // -M switch, 0
	this->DetectInter = 0;        // -d switch, 0
	this->CheckClosure = 0;       // -c switch, 0
	this->Order = 1;              // number after -o switch, 1 (e.g. -o2 for quadratic elements)
	this->DoCheck = 0;            // -C switch, 0
	this->UseSizingFunction = 0;  // -m switch, 0

	this->Verbose = 0;

	this->CellEntityIdsArrayName = NULL;
	this->TetrahedronVolumeArrayName = NULL;
	this->SizingFunctionArrayName = NULL;

	this->OutputSurfaceElements = 1;
	this->OutputVolumeElements = 1;

	this->LastRunExitStatus = -1;
}

uqMeshTetGenWrapper::~uqMeshTetGenWrapper(){
	if(this->CellEntityIdsArrayName){
		delete[] this->CellEntityIdsArrayName;
		this->CellEntityIdsArrayName = NULL;
	}

	if(this->TetrahedronVolumeArrayName){
		delete[] this->TetrahedronVolumeArrayName;
		this->TetrahedronVolumeArrayName = NULL;
	}

	if(this->SizingFunctionArrayName){
		delete[] this->SizingFunctionArrayName;
		this->SizingFunctionArrayName = NULL;
	}
}

std::string TetgenSwitchesPrep(){
	// Performes all of the nessecary setup to call tetreahedralize()
	// Make the switches string to control tetgen behaviour
	std::string tetgen_switches;
}

int uqMeshTetGenWrapper::RequestData(	vtkInformation *vtkNotUsed(request),
																				vtkInformationVector **inputVector,
																				vtkInformationVector *outputVector){
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkUnstructuredGrid *output = vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	if(this->VarVolume && !this->TetrahedronVolumeArrayName){
		vtkErrorMacro(<<"VarVolumeOn but TetrahedronVolumeArrayName not specified");
		return 1;
	}

	vtkStdString tetgenOptionString;
	tetgenbehavior b;
	tetgenio in;
	tetgenio out;
	tetgenio addin;
	tetgenio bgmin;

	char buffer[64];

	if(this->PLC){
		tetgenOptionString += "p";
		b.plc = 1;
	}

	if(this->Refine){
		tetgenOptionString += "r";  
		b.refine = 1;
	}

	if(this->Coarsen){
		tetgenOptionString += "R";
		b.coarsen = 1;
	}

	if(this->Quality){
		tetgenOptionString += "q"; 
		b.quality = 1;
		sprintf(buffer,"%f",this->MinRatio);
		tetgenOptionString += buffer;
		b.minratio = this->MinRatio;
		tetgenOptionString += "q";  
		sprintf(buffer,"%f",this->MinDihedral);
		b.mindihedral = this->MinDihedral;
		tetgenOptionString += buffer;
		tetgenOptionString += "q";  
		sprintf(buffer,"%f",this->MaxDihedral);
		tetgenOptionString += buffer;
		b.optmaxdihedral = this->MaxDihedral;
  }

	if(this->NoBoundarySplit){
		tetgenOptionString += "Y";
		b.nobisect = 1;
	}

	if(this->RemoveSliver){
		tetgenOptionString += "s";
		b.psc = 1;
	}

	if(this->Order == 2){
		tetgenOptionString += "o2";
		b.order = 2;
	}
	else{
		tetgenOptionString += "o1";
		b.order = 1;
	}

	tetgenOptionString += "T";
	// b.epsilon = 1E-8;
	sprintf(buffer,"%e",this->Epsilon);
	tetgenOptionString += buffer;

	if(this->VarVolume){
		tetgenOptionString += "a";
		b.varvolume = 1;
	}

	if(this->FixedVolume){
		tetgenOptionString += "a";
		sprintf(buffer,"%f",this->MaxVolume);
		tetgenOptionString += buffer;
		b.fixedvolume = 1;
		b.maxvolume = this->MaxVolume;
	}

	if(this->RegionAttrib){
		tetgenOptionString += "A";
		b.regionattrib = 1;
	}

  if(this->NoMerge){
		tetgenOptionString += "M";
		b.nomergefacet = 1;
		b.nomergevertex = 1;
	}

	if(this->DetectInter){
		tetgenOptionString += "d";
	}

	if(this->DoCheck){
		tetgenOptionString += "C";
		b.docheck = 1;
	}

	if(this->CheckClosure){
		tetgenOptionString += "c";
		b.convex = 1;
	}

	tetgenOptionString += "z";  
	// b.zeroindex = 1;
// tetgenOptionString += "Y";  

	if(this->Verbose){
		tetgenOptionString += "V";
		b.verbose = 1;
	}
	else{
		tetgenOptionString += "Q";
		// b.quiet = 1;
	}

	vtkDataArray* sizingFunctionArray = NULL;
	if(this->SizingFunctionArrayName){
		sizingFunctionArray = input->GetPointData()->GetArray(this->SizingFunctionArrayName);
	}

	if(this->UseSizingFunction && sizingFunctionArray){
		tetgenOptionString += "m";
		b.metric = 1;
	}

	// tetgenio in_tetgenio;
	// tetgenio out_tetgenio;

	in.firstnumber = 0;

	const int meshDimensionality = 3; //TODO: set this
	in.mesh_dim = meshDimensionality;

	//TODO - all point arrays except marker array
	in.pointattributelist = NULL; //REAL*
	in.numberofpointattributes = 0;
	//TODO

	in.pointmtrlist = NULL;
	in.numberofpointmtrs = 0;

	int numberOfPoints = input->GetNumberOfPoints();

	in.numberofpoints = numberOfPoints;
	in.pointlist = new REAL[meshDimensionality * numberOfPoints];
	if(sizingFunctionArray){
		in.numberofpointmtrs = 1;
		in.pointmtrlist = new REAL[numberOfPoints];
	}

	double point[3];
	int i;
	for(i=0; i<numberOfPoints; i++){
		input->GetPoint(i,point);
		in.pointlist[meshDimensionality * i + 0] = point[0];
		in.pointlist[meshDimensionality * i + 1] = point[1];
		in.pointlist[meshDimensionality * i + 2] = point[2];
		if(sizingFunctionArray){
			in.pointmtrlist[i] = sizingFunctionArray->GetComponent(i,0);
			if(in.pointmtrlist[i] == 0.0){
				in.pointmtrlist[i] = VTK_VMTK_FLOAT_TOL;
			}
		}
	}

	vtkIdList* facetCellIds = vtkIdList::New();
	vtkIdList* tetraCellIds = vtkIdList::New();

	int numberOfCells = input->GetNumberOfCells();
	for(i=0; i<numberOfCells; i++){
		vtkCell* cell = input->GetCell(i);
		int cellType = cell->GetCellType();
		switch (cellType){
			case VTK_TRIANGLE: 
			case VTK_QUADRATIC_TRIANGLE:
			case VTK_POLYGON:
				facetCellIds->InsertNextId(i);
			break;
			case VTK_TETRA:
				if (this->Order != 1){
					vtkErrorMacro(<<"Element of incorrect order found.");
					break;
				}
				tetraCellIds->InsertNextId(i);
				break;
			case VTK_QUADRATIC_TETRA:
				if(this->Order != 2){
					vtkErrorMacro(<<"Element of incorrect order found.");
					break;
				}
				tetraCellIds->InsertNextId(i);
				break;
			default:
				vtkErrorMacro(<<"Invalid element found, cellId "<<i<<", cellType "<<cellType);
				break;
			}
	}

	int numberOfFacets = facetCellIds->GetNumberOfIds();
	int numberOfTetras = tetraCellIds->GetNumberOfIds();

	//TODO - input as vtkPointSet (point inside hole ([0],[1],[2]))
	in.holelist = NULL; //REAL* 
	in.numberofholes = 0; 
	//TODO
  
	if(numberOfFacets > 0){
		in.numberoffacets = numberOfFacets;
		in.facetlist = new tetgenio::facet[numberOfFacets];
		in.facetmarkerlist = new int[numberOfFacets];

		vtkDataArray* facetMarkerArray = input->GetCellData()->GetArray(this->CellEntityIdsArrayName);

		for(i=0; i<numberOfFacets; i++){
			vtkIdType npts;
			const vtkIdType *pts;
			input->GetCellPoints(facetCellIds->GetId(i),npts,pts);
			in.facetlist[i].numberofpolygons = 1;
			in.facetlist[i].polygonlist = new tetgenio::polygon[in.facetlist[i].numberofpolygons];
			in.facetlist[i].numberofholes = 0;
			in.facetlist[i].holelist = NULL;
			in.facetlist[i].polygonlist[0].numberofvertices = npts;
			in.facetlist[i].polygonlist[0].vertexlist = new int[npts];
			for(int j=0; j<npts; j++){
				in.facetlist[i].polygonlist[0].vertexlist[j] = pts[j];
			}

			if(facetMarkerArray){
				in.facetmarkerlist[i] = static_cast<int>(facetMarkerArray->GetComponent(facetCellIds->GetId(i),0));
			}
			else{
				in.facetmarkerlist[i] = 0;
			}
		}
	}

	//TODO - all cell arrays except volume array
	in.tetrahedronattributelist = NULL; //REAL* 
	in.numberoftetrahedronattributes = 0; 
	//TODO

	if(numberOfTetras > 0){
		vtkDataArray* tetrahedronVolumeArray = NULL;
		if(this->VarVolume){
			tetrahedronVolumeArray = input->GetCellData()->GetArray(this->TetrahedronVolumeArrayName);
		}

		in.numberoftetrahedra = numberOfTetras;
		switch(this->Order){
			case 1:
				in.numberofcorners = 4;
				break;
			case 2:
				in.numberofcorners = 10;
				break;
			default:
				in.numberofcorners = 0;
				break;
		}

		in.tetrahedronlist = new int[in.numberofcorners * numberOfTetras];

		if(tetrahedronVolumeArray){
			in.tetrahedronvolumelist = new REAL[numberOfTetras];
		}

		for(i=0; i<numberOfTetras; i++){
			vtkIdType npts;
			const vtkIdType *pts;
			input->GetCellPoints(tetraCellIds->GetId(i),npts,pts);
			for(int j=0; j<npts; j++){
				in.tetrahedronlist[i*in.numberofcorners + j] = pts[j];
			}

			if(tetrahedronVolumeArray){
				in.tetrahedronvolumelist[i] = tetrahedronVolumeArray->GetComponent(i,0);
			}
		}
	}

	facetCellIds->Delete();
	tetraCellIds->Delete();

	//TODO - input as vtkPointSet + arrays for attributes (point inside region ([0],[1],[2]), regional attribute [3], volume constraint [4])
	in.regionlist = NULL; //REAL* 
	in.numberofregions = 0; 
	//TODO

	char tetgenOptions[512];
	strcpy(tetgenOptions,tetgenOptionString.c_str());
	cout<<"TetGen command line options: "<<tetgenOptions<<endl;
	try{
		// tetrahedralize(tetgenbehavior *b,
		//								tetgenio *in,
		//								tetgenio *out,
		//								tetgenio *addin = NULL,
		//								tetgenio *bgmin = NULL)
		tetrahedralize(&b, &in, &out);
	}
	catch ( ... ){
		vtkErrorMacro(<<"TetGen quit with an exception.");
		this->LastRunExitStatus = 1;
		return 1;
	}
    
	this->LastRunExitStatus = 0;

	//TODO
	//  out.edgelist; //int* 
	//  out.edgemarkerlist; //int* 
	//  out.numberofedges; 
	//  out.neighborlist; //int* 
	//TODO

	vtkPoints* outputPoints = vtkPoints::New();

	outputPoints->SetNumberOfPoints(out.numberofpoints);
	for(i=0; i<out.numberofpoints; i++){
		point[0] = out.pointlist[meshDimensionality * i + 0];
		point[1] = out.pointlist[meshDimensionality * i + 1];
		point[2] = out.pointlist[meshDimensionality * i + 2];
		outputPoints->SetPoint(i,point);
	}

	output->SetPoints(outputPoints);

	vtkCellArray* outputCellArray = vtkCellArray::New();

	int numberOfOutputCells = 0;

  if(this->OutputVolumeElements){
		numberOfOutputCells += out.numberoftetrahedra;
	}

	if(this->OutputSurfaceElements){
		numberOfOutputCells += out.numberoftrifaces;
	}

	int* outputCellTypes = new int[numberOfOutputCells];

	vtkIntArray* outputCellMarkerArray = vtkIntArray::New();
	outputCellMarkerArray->SetNumberOfTuples(numberOfOutputCells);
	outputCellMarkerArray->SetName(this->CellEntityIdsArrayName);
	outputCellMarkerArray->FillComponent(0,-1.0);

	int cellIdOffset = 0;

	if(this->OutputVolumeElements){
		int outputCellType;
		switch(this->Order){
			case 1:
				outputCellType = VTK_TETRA;
				break;
			case 2:
				outputCellType = VTK_QUADRATIC_TETRA;
				break;
			default:
				outputCellType = VTK_TETRA;
		}

		for(i=0; i<out.numberoftetrahedra; i++){
			outputCellArray->InsertNextCell(out.numberofcorners);
			for(int j=0; j<out.numberofcorners; j++){
				outputCellArray->InsertCellPoint(out.tetrahedronlist[i*out.numberofcorners + j]);
			}
			outputCellTypes[cellIdOffset+i] = outputCellType;
		}

		cellIdOffset = out.numberoftetrahedra;
	}

	if(this->OutputSurfaceElements){
		int outputCellType = VTK_TRIANGLE;
		int numberOfTrifaceCorners = 3;

		for(i=0; i<out.numberoftrifaces; i++){
			outputCellArray->InsertNextCell(numberOfTrifaceCorners);
			for(int j=0; j<numberOfTrifaceCorners; j++){
				outputCellArray->InsertCellPoint(out.trifacelist[i*numberOfTrifaceCorners + j]);
			}
			outputCellTypes[cellIdOffset+i] = outputCellType;
			outputCellMarkerArray->SetValue(cellIdOffset+i,out.trifacemarkerlist[i]);
		}

		cellIdOffset = out.numberoftrifaces;
	}

	output->SetCells(outputCellTypes,outputCellArray);

	output->GetCellData()->AddArray(outputCellMarkerArray);

	outputPoints->Delete();
	outputCellArray->Delete();
	delete[] outputCellTypes;

	outputCellMarkerArray->Delete();

	return 1;
}

void uqMeshTetGenWrapper::PrintSelf(std::ostream& os, vtkIndent indent){
	this->Superclass::PrintSelf(os,indent);
}
