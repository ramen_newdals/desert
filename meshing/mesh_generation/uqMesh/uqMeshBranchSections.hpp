/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshBranchSections.h,v $
Language:  C++
Date:      $Date: 2006/10/17 15:16:16 $
Version:   $Revision: 1.1 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshBranchSections - Extract a vessel (cross) section n-spheres distance from from the start point of a branch. 
// .SECTION Description
//  The set of vessel sections contain the profile as well as the the following information about the section:
//  - Branch Section Group Ids
//  - Branch Section Bifurcation Group Ids
//  - Branch Section Orientation
//  - Branch Section Distance Spheres
//  - Branch Section Point
//  - Branch Section Normal
//  - Branch Section Area
//  - Branch Section Min Size
//  - Branch Section Max Size
//  - Branch Section Shape
//  - Branch Section Closed

#ifndef __uqMeshBranchSections_h
#define __uqMeshBranchSections_h

// VTK headers
#include <vtkPolyDataAlgorithm.h>
#include <vtkPolyData.h>
#include <vtkPolyLine.h>
#include <vtkTriangle.h>
#include <vtkPolygon.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkPlane.h>
#include <vtkLine.h>
#include <vtkCutter.h>
#include <vtkStripper.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkMath.h>
#include <vtkCleanPolyData.h>
#include <vtkAppendPolyData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

// project specific headers
#include "uqMeshCenterlineUtilities.hpp"
#include "uqMeshCenerlineSphereDistance.hpp"
#include "uqMeshBranchUtilities.hpp"


class uqMeshBranchSections : public vtkPolyDataAlgorithm
{
  public: 
  vtkTypeMacro(uqMeshBranchSections,vtkPolyDataAlgorithm)
  void PrintSelf(std::ostream& os, vtkIndent indent) override;

  static uqMeshBranchSections* New();

  vtkSetStringMacro(GroupIdsArrayName);
  vtkGetStringMacro(GroupIdsArrayName);

  vtkSetObjectMacro(Centerlines,vtkPolyData);
  vtkGetObjectMacro(Centerlines,vtkPolyData);

  vtkSetStringMacro(CenterlineRadiusArrayName);
  vtkGetStringMacro(CenterlineRadiusArrayName);

  vtkSetStringMacro(CenterlineGroupIdsArrayName);
  vtkGetStringMacro(CenterlineGroupIdsArrayName);

  vtkSetStringMacro(CenterlineIdsArrayName);
  vtkGetStringMacro(CenterlineIdsArrayName);

  vtkSetStringMacro(CenterlineTractIdsArrayName);
  vtkGetStringMacro(CenterlineTractIdsArrayName);

  vtkSetStringMacro(BlankingArrayName);
  vtkGetStringMacro(BlankingArrayName);

  vtkSetStringMacro(BranchSectionAreaArrayName);
  vtkGetStringMacro(BranchSectionAreaArrayName);

  vtkSetStringMacro(BranchSectionMinSizeArrayName);
  vtkGetStringMacro(BranchSectionMinSizeArrayName);

  vtkSetStringMacro(BranchSectionMaxSizeArrayName);
  vtkGetStringMacro(BranchSectionMaxSizeArrayName);

  vtkSetStringMacro(BranchSectionShapeArrayName);
  vtkGetStringMacro(BranchSectionShapeArrayName);

  vtkSetStringMacro(BranchSectionGroupIdsArrayName);
  vtkGetStringMacro(BranchSectionGroupIdsArrayName);

  vtkSetStringMacro(BranchSectionClosedArrayName);
  vtkGetStringMacro(BranchSectionClosedArrayName);

  vtkSetStringMacro(BranchSectionDistanceSpheresArrayName);
  vtkGetStringMacro(BranchSectionDistanceSpheresArrayName);

  vtkSetMacro(NumberOfDistanceSpheres,int);
  vtkGetMacro(NumberOfDistanceSpheres,int);

  vtkSetMacro(ReverseDirection,int);
  vtkGetMacro(ReverseDirection,int);
  vtkBooleanMacro(ReverseDirection,int);

  static double ComputeBranchSectionArea(vtkPolyData* branchSection);
  static double ComputeBranchSectionShape(vtkPolyData* branchSection, double center[3], double sizeRange[2]);

  static void ExtractCylinderSection(vtkPolyData* cylinder, double origin[3], double normal[3], vtkPolyData* section, bool & closed);

  protected:
  uqMeshBranchSections();
  ~uqMeshBranchSections();  

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

  void ComputeBranchSections(vtkPolyData* input, int groupId, vtkPolyData* output);

  vtkPolyData* Centerlines;

  char* GroupIdsArrayName;
  char* CenterlineRadiusArrayName;
  char* CenterlineGroupIdsArrayName;
  char* CenterlineIdsArrayName;
  char* CenterlineTractIdsArrayName;
  char* BlankingArrayName;

  char* BranchSectionGroupIdsArrayName;
  char* BranchSectionAreaArrayName;
  char* BranchSectionMinSizeArrayName;
  char* BranchSectionMaxSizeArrayName;
  char* BranchSectionShapeArrayName;
  char* BranchSectionClosedArrayName;
  char* BranchSectionDistanceSpheresArrayName;

  int NumberOfDistanceSpheres;
  int ReverseDirection;

  private:
  uqMeshBranchSections(const uqMeshBranchSections&);  // Not implemented.
  void operator=(const uqMeshBranchSections&);  // Not implemented.
};

#endif