/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshBranchUtilities.h,v $
Language:  C++
Date:      $Date: 2006/04/06 16:46:43 $
Version:   $Revision: 1.3 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshBranchUtilities - Utility functions to ease working with branches and surfaces. 
// .SECTION Description
// - ExtractGroup: Extract a single surface branch group from a surface which has already been grouped. 
// - GetGroupIdsList: get the group ids which are contained within a grouped surface as a vtkIdList.

#ifndef __uqMeshBranchUtilities_h
#define __uqMeshBranchUtilities_h

// VTK headers
#include <vtkObject.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkObjectFactory.h>
#include <vtkVersion.h>

class vtkPolyData;
class vtkIdList;

class uqMeshBranchUtilities : public vtkObject
{
public: 
  vtkTypeMacro(uqMeshBranchUtilities,vtkObject)
  static uqMeshBranchUtilities* New();

  static void GetGroupsIdList(vtkPolyData* surface, const char* groupIdsArrayName, vtkIdList* groupIds);
  static void ExtractGroup(vtkPolyData* surface, const char* groupIdsArrayName, vtkIdType groupId, bool cleanGroupSurface, vtkPolyData* groupSurface);
  
protected:
  uqMeshBranchUtilities() {};
  ~uqMeshBranchUtilities() {};

private:
  uqMeshBranchUtilities(const uqMeshBranchUtilities&);  // Not implemented.
  void operator=(const uqMeshBranchUtilities&);  // Not implemented.
};

#endif
