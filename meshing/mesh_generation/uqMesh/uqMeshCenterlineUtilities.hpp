/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshCenterlineUtilities.h,v $
Language:  C++
Date:      $Date: 2006/10/17 15:16:16 $
Version:   $Revision: 1.5 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshCenterlineUtilities - Collection of functions which are used when working with split and grouped centerlines.
// .SECTION Description
// ...

#ifndef __uqMeshCenterlineUtilities_h
#define __uqMeshCenterlineUtilities_h

// VTK headers
#include <vtkObject.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkIdList.h>
#include <vtkPolyLine.h>
#include <vtkMath.h>
#include <vtkObjectFactory.h>

// project headers
#include "uqMeshConstants.hpp"

class vtkPolyData;
class vtkPoints;
class vtkIdList;

class uqMeshCenterlineUtilities : public vtkObject
{
public: 
  vtkTypeMacro(uqMeshCenterlineUtilities,vtkObject)
  static uqMeshCenterlineUtilities* New(); 

  static vtkIdType GetMaxGroupId(vtkPolyData* centerlines, const char* groupIdsArrayName);
  
  static void GetGroupsIdList(vtkPolyData* centerlines, const char* groupIdsArrayName, vtkIdList* groupIds);
  
  static void GetGroupsIdList(vtkPolyData* centerlines, const char* groupIdsArrayName, const char* blankingArrayName, int blanked, vtkIdList* groupIds);
  
  static void GetNonBlankedGroupsIdList(vtkPolyData* centerlines, const char* groupIdsArrayName, const char* blankingArrayName, vtkIdList* groupIds);
  
  static void GetBlankedGroupsIdList(vtkPolyData* centerlines, const char* groupIdsArrayName, const char* blankingArrayName, vtkIdList* groupIds);

  static void GetGroupCellIds(vtkPolyData* centerlines, const char* groupIdsArrayName, vtkIdType groupId, vtkIdList* groupCellIds);
  
  static void GetGroupUniqueCellIds(vtkPolyData* centerlines, const char* groupIdsArrayName, vtkIdType groupId, vtkIdList* groupCellIds);

  static void GetCenterlineCellIds(vtkPolyData* centerlines, const char* centerlineIdsArrayName, vtkIdType centerlineId, vtkIdList* centerlineCellIds);
  
  static void GetCenterlineCellIds(vtkPolyData* centerlines, const char* centerlineIdsArrayName, const char* tractIdsArrayName, vtkIdType centerlineId, vtkIdList* centerlineCellIds);

  static int IsGroupBlanked(vtkPolyData* centerlines, const char* groupIdsArrayName, const char* blankingArrayName, vtkIdType groupId);
  
  static int IsCellBlanked(vtkPolyData* centerlines, const char* blankingArrayName, vtkIdType cellId);
  
  static void FindAdjacentCenterlineGroupIds(vtkPolyData* centerlines, const char* groupIdsArrayName, const char* centerlineIdsArrayName, const char* tractIdsArrayName, vtkIdType groupId, vtkIdList* upStreamGroupIds, vtkIdList* downStreamGroupIds);

  static void InterpolatePoint(vtkPolyData* centerlines, int cellId, int subId, double pcoord, double interpolatedPoint[3]);

  static void InterpolateTuple(vtkPolyData* centerlines, const char* arrayName, int cellId, int subId, double pcoord, double* interpolatedTuple);

  static void InterpolateTuple1(vtkPolyData* centerlines, const char* arrayName, int cellId, int subId, double pcoord, double& interpolatedTuple1)
  {
    InterpolateTuple(centerlines,arrayName,cellId,subId,pcoord,&interpolatedTuple1);
  }
 
  static void InterpolateTuple3(vtkPolyData* centerlines, const char* arrayName, int cellId, int subId, double pcoord, double interpolatedTuple3[3])
  {
    InterpolateTuple(centerlines,arrayName,cellId,subId,pcoord,interpolatedTuple3);
  }
 
  static void FindMergingPoints(vtkPolyData* centerlines, vtkPoints* mergingPoints, double tolerance);

protected:
  uqMeshCenterlineUtilities() {};
  ~uqMeshCenterlineUtilities() {};

private:
  uqMeshCenterlineUtilities(const uqMeshCenterlineUtilities&);  // Not implemented.
  void operator=(const uqMeshCenterlineUtilities&);  // Not implemented.
};

#endif
