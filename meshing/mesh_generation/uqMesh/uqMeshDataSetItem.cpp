/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshDataSetItem.cxx,v $
  Language:  C++
  Date:      $Date: 2005/11/15 17:39:25 $
  Version:   $Revision: 1.3 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "uqMeshDataSetItem.hpp"

uqMeshDataSetItem::uqMeshDataSetItem()
  {
  this->DataSetPointId = -1;
  this->DataSet = NULL;
  this->ReallocateOnBuild = 0;
  }

void uqMeshDataSetItem::DeepCopy(uqMeshItem* src)
  {
  this->Superclass::DeepCopy(src);

  uqMeshDataSetItem* dataSetItemSrc = uqMeshDataSetItem::SafeDownCast(src);

  if (dataSetItemSrc==NULL)
    {
    vtkErrorMacro(<<"Trying to deep copy a non-stencil item");
    }

  this->SetDataSet(dataSetItemSrc->GetDataSet());
  this->DataSetPointId = dataSetItemSrc->DataSetPointId;

  }
