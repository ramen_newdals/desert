/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshItem.h,v $
  Language:  C++
  Date:      $Date: 2006/04/06 16:46:43 $
  Version:   $Revision: 1.3 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshItem - Base class for the construction of neighborhoods and stencil from a set of points. 
// .SECTION Description
// ..

#ifndef __uqMeshItem_h
#define __uqMeshItem_h

#include <vtkObject.h>
#include <vtkDataSet.h>
#include <vtkObjectFactory.h>

class uqMeshItem : public vtkObject 
{
public:

  vtkTypeMacro(uqMeshItem,vtkObject)

  virtual vtkIdType GetItemType() = 0;

  // Description:
  // Standard DeepCopy method.  Since this object contains no reference
  // to other objects, there is no ShallowCopy.
  virtual void DeepCopy(uqMeshItem *src);

protected:
  uqMeshItem() {};
  ~uqMeshItem() {};

private:
  uqMeshItem(const uqMeshItem&);  // Not implemented.
  void operator=(const uqMeshItem&);  // Not implemented.
};

#endif

