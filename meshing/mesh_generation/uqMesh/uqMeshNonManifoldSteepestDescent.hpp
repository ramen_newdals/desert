/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshNonManifoldSteepestDescent.h,v $
Language:  C++
Date:      $Date: 2006/04/06 16:46:43 $
Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshNonManifoldSteepestDescent - Abstract class for steepest descent on a polygonal non-manifold.
// .SECTION Description
// This class is an abstract filter used as base class for performing steepest descent on a non-manifold surface made of convex polygons (such as the Voronoi diagram) on the basis of a given scalar field. Steepest descent is performed on the edges of input polygons with a first order approximation.
//
// .SECTION See Also
// vtkSteepestDescentLineTracer vtkSurfaceToCenterlines vtkVoronoiDiagram3D

#ifndef __uqMeshNonManifoldSteepestDescent_h
#define __uqMeshNonManifoldSteepestDescent_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkDataArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkIdList.h>
#include <vtkMath.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

// Custom headers
#include "uqMeshConstants.hpp"

#define VTK_VMTK_DOWNWARD 0
#define VTK_VMTK_UPWARD 1

/// @brief This is a comment to see if doxygen is working
class uqMeshNonManifoldSteepestDescent : public vtkPolyDataAlgorithm
{
  public:
  vtkTypeMacro(uqMeshNonManifoldSteepestDescent,vtkPolyDataAlgorithm)
  void PrintSelf(std::ostream& os, vtkIndent indent) override;

  static uqMeshNonManifoldSteepestDescent *New();

  // Description:
  // Set/Get the name of the point data array used as the descent scalar field.
  vtkSetStringMacro(DescentArrayName);
  vtkGetStringMacro(DescentArrayName);

  vtkSetMacro(Direction,int);
  vtkGetMacro(Direction,int);
  void SetDirectionToDownward() 
  {this->SetDirection(VTK_VMTK_DOWNWARD); }
  void SetDirectionToUpward() 
  {this->SetDirection(VTK_VMTK_UPWARD); }

  protected:
  uqMeshNonManifoldSteepestDescent();
  ~uqMeshNonManifoldSteepestDescent();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

  // Description:
  // Compute the steepest descent point in terms of edge (point id pair) and parametric coordinate on edge. It takes in input a starting point expressed in terms of edge (point id pair) and parametric coordinate on edge. It returns the descent value.
  double GetSteepestDescent(vtkPolyData* input, vtkIdType* edge, double s, vtkIdType* steepestDescentEdge, double &steepestDescentS);
  double GetSteepestDescentInCell(vtkPolyData* input, vtkIdType cellId, vtkIdType* edge, double s, vtkIdType* steepestDescentEdge, double &steepestDescentS, double &steepestDescentLength);

  vtkDataArray* DescentArray;
  char* DescentArrayName;

  int NumberOfEdgeSubdivisions;
  int Direction;

  private:
  uqMeshNonManifoldSteepestDescent(const uqMeshNonManifoldSteepestDescent&);  // Not implemented.
  void operator=(const uqMeshNonManifoldSteepestDescent&);  // Not implemented.
};

#endif

