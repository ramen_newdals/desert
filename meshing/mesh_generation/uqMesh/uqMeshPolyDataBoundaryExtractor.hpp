/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshPolyDataBoundaryExtractor.h,v $
Language:  C++
Date:      $Date: 2006/07/07 10:46:19 $
Version:   $Revision: 1.5 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataBoundaryExtractor - Extract boundary edges as poly lines.
// .SECTION Description
// This class identifies boundary edges and organizes them into poly lines based on connectivity. It also provides the output with a point data vtkIntArray (set as active scalars) in which the ids of boundary points in the input dataset are stored.

#ifndef __uqMeshPolyDataBoundaryExtractor_h
#define __uqMeshPolyDataBoundaryExtractor_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkCellArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkIdList.h>
#include <vtkGenericCell.h>
#include <vtkIdTypeArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

class uqMeshPolyDataBoundaryExtractor : public vtkPolyDataAlgorithm
{
  public: 
  vtkTypeMacro(uqMeshPolyDataBoundaryExtractor,vtkPolyDataAlgorithm)
  void PrintSelf(std::ostream& os, vtkIndent indent) override;

  static uqMeshPolyDataBoundaryExtractor *New();
  
  protected:
  uqMeshPolyDataBoundaryExtractor();
  ~uqMeshPolyDataBoundaryExtractor() {};

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

  private:
  uqMeshPolyDataBoundaryExtractor(const uqMeshPolyDataBoundaryExtractor&);  // Not implemented.
  void operator=(const uqMeshPolyDataBoundaryExtractor&);  // Not implemented.
};

#endif
