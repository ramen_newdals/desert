#ifndef __uqMeshPolyDataCenterlines_h
#define __uqMeshPolyDataCenterlines_h


#include <vtkPolyDataNormals.h>
#include <vtkDelaunay3D.h>
#include <vtkArrayCalculator.h>
#include <vtkMath.h>
#include <vtkPolyData.h>
#include <vtkPolyDataAlgorithm.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>
#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkVersion.h>

// Custom Headers
#include "uqMeshNonManifoldFastMarching.hpp"
#include "uqMeshConstants.hpp"
#include "uqMeshSteepestDescentLineTracer.hpp"
#include "uqMeshInternalTetrahedraExtractor.hpp"

#include "uqMeshVoronoiDiagram3D.hpp"
#include "uqMeshSimplifyVoronoiDiagram.hpp"

/// @brief Reads in a capped surface mesh and from this mesh genreates the centerlines. Requires that the source and taget seeds are set where souces are the inlets, and targets are the outlets of the mesh. 
class uqMeshPolyDataCenterlines : public vtkPolyDataAlgorithm{
	public: 
	vtkTypeMacro(uqMeshPolyDataCenterlines,vtkPolyDataAlgorithm)
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	static uqMeshPolyDataCenterlines *New();

	virtual void SetSourceSeedIds(vtkIdList*);
	vtkGetObjectMacro(SourceSeedIds,vtkIdList);

	virtual void SetTargetSeedIds(vtkIdList*);
	vtkGetObjectMacro(TargetSeedIds,vtkIdList);

	virtual void SetCapCenterIds(vtkIdList*);
	vtkGetObjectMacro(CapCenterIds,vtkIdList);

	vtkSetObjectMacro(DelaunayTessellation,vtkUnstructuredGrid);
	vtkGetObjectMacro(DelaunayTessellation,vtkUnstructuredGrid);

	vtkSetObjectMacro(VoronoiDiagram,vtkPolyData);
	vtkGetObjectMacro(VoronoiDiagram,vtkPolyData);

	vtkSetObjectMacro(PoleIds,vtkIdList);
	vtkGetObjectMacro(PoleIds,vtkIdList);

	vtkSetStringMacro(RadiusArrayName);
	vtkGetStringMacro(RadiusArrayName);

	vtkSetStringMacro(CostFunction);
	vtkGetStringMacro(CostFunction);

	vtkSetStringMacro(EikonalSolutionArrayName);
	vtkGetStringMacro(EikonalSolutionArrayName);

	vtkSetStringMacro(EdgeArrayName);
	vtkGetStringMacro(EdgeArrayName);

	vtkSetStringMacro(EdgePCoordArrayName);
	vtkGetStringMacro(EdgePCoordArrayName);

	vtkSetStringMacro(CostFunctionArrayName);
	vtkGetStringMacro(CostFunctionArrayName);

	vtkSetMacro(FlipNormals,int);
	vtkGetMacro(FlipNormals,int);
	vtkBooleanMacro(FlipNormals,int);

	vtkSetMacro(SimplifyVoronoi,int);
	vtkGetMacro(SimplifyVoronoi,int);
	vtkBooleanMacro(SimplifyVoronoi,int);

	vtkSetMacro(CenterlineResampling,int);
	vtkGetMacro(CenterlineResampling,int);
	vtkBooleanMacro(CenterlineResampling,int);

	vtkSetMacro(ResamplingStepLength,double);
	vtkGetMacro(ResamplingStepLength,double);

	vtkSetMacro(AppendEndPointsToCenterlines,int);
	vtkGetMacro(AppendEndPointsToCenterlines,int);
	vtkBooleanMacro(AppendEndPointsToCenterlines,int);

	vtkSetMacro(GenerateDelaunayTessellation,int);
	vtkGetMacro(GenerateDelaunayTessellation,int);
	vtkBooleanMacro(GenerateDelaunayTessellation,int);

	vtkSetMacro(GenerateVoronoiDiagram,int);
	vtkGetMacro(GenerateVoronoiDiagram,int);
	vtkBooleanMacro(GenerateVoronoiDiagram,int);

	vtkSetMacro(StopFastMarchingOnReachingTarget,int);
	vtkGetMacro(StopFastMarchingOnReachingTarget,int);
	vtkBooleanMacro(StopFastMarchingOnReachingTarget,int);

	vtkSetMacro(DelaunayTolerance,double);
	vtkGetMacro(DelaunayTolerance,double);


	protected:
	uqMeshPolyDataCenterlines();
	~uqMeshPolyDataCenterlines();  

	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

	void FindVoronoiSeeds(vtkUnstructuredGrid *delaunay, vtkIdList *boundaryBaricenterIds, vtkDataArray *normals, vtkIdList *seedIds);
	void AppendEndPoints(vtkPoints* endPointPairs);
	void ResampleCenterlines();
	void ReverseCenterlines();

	vtkIdList* SourceSeedIds;
	vtkIdList* TargetSeedIds;

	vtkIdList* CapCenterIds;

	vtkUnstructuredGrid* DelaunayTessellation;

	vtkPolyData* VoronoiDiagram;

	vtkIdList* PoleIds;

	char* RadiusArrayName;
	char* CostFunction;
	char* EikonalSolutionArrayName;
	char* EdgeArrayName;
	char* EdgePCoordArrayName;
	char* CostFunctionArrayName;

	int FlipNormals;
	int SimplifyVoronoi;
	int AppendEndPointsToCenterlines;
	int CenterlineResampling;

	double ResamplingStepLength;

	int GenerateVoronoiDiagram;
	int StopFastMarchingOnReachingTarget;
	int GenerateDelaunayTessellation;
	double DelaunayTolerance;

	private:
	uqMeshPolyDataCenterlines(const uqMeshPolyDataCenterlines&);  // Not implemented.
	void operator=(const uqMeshPolyDataCenterlines&);  // Not implemented.
};

#endif