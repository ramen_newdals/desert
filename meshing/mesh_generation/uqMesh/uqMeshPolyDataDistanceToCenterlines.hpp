/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshPolyDataDistanceToCenterlines.h,v $
  Language:  C++
  Date:      $Date: 2006/04/06 16:46:43 $
  Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataDistanceToCenterlines - calculate the minimum euclidian from surface points to a centerline.
// .SECTION Description
// This function has three distinct ways of working:
// 1) UseRadiusInformation: 1 (default=1) -> Compute the euclidian distance between a surface point and the closest centerline point (based on polyball association).
// 2) EvaluateTubeFunction: 1 (default=0) -> Compute the euclidian distance between a surface point and the center of the tube function (based on polyball line).
// 3) EvaluateCenterlineRadius: 1 (default=0) -> Find the centerline point which is closest to a surface point (similar to method 1), and set distance at that surface point to the radius of the sphere associated with the closest centerline point id.
// By setting ProjectPointArrays: 1 (default=0) -> Project point data from the centerline onto every surface point by linearly interpolating the relative position of the surface point on the line formed by the two closest centerline points.

#ifndef __uqMeshPolyDataDistanceToCenterlines_h
#define __uqMeshPolyDataDistanceToCenterlines_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkCell.h>
#include <vtkMath.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

// Custom headers
#include "uqMeshPolyBallLine.hpp"

class uqMeshPolyDataDistanceToCenterlines : public vtkPolyDataAlgorithm
{
public:
  static uqMeshPolyDataDistanceToCenterlines* New();
  vtkTypeMacro(uqMeshPolyDataDistanceToCenterlines,vtkPolyDataAlgorithm)

  vtkSetObjectMacro(Centerlines,vtkPolyData);
  vtkGetObjectMacro(Centerlines,vtkPolyData);

  vtkSetMacro(UseRadiusInformation,int);
  vtkGetMacro(UseRadiusInformation,int);
  vtkBooleanMacro(UseRadiusInformation,int);

  vtkSetMacro(EvaluateTubeFunction,int);
  vtkGetMacro(EvaluateTubeFunction,int);
  vtkBooleanMacro(EvaluateTubeFunction,int);

  vtkSetMacro(EvaluateCenterlineRadius,int);
  vtkGetMacro(EvaluateCenterlineRadius,int);
  vtkBooleanMacro(EvaluateCenterlineRadius,int);

  vtkSetStringMacro(DistanceToCenterlinesArrayName);
  vtkGetStringMacro(DistanceToCenterlinesArrayName);

  vtkSetStringMacro(CenterlineRadiusArrayName);
  vtkGetStringMacro(CenterlineRadiusArrayName);

  vtkSetMacro(ProjectPointArrays,int);
  vtkGetMacro(ProjectPointArrays,int);
  vtkBooleanMacro(ProjectPointArrays,int);

protected:
  uqMeshPolyDataDistanceToCenterlines();
  ~uqMeshPolyDataDistanceToCenterlines();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

  char* DistanceToCenterlinesArrayName;
  char* CenterlineRadiusArrayName;

  vtkPolyData* Centerlines;

  int UseRadiusInformation;
  int EvaluateTubeFunction;
  int EvaluateCenterlineRadius;
  int ProjectPointArrays;

private:
  uqMeshPolyDataDistanceToCenterlines(const uqMeshPolyDataDistanceToCenterlines&);  // Not implemented.
  void operator=(const uqMeshPolyDataDistanceToCenterlines&);  // Not implemented.
};

#endif

