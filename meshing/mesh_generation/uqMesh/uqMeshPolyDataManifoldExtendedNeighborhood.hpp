/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshPolyDataManifoldExtendedNeighborhood.h,v $
  Language:  C++
  Date:      $Date: 2006/04/06 16:46:44 $
  Version:   $Revision: 1.3 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataManifoldExtendedNeighborhood - Apply a an extended neighborhood to the points of a manifold surface.
// .SECTION Description
// ..

#ifndef __uqMeshPolyDataManifoldExtendedNeighborhood_h
#define __uqMeshPolyDataManifoldExtendedNeighborhood_h

#include <vtkObject.h>
#include <vtkPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkMath.h>

// Custom headers
#include "uqMeshConstants.hpp"
#include "uqMeshPolyDataManifoldNeighborhood.hpp"

class uqMeshPolyDataManifoldExtendedNeighborhood : public uqMeshPolyDataManifoldNeighborhood
{
public:

  static uqMeshPolyDataManifoldExtendedNeighborhood *New();
  vtkTypeMacro(uqMeshPolyDataManifoldExtendedNeighborhood, uqMeshPolyDataManifoldNeighborhood)

  virtual vtkIdType GetItemType() override {return VTK_VMTK_POLYDATA_MANIFOLD_EXTENDED_NEIGHBORHOOD;};

  // Description:
  // Build the neighborhood.
  virtual void Build() override;

protected:
  uqMeshPolyDataManifoldExtendedNeighborhood() {};
  ~uqMeshPolyDataManifoldExtendedNeighborhood() {};

private:
  uqMeshPolyDataManifoldExtendedNeighborhood(const uqMeshPolyDataManifoldExtendedNeighborhood&);  // Not implemented.
  void operator=(const uqMeshPolyDataManifoldExtendedNeighborhood&);  // Not implemented.
};

#endif

