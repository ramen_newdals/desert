/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshPolyDataManifoldStencil.h,v $
  Language:  C++
  Date:      $Date: 2006/04/06 16:46:44 $
  Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataManifoldStencil - Base class for stencils applied to a manifold surface.
// .SECTION Description
// ..

#ifndef __uqMeshPolyDataManifoldStencil_h
#define __uqMeshPolyDataManifoldStencil_h

#include <vtkObject.h>
#include <vtkPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkMath.h>

// Custom Headers
#include "uqMeshPolyDataManifoldExtendedNeighborhood.hpp"
#include "uqMeshPolyDataManifoldNeighborhood.hpp"
#include "uqMeshStencil.hpp"
#include "uqMeshMath.hpp"

class uqMeshPolyDataManifoldStencil : public uqMeshStencil 
{
public:

  vtkTypeMacro(uqMeshPolyDataManifoldStencil,uqMeshStencil)

  vtkGetMacro(Area,double);

  // Description:
  // Build the stencil.
  virtual void Build() override;

  virtual void ComputeArea();
  virtual void ScaleWithArea() = 0;

  void DeepCopy(uqMeshPolyDataManifoldStencil *src);

  vtkGetMacro(UseExtendedNeighborhood,int);
  vtkSetMacro(UseExtendedNeighborhood,int);
  vtkBooleanMacro(UseExtendedNeighborhood,int);
  
protected:
  uqMeshPolyDataManifoldStencil();
  ~uqMeshPolyDataManifoldStencil() {};

  void ScaleWithAreaFactor(double factor);

  double Area;

  int UseExtendedNeighborhood;
  
private:
  uqMeshPolyDataManifoldStencil(const uqMeshPolyDataManifoldStencil&);  // Not implemented.
  void operator=(const uqMeshPolyDataManifoldStencil&);  // Not implemented.
};

#endif

