/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshPolyDataSizingFunction.cxx,v $
Language:  C++
Date:      $Date: 2006/07/17 09:53:14 $
Version:   $Revision: 1.6 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "uqMeshPolyDataSizingFunction.hpp"

vtkStandardNewMacro(uqMeshPolyDataSizingFunction);

uqMeshPolyDataSizingFunction::uqMeshPolyDataSizingFunction(){
	this->SizingFunctionArrayName = NULL;
	this->ScaleFactor = 1.0;
}

uqMeshPolyDataSizingFunction::~uqMeshPolyDataSizingFunction(){
	if(this->SizingFunctionArrayName){
		delete[] this->SizingFunctionArrayName;
		this->SizingFunctionArrayName = NULL;
	}
}

int uqMeshPolyDataSizingFunction::RequestData( 	vtkInformation *vtkNotUsed(request),
																								vtkInformationVector **inputVector,
																								vtkInformationVector *outputVector){
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkPolyData *input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	// Error handeling
	if(input->GetNumberOfPoints() < 1){
		vtkErrorMacro(<<"No Input Points Provides");
		return 1;
	}
	if(!this->SizingFunctionArrayName){
		vtkErrorMacro(<<"No SizingFunctionArrayName specified");
		return 1;
	}
	// BuildCells and BuildLinks needed to speed up accesing GetCell, GetPoint, GetId, etc...
	input->BuildCells();
	input->BuildLinks();

	int NumPoints = input->GetNumberOfPoints();
	vtkDoubleArray* sizingFunctionArray = vtkDoubleArray::New();
	sizingFunctionArray->SetName(this->SizingFunctionArrayName);
	sizingFunctionArray->SetNumberOfTuples(NumPoints);
	sizingFunctionArray->FillComponent(0,0.0);
  
	vtkIdList* PointCells = vtkIdList::New();

	int i, j, NumOfPointCells;
	double AvgArea;
	for(i=0; i<NumPoints; i++){
		input->GetPointCells(i,PointCells);
		NumOfPointCells = PointCells->GetNumberOfIds();
		AvgArea = 0.0;
		if(NumOfPointCells == 0){
			continue;
		}
		for(j=0; j<NumOfPointCells; j++){
			vtkTriangle* triangle = vtkTriangle::SafeDownCast(input->GetCell(PointCells->GetId(j)));
			if(!triangle){
				vtkErrorMacro(<<"Cell" << input->GetCell(PointCells->GetId(j)) << "not triangle: skipping cell for sizing function computation");
			}
			double point0[3], point1[3], point2[3];
			triangle->GetPoints()->GetPoint(0,point0);
			triangle->GetPoints()->GetPoint(1,point1);
			triangle->GetPoints()->GetPoint(2,point2);
			AvgArea += vtkTriangle::TriangleArea(point0,point1,point2);
		}
		AvgArea /= NumOfPointCells;

		double sizingFunction = sqrt(AvgArea) * this->ScaleFactor;
		sizingFunctionArray->SetValue(i,sizingFunction);
	}

	output->DeepCopy(input);
	output->GetPointData()->AddArray(sizingFunctionArray);

	sizingFunctionArray->Delete();
	PointCells->Delete();

	return 1;
}

void uqMeshPolyDataSizingFunction::PrintSelf(std::ostream& os, vtkIndent indent){
	this->Superclass::PrintSelf(os,indent);
}