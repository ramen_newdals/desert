/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshPolyDataSizingFunction.h,v $
Language:  C++
Date:      $Date: 2006/07/17 09:53:14 $
Version:   $Revision: 1.5 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataSizingFunction - constructs a sizing function for volume meshing on the basis of input surface
// .SECTION Description
// ...

#ifndef __uqMeshPolyDataSizingFunction_h
#define __uqMeshPolyDataSizingFunction_h

// VTK Headers
#include <vtkPolyDataAlgorithm.h>
#include <vtkIdList.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

class uqMeshPolyDataSizingFunction : public vtkPolyDataAlgorithm
{
	public: 
	vtkTypeMacro(uqMeshPolyDataSizingFunction,vtkPolyDataAlgorithm)
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	static uqMeshPolyDataSizingFunction *New();
  
	vtkSetStringMacro(SizingFunctionArrayName);
	vtkGetStringMacro(SizingFunctionArrayName);

	vtkSetMacro(ScaleFactor,double);
	vtkGetMacro(ScaleFactor,double);

	protected:
	uqMeshPolyDataSizingFunction();
	~uqMeshPolyDataSizingFunction();  

	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

	char* SizingFunctionArrayName;
	double ScaleFactor;

	private:
	uqMeshPolyDataSizingFunction(const uqMeshPolyDataSizingFunction&);  // Not implemented.
	void operator=(const uqMeshPolyDataSizingFunction&);  // Not implemented.
};

#endif
