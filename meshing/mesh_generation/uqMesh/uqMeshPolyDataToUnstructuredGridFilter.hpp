/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshPolyDataToUnstructuredGridFilter.h,v $
Language:  C++
Date:      $Date: 2006/04/06 16:47:48 $
Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataToUnstructuredGridFilter - create a mesh type object from a surface type object
// .SECTION Description
// ...

#ifndef __uqMeshPolyDataToUnstructuredGridFilter_h
#define __uqMeshPolyDataToUnstructuredGridFilter_h

// VTK Headers
#include <vtkUnstructuredGridAlgorithm.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

class vtkPolyData;

class uqMeshPolyDataToUnstructuredGridFilter : public vtkUnstructuredGridAlgorithm
{
public:
	vtkTypeMacro(uqMeshPolyDataToUnstructuredGridFilter,vtkUnstructuredGridAlgorithm)
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	static uqMeshPolyDataToUnstructuredGridFilter *New();

protected:
	uqMeshPolyDataToUnstructuredGridFilter() {}
	~uqMeshPolyDataToUnstructuredGridFilter() {}

	int FillInputPortInformation(int, vtkInformation *info) override;
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;
  
private:
	uqMeshPolyDataToUnstructuredGridFilter(const uqMeshPolyDataToUnstructuredGridFilter&);  // Not implemented.
	void operator=(const uqMeshPolyDataToUnstructuredGridFilter&);  // Not implemented.
};

#endif


