/*=========================================================================

  Program:   VMTK
  Module:    $RCSfile: uqMeshPolyDataUmbrellaStencil.h,v $
  Language:  C++
  Date:      $Date: 2006/04/06 16:46:44 $
  Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshPolyDataUmbrellaStencil - Weight the neighborhood connections of a surface mesh with a laplacian.
// .SECTION Description
// ..

#ifndef __uqMeshPolyDataUmbrellaStencil_h
#define __uqMeshPolyDataUmbrellaStencil_h

// VTK Headers
#include <vtkObject.h>
#include <vtkObjectFactory.h>

// Custom headers
#include "uqMeshPolyDataManifoldStencil.hpp"
#include "uqMeshConstants.hpp"

class uqMeshPolyDataUmbrellaStencil : public uqMeshPolyDataManifoldStencil
{
public:

  static uqMeshPolyDataUmbrellaStencil *New();
  vtkTypeMacro(uqMeshPolyDataUmbrellaStencil,uqMeshPolyDataManifoldStencil)

  virtual vtkIdType GetItemType() override {return VTK_VMTK_UMBRELLA_STENCIL;};

  void Build() override;

protected:
  uqMeshPolyDataUmbrellaStencil();
  ~uqMeshPolyDataUmbrellaStencil() {};

  void ScaleWithArea() override {};

private:
  uqMeshPolyDataUmbrellaStencil(const uqMeshPolyDataUmbrellaStencil&);  // Not implemented.
  void operator=(const uqMeshPolyDataUmbrellaStencil&);  // Not implemented.
};

#endif

