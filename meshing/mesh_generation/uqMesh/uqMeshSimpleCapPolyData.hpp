/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshSimpleCapPolyData.h,v $
Language:  C++
Date:      $Date: 2006/07/17 09:53:14 $
Version:   $Revision: 1.5 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshSimpleCapPolyData - Add caps to boundaries.
// .SECTION Description
// This class closes the boundaries of a surface with a cap.

#ifndef __uqMeshSimpleCapPolyData_h
#define __uqMeshSimpleCapPolyData_h

// VTK Headers
#include <vtkPolyDataAlgorithm.h>
#include <vtkIdList.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyLine.h>
#include <vtkIdTypeArray.h>
#include <vtkIntArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkVersion.h>

// Custom headers
#include "uqMeshPolyDataBoundaryExtractor.hpp"

class uqMeshSimpleCapPolyData : public vtkPolyDataAlgorithm
{
	public: 
	vtkTypeMacro(uqMeshSimpleCapPolyData,vtkPolyDataAlgorithm)
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	static uqMeshSimpleCapPolyData *New();

	vtkSetObjectMacro(BoundaryIds,vtkIdList);
	vtkGetObjectMacro(BoundaryIds,vtkIdList);
  
	vtkSetStringMacro(CellEntityIdsArrayName);
	vtkGetStringMacro(CellEntityIdsArrayName);

	vtkSetMacro(CellEntityIdOffset,int);
	vtkGetMacro(CellEntityIdOffset,int);

	protected:
	uqMeshSimpleCapPolyData();
	~uqMeshSimpleCapPolyData();  

	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

	vtkIdList* BoundaryIds;
	char* CellEntityIdsArrayName;
	int CellEntityIdOffset;

	private:
	uqMeshSimpleCapPolyData(const uqMeshSimpleCapPolyData&);  // Not implemented.
	void operator=(const uqMeshSimpleCapPolyData&);  // Not implemented.
};

#endif
