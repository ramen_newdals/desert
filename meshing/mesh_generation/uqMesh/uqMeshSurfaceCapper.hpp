/*=========================================================================

  Program:   BSL UQ toolkit
  Module:    uqMeshSurfaceCapper.h

  Copyright (c) Noah Egnatis
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   uqMeshSurfaceCapper
 * @brief   identify and fill holes in mesh
 *
 * vtkFillHolesFilter is a filter that identifies and fills holes in
 * input vtkPolyData meshes. Holes are identified by locating
 * boundary edges, linking them together into loops, and then
 * triangulating the resulting loops. Note that you can specify
 * an approximate limit to the size of the hole that can be filled.
 *
 * @warning
 * Note that any mesh with boundary edges by definition has a
 * topological hole. This even includes a reactangular grid
 * (e.g., the output of vtkPlaneSource). In such situations, if
 * the outer hole is filled, retriangulation of the hole will cause
 * geometric overlap of the mesh. This can be prevented by using
 * the hole size instance variable to prevent the larger holes
 * from being triangulated.
 *
 * @warning
 * Note this filter only operates on polygons and triangle strips.
 * Vertices and polylines are passed through untouched.
 */

#ifndef __uqMeshSurfaceCapper_h
#define __uqMeshSurfaceCapper_h

// vtk data headers
#include <vtkDataObject.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCellIterator.h>
#include <vtkTriangleStrip.h>
#include <vtkPolygon.h>
#include <vtkSphere.h>
#include <vtkDoubleArray.h>
// vtk filter headers
#include <vtkFillHolesFilter.h>
#include <vtkConnectivityFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkXMLPolyDataReader.h>
// vtk filter programing headers
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkSmartPointer.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkPolyDataAlgorithm.h>
#include <vtkFiltersModelingModule.h>

class uqMeshSurfaceCapper : public vtkPolyDataAlgorithm
{
public:
  ///@{
  /**
   * Standard methods for instantiation, type information and printing.
   */
  vtkTypeMacro(uqMeshSurfaceCapper, vtkPolyDataAlgorithm)
  void PrintSelf(ostream& os, vtkIndent indent) override;
  ///@}

	///@{
  /**
   * Specify the maximum hole size to fill. This is represented as a radius
   * to the bounding circumsphere containing the hole.  Note that this is an
   * approximate area; the actual area cannot be computed without first
   * triangulating the hole.
   */
  vtkSetClampMacro(HoleSize, double, 0.0, VTK_FLOAT_MAX);
  vtkGetMacro(HoleSize, double);
	///@}

  static uqMeshSurfaceCapper* New();

  /// @brief cap the input poly-data and tag the new cells
  /// @param hole_size Size of holes to detect
  int capSurface(vtkPolyData* input, vtkPolyData* output, double hole_size);

  void getNormals();
  void get_cap_cells();
	void update_connectivity();
	void output_capped_mesh(vtkPolyData *output_mesh);
	void printSurfaceData();

protected:
  uqMeshSurfaceCapper();
  ~uqMeshSurfaceCapper();

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*);

	double HoleSize;

private:
  uqMeshSurfaceCapper(const uqMeshSurfaceCapper&); // Not implemented.
  void operator=(const uqMeshSurfaceCapper&);        // Not implemented.
  	/* data */
	vtkNew<vtkXMLPolyDataReader> input_mesh;
	vtkNew<vtkFillHolesFilter> fillHoles;
	vtkNew<vtkConnectivityFilter> connectivity;
	vtkNew<vtkPolyDataNormals> normals;
	vtkNew<vtkPolyData> hole_poly_data;
};

#endif
