/*=========================================================================

Program:   VMTK
Module:    $RCSfile: uqMeshSurfaceProjection.h,v $
Language:  C++
Date:      $Date: 2006/04/06 16:47:48 $
Version:   $Revision: 1.4 $

  Copyright (c) Luca Antiga, David Steinman. All rights reserved.
  See LICENSE file for details.

  Portions of this code are covered under the VTK copyright.
  See VTKCopyright.txt or http://www.kitware.com/VTKCopyright.htm 
  for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
// .NAME uqMeshSurfaceProjection - project point data from a reference surface onto an input surface.
// .SECTION Description
// .

#ifndef __uqMeshSurfaceProjection_h
#define __uqMeshSurfaceProjection_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPolyData.h>
#include <vtkCellLocator.h>
#include <vtkDoubleArray.h>
#include <vtkGenericCell.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkMath.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>

class vtkPolyData;

class uqMeshSurfaceProjection : public vtkPolyDataAlgorithm
{
	public: 
	vtkTypeMacro(uqMeshSurfaceProjection,vtkPolyDataAlgorithm)
	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	static uqMeshSurfaceProjection *New();

	// Description:
	// Set/Get the reference surface to compute distance from.
	vtkSetObjectMacro(ReferenceSurface,vtkPolyData);
	vtkGetObjectMacro(ReferenceSurface,vtkPolyData);

	protected:
	uqMeshSurfaceProjection();
	~uqMeshSurfaceProjection();  

	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

	vtkPolyData *ReferenceSurface;

	private:
	uqMeshSurfaceProjection(const uqMeshSurfaceProjection&);  // Not implemented.
	void operator=(const uqMeshSurfaceProjection&);  // Not implemented.
};

#endif
