#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

struct CSVRow {
    // std::string name;
    long id;
    // Add more fields as needed
};

std::vector<CSVRow> readCSV(const std::string& filename) {
    std::vector<CSVRow> data;
    std::ifstream file(filename);

    if (!file.is_open()) {
        std::cerr << "Failed to open file: " << filename << std::endl;
        return data; // Return an empty vector in case of failure
    }

    std::string line;
    bool isFirstLine = true; // To skip the header line

    while (std::getline(file, line)) {
        if (isFirstLine) {
            isFirstLine = false;
            continue; // Skip the header line
        }

        std::stringstream ss(line);
        CSVRow row;
        std::string field;

        // Assuming the CSV has two fields: name (string) and age (integer)
        // if (std::getline(ss, field, ',')) {
        //     row.name = field;
        // }

        if (std::getline(ss, field, ',')) {
            try {
                row.id = std::stoi(field);
            } catch (const std::exception& e) {
                std::cerr << "Error parsing age: " << e.what() << std::endl;
                continue; // Skip this line if age cannot be parsed
            }
        }

        data.push_back(row);
    }

    file.close();
    return data;
}

int main() {
    std::string filename = "sac_points.csv";
    std::vector<CSVRow> data = readCSV(filename);

    for (const CSVRow& row : data) {
        std::cout << "Id: " << row.id << std::endl;
    }

    return 0;
}

// Function to write data to a CSV file at a specific row and column
template<typename T>
void writeToCSV(const std::string& filename, int row, int col, const T& data) {
    std::fstream file(filename, std::ios::in | std::ios::out);
    if (!file) {
        std::cerr << "Error: File not found or unable to open." << std::endl;
        return;
    }

    std::vector<std::vector<std::string>> rows;
    std::string line;

    // Read the existing data from the CSV file
    while (std::getline(file, line)) {
        std::vector<std::string> row;
        size_t start = 0, end = 0;
        
        while ((end = line.find(',', start)) != std::string::npos) {
            row.push_back(line.substr(start, end - start));
            start = end + 1;
        }
        
        row.push_back(line.substr(start));
        rows.push_back(row);
    }

    // Update the specific cell with the new data
    if (row >= 0 && row < rows.size() && col >= 0) {
        if (col >= rows[row].size()) {
            rows[row].resize(col + 1);
        }
        rows[row][col] = std::to_string(data);
    } else {
        std::cerr << "Error: Invalid row or column index." << std::endl;
        return;
    }

    // Write the updated data back to the CSV file
    file.close();
    file.open(filename, std::ios::out | std::ios::trunc);

    for (const auto& row : rows) {
        for (size_t i = 0; i < row.size(); ++i) {
            file << row[i];
            if (i < row.size() - 1) {
                file << ',';
            }
        }
        file << '\n';
    }

    std::cout << "Data written to row " << row << ", column " << col << ": " << data << std::endl;
    file.close();
}