#ifndef __CSVReader
#define __CSVReader
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

enum dataType{
  
}

class CSVReader
{
private:
  /* data */
public:
  CSVReader(/* args */);
  ~CSVReader();
};

CSVReader::CSVReader(/* args */)
{
}

CSVReader::~CSVReader()
{
}

template<typename T>
void writeToCSV(const std::string& filename, int row, int col, const T& data);