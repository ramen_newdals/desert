import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader'
import Stats from 'three/examples/jsm/libs/stats.module'

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera
	( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const controls = new OrbitControls(camera, renderer.domElement)
controls.enableDamping = true

/* TESTING CUBES!*/
const geometry_0 = new THREE.BoxGeometry( 1, 1, 1 );
const material_0 = new THREE.MeshBasicMaterial( { color: 0x00ff69 } );
const cube_0 = new THREE.Mesh( geometry_0, material_0 );


const geometry_1 = new THREE.BoxGeometry( 6, 1, 1 );
const material_1 = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
const cube_1 = new THREE.Mesh( geometry_1, material_1 );

const geometry_2 = new THREE.BoxGeometry( 1, 8, 1 );
const material_2 = new THREE.MeshBasicMaterial( { color: 0x00bebe } );
const cube_2 = new THREE.Mesh( geometry_2, material_2 );

const geometry_3 = new THREE.BoxGeometry( 1, 4, 1 );
const material_3 = new THREE.MeshBasicMaterial( { color: 0x00cafe } );
const cube_3 = new THREE.Mesh( geometry_3, material_3 );
// // Adding the THREE.Mesh objects to the scene
scene.add( cube_0 );
scene.add( cube_1 );
scene.add( cube_2 );
scene.add( cube_3 );

camera.position.z = 5;
/* TESTING CUBES!*/

// loading .stl Aneurysm input files
const loader = new STLLoader();
loader.load('./public/model.stl', function ( geometry ) {
	const mesh = new THREE.Mesh( geometry, material_0 );
	scene.add( mesh )}, undefined, function(error) {
		console.log(error);});

window.addEventListener('resize', onWindowResize, false)
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

const stats = new Stats()
document.body.appendChild(stats.dom)

// Set the default position of the camera
camera.position.set(16.69, 21.420, 79.187);

controls.update();

function animate() {
	requestAnimationFrame( animate );

	controls.update()

	cube_0.rotation.x += 0.0420;
	cube_1.rotation.y += 0.069;
	cube_2.rotation.x += 0.02;
	cube_3.rotation.z += 0.05;

	render( scene, camera );

	stats.update()
}

function render() {
	renderer.render(scene, camera)
}

animate();