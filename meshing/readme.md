# Meshing Procedure

## Meshing Procedure
Meshing of an anuerysm case follows the following procedure

- surface = read_polydata(input_model(filename)) --> Load the given file and return vtkPolyData object
- is_surface_capped(surface) --> Checks if the surface is capped and returns True/False and the number of holes
- vmtk_cap_polydata(surface, )
- get_centers_for_meshing()
- compute_centerlines()
- get_centerline_tolerance()
- get_regions_to_refine()
- vmtk_compute_voroni_diagram()
- smooth_voroni_diagram()
- write_polydata()
- create_new_surface()
- prepare_output_surface()
- compute_centers()
- vmtk_smooth_surface()
- add_flow_extension()
- vmtk_cap_polydata()
- read_polydata()
- dist_sphere_constant()
- dist_sphere_curvature()
- dist_sphere_diam()
- generate_mesh()
- write_mesh()