# Original program source from VMTK SurfaceConversion Script
##   Copyright (c) Luca Antiga, David Steinman. All rights reserved.
#
# Modified by Noah Egnatis July,28th,2023
import vtk
import sys

class SurfaceWriter():
	def __init__(self) -> None:
		self.InputFileName = ''
		self.OutputFileName = ''
		self.Surface = None
		self.Mode = "binary"  
		pass
    
	def PrintLog(LogMessage):
		print(LogMessage)
    
	def WriteTecplotSurfaceFile(self):
		if(self.OutputFileName == ''):
			self.PrintError('Error: no OutputFileName.')
		self.PrintLog('Writing Tecplot file.')
		triangleFilter = vtk.vtkTriangleFilter()
		triangleFilter.SetInputData(self.Surface)
		triangleFilter.PassVertsOff()
		triangleFilter.PassLinesOff()
		triangleFilter.Update()
		self.Surface = triangleFilter.GetOutput()
		f=open(self.OutputFileName, 'w')
		line = "VARIABLES = X,Y,Z"
		arrayNames = []
		for i in range(self.Surface.GetPointData().GetNumberOfArrays()):
			array = self.Surface.GetPointData().GetArray(i)
			arrayName = array.GetName()
			if arrayName == None:
				continue
			if (arrayName[-1]=='_'):
				continue
			arrayNames.append(arrayName)
			if(array.GetNumberOfComponents() == 1):
				line = line + ',' + arrayName
			else:
				for j in range(array.GetNumberOfComponents()):
					line = line + ',' + arrayName + str(j)
		line = line + '\n'
		f.write(line)
		line = "ZONE " + "N=" + str(self.Surface.GetNumberOfPoints()) + ',' + "E=" + str(self.Surface.GetNumberOfCells()) + ',' + "F=FEPOINT" + ',' + "ET=TRIANGLE" + '\n'
		f.write(line)
		for i in range(self.Surface.GetNumberOfPoints()):
			point = self.Surface.GetPoint(i)
			line = str(point[0]) + ' ' + str(point[1]) + ' ' + str(point[2])
			for arrayName in arrayNames:
				array = self.Surface.GetPointData().GetArray(arrayName)
				for j in range(array.GetNumberOfComponents()):
					line = line + ' ' + str(array.GetComponent(i,j))
				line = line + '\n'
				f.write(line)
		for i in range(self.Surface.GetNumberOfCells()):
			cellPointIds = self.Surface.GetCell(i).GetPointIds()
			line = ''
			for j in range(cellPointIds.GetNumberOfIds()):
				if (j>0):
					line = line + ' '
				line = line + str(cellPointIds.GetId(j)+1)
				line = line + '\n'
				f.write(line)
            
if __name__ == '__main__':
	args = sys.argv # Arg 1 is file name to be converted arg2 is format to convert to
	if len(sys.argv) == 1:
		print("Usage is SurfaceConversion InputFileName OutputFileName")
		exit()
	conversion = SurfaceWriter
	conversion.InputFileName = sys.argv[1]
	conversion.OutputFileName = sys.argv[2]
	reader = vtk.vtkXMLPolyDataReader()
	reader.SetFileName(conversion.InputFileName)
	reader.Update()
	conversion.Surface = reader.GetOutput()
	conversion.WriteTecplotSurfaceFile(conversion)
