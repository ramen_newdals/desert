1. Read in input data (.h5 wss, .h5 cell normals)
2. Compute the following indicies (In the specified order):
	- TAWSS (Time averaged WSS, compoenent by componenet np.mean for each)
	- TAWSSVM (The vector vector magnitude of all compoeonts of the TAWSS)
	- TAWSSV (Normalized TAWSS componenets)
	- Trans_WSS_term_1 (cross(normal, TAWSSV))
	- Trans_WSS_temp (sum(timesteps, abs(dot((wss_1, wss_2, wss_2), Trans_WSS_term_1))))
	- Trans_WSS = Trans_WSS_temp/num_time_steps
	- OSI (0.5*(1-TAWSSVM/TAWSS))
	- SPI (fft(U)->power@25[Hz]/power@0[Hz], return 0 if power@0[Hz]==0)
	- SPI_a (fft(U-np.mean(U))->power@25[Hz]/power@0[Hz], return 0 if power@0[Hz]==0)
	- wss_proj_on_tawss (dot(TAWSS, WSS)/TAWSSVM)
	- SPI_p (ffT(wss_prof_on_tawss))