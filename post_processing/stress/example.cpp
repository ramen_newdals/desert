#include <string>
#include <vector>
#include <CL/cl.h>
#include <iostream>
#include "hdf5.h"
#include "H5Cpp.h"
#include "read_mesh.hpp"
#define H5FILE_NAME "/home/ramen_newdals/Documents/BSL/oldCFDPipeline/sampleData/p302/temp/mesh.h5"

int main(void)
{
    hid_t file;
    file = H5Fopen(H5FILE_NAME, H5F_ACC_RDONLY, H5P_DEFAULT);
    H5Fclose(file);

    cl_int CL_err = CL_SUCCESS;
    cl_uint numPlatforms = 0;

    CL_err = clGetPlatformIDs( 0, NULL, &numPlatforms );

    if(CL_err == CL_SUCCESS)
        printf("%u platform(s) found\n", numPlatforms);
    else
        printf("clGetPlatformIDs(%i)\n", CL_err);	
		std::vector<float> cells;
		std::string file_path {"/home/ramen_newdals/Documents/BSL/aneurisk-uq/c0003/hf_results/art_c0003_ICA_T_I8_ICA_V27_FC_ICA_Q321_Per951_Newt370_ts10000_cy2_uO1/c0003_ICA_T.h5"};
		std::string group_name {"Mesh"};
		std::string dataset_name {"topology"};
		cells = read_component(file_path, group_name, dataset_name);
    return 0;
}
