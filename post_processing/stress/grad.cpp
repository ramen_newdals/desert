#include "grad.hpp"
#include <iostream>
#include <cstdlib>
#include <memory>

int teta_jacobi_inv(double **inverse, double *derivs) {
  return 69;
}

void tetra_calc_deriv(int dim, double* values, double* derivs)
{
	double *jI[3], j0[3], j1[3], j2[3];
  double functionDerivs[12], sum[3], value;
  int i, j, k;

  // compute inverse Jacobian and interpolation function derivatives
  jI[0] = j0;
  jI[1] = j1;
  jI[2] = j2;
  teta_jacobi_inv(jI, functionDerivs);

  // now compute derivates of values provided
  for (k = 0; k < dim; k++) {// loop over values per point
    sum[0] = sum[1] = sum[2] = 0.0;
    for (i = 0; i < 4; i++) {// loop over interp. function derivatives
      value = values[dim * i + k];
      sum[0] += functionDerivs[i] * value;
      sum[1] += functionDerivs[4 + i] * value;
      sum[2] += functionDerivs[8 + i] * value;
    }

    for (j = 0; j < 3; j++) // loop over derivative directions
    {
      derivs[3 * k + j] = sum[0] * jI[j][0] + sum[1] * jI[j][1] + sum[2] * jI[j][2];
    }
  }
}

void init_mesh(test_mesh* mesh) {
	// Make a single tetra-heydron to test
	mesh->num_nodes = 1;
  mesh->node_ids = reinterpret_cast<int*>
                    (std::malloc(mesh->num_nodes*sizeof(int)));
  mesh->nodes_x = reinterpret_cast<float*>
                    (std::malloc(mesh->num_nodes*sizeof(float)));
  mesh->nodes_y = reinterpret_cast<float*>
                    (std::malloc(mesh->num_nodes*sizeof(float)));
  mesh->nodes_z = reinterpret_cast<float*>
                    (std::malloc(mesh->num_nodes*sizeof(float)));

  mesh->node_ids[0] = 10;
  mesh->nodes_x[0] = 10;
  mesh->nodes_y[0] = 6;
  mesh->nodes_z[0] = 1;

  std::cout << "DEBUG_2" << std::endl;
}
void test_tetra_mesh(tetra_mesh& mesh) {
	// Make a single tetra-heydron to test
	mesh.num_cells = 1;
  std::cout << "DEBUG_2" << std::endl;
	mesh.num_nodes = 4;

  mesh.node_ids = reinterpret_cast<int*>
                    (std::malloc(mesh.num_nodes*sizeof(int)));
	mesh.nodes_x = reinterpret_cast<float*>
                    (std::malloc(mesh.num_nodes*sizeof(float)));
	mesh.nodes_y = reinterpret_cast<float*>
                    (std::malloc(mesh.num_nodes*sizeof(float)));
	mesh.nodes_z = reinterpret_cast<float*>
                    (std::malloc(mesh.num_nodes*sizeof(float)));

  // Set the node_ids

  // Assigin the unit tetra cords for "random" point ordering
  // X Points
  mesh.nodes_x[0] = 0; mesh.nodes_x[1] = 1; mesh.nodes_x[2] = 0; mesh.nodes_x[3] = 0;

  // Y Points
  mesh.nodes_y[0] = 0; mesh.nodes_y[1] = 1; mesh.nodes_y[2] = 8; mesh.nodes_y[3] = 7;

  // Z Points
  mesh.nodes_z[0] = 4; mesh.nodes_z[1] = 2; mesh.nodes_z[2] = 0; mesh.nodes_z[3] = 0;
}

void delete_tetra_mesh(tetra_mesh& mesh) {
  // Free all of the dynamic memory needed for the mesh
  std::free(mesh.node_ids);
  std::free(mesh.nodes_x);
  std::free(mesh.nodes_y);
  std::free(mesh.nodes_z);
}

void print_tetra_mesh(tetra_mesh& mesh) {
	// Print out all of the nodes
  std::cout << "=========== Printing Mesh Info" << " ===========" << std::endl;  
  std::cout << "Number of Nodes: " << mesh.num_nodes << std::endl;
  if(true) {
    std::cout << "Node Cords are" << std::endl;
    for(long i = 0; i < mesh.num_nodes ; i++) {
      std::cout << "   x_" << i << " =  " << mesh.nodes_x[i] << ", "
                << "y_" << i << " = " << mesh.nodes_y[i] << ", "
                << "z_" << i << " = " << mesh.nodes_z[i] << ", "
                << std::endl;
    }
  }
}

void test(A* input) {
  input->test = 10;
  input->yeah = 5;
}