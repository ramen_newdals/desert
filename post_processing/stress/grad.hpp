#ifndef __grad_h
#define __grad_h

int teta_jacobi_inv(double *inverse, double* derivs);
void tetra_calc_deriv(int dim, double* values, double* derivs);

struct test_mesh
{
	int num_nodes;
	int* node_ids;
	float* nodes_x, *nodes_y, *nodes_z;
};

struct tetra_mesh
{
	int num_nodes;
	int *node_ids;

	int num_cells;
	int* cell_ids;
	int* cell_node_connectivity;

	float *nodes_x;
	float *nodes_y;
	float *nodes_z;
};

struct A
{
	int test;
	int yeah;
};

void test(A* input);

void tetra_mesh_point_grad(tetra_mesh* mesh);

void init_mesh(test_mesh* mesh);

void test_tetra_mesh(tetra_mesh& mesh);

void delete_tetra_mesh(tetra_mesh* mesh);

void print_tetra_mesh(tetra_mesh* mesh);

#endif
