#include <string>
#include <vector>
#include <algorithm>
#include <numbers>
#include "hdf5.h"
#include "H5Cpp.h"

#ifndef __read_mesh_h
#define __read_mesh_h



std::vector<float> read_component
	(std::string file_path, std::string group_name, std::string dataset_name) 
{
	H5::H5File file(file_path.c_str(), H5F_ACC_RDONLY);
	hsize_t dims[2];
	dims[0] = 3591533;
	dims[1] = 4;

	H5::DataSpace dataspace = H5::DataSpace(0, dims);
	H5::Group solution;
	H5::DataSet dataset;

	int* cells = new int[3591533*4];

	file.openFile(file_path, H5F_ACC_RDONLY);
	solution = file.openGroup(group_name);
	dataset = solution.openDataSet(dataset_name);
	dataset.read(cells, H5T_NATIVE_INT);

	dataset.close();
	solution.close();
	dataspace.close();
	file.close();

	std::vector<float> output;

	// Read in all cells to the cells_vector
	for(int i = 0; i<3591533; i++) {
		std::cout << cells[i*4] << ", " << cells[(i*4)+1] << ", " << cells[(i*4)+2]
							<< cells[(i*4)+3] << std::endl;
	}
	return output;
}

class meshReader {
public:
    std::string file_name {"/home/ramen_newdals/Documents/BSL/stress/mesh.h5"};
    std::string group_name {"Mesh"};
    std::string verticies_dataset {"coordinates"};
    std::string cells_dataset {"topology"};

    std::string file_name_velocity {"/home/ramen_newdals/Documents/BSL/stress/solutionData.h5"};
    std::string group_name_velocity {"Solution"};
    std::string velocity_dataset {"u"};

    std::vector<std::vector<int>> cell_vector;
    std::vector<std::vector<double>> verticie_vector;
    std::vector<std::vector<double>> velocity_vector;
    std::vector<std::vector<double>> grad_velocity;

    int RANK, verticies_dim0, verticies_dim1, cells_dim0, cells_dim1;
    double *verticies;
    double *velocity;
    int *cells;

    meshReader(std::string input_file_path) {
        RANK = 0;
        verticies_dim0 = 216955;
        verticies_dim1 = 3;
        cells_dim0 = 1262194, 
        cells_dim1 = 4;
        verticies = new double[verticies_dim0*verticies_dim1];
        velocity = new double[verticies_dim0*verticies_dim1];
        cells = new int[cells_dim0*cells_dim1];

    };
    ~meshReader() {
        delete velocity;
        delete verticies;
        delete cells;
    };

    std::vector<double> make_verticie(double x, double y, double z) {
        std::vector<double> verticie;
        verticie.push_back(x);
        verticie.push_back(y);
        verticie.push_back(z);
        return verticie;
    }

    std::vector<int> make_cell(int v0, int v1, int v2, int v3) {
        std::vector<int> cell;
        cell.push_back(v0);
        cell.push_back(v1);
        cell.push_back(v2);
        cell.push_back(v3);
        return cell;
    }

    int readVerticies()
    {
        H5::H5File file(file_name, H5F_ACC_RDONLY);
        hsize_t dims[2];
        dims[0] = verticies_dim0;
        dims[1] = verticies_dim1;

        H5::DataSpace dataspace = H5::DataSpace(RANK, dims);
        H5::Group solution;
        H5::DataSet dataset;

        file.openFile(file_name, H5F_ACC_RDONLY);
        solution = file.openGroup(group_name);
        dataset = solution.openDataSet(verticies_dataset);
        dataset.read(verticies, H5T_NATIVE_DOUBLE);
        
        dataset.close();
        solution.close();
        dataspace.close();
        file.close();

        // Read in all verticies to verticie_vector
        for(int i = 0; i<verticies_dim0; i++)
        {verticie_vector.push_back(make_verticie(verticies[(i*3)], verticies[(i*3)+1],verticies[(i*3)+2]));}
        return 0;
    }

    int readCells()
    {
        H5::H5File file(file_name, H5F_ACC_RDONLY);
        hsize_t dims[2];
        dims[0] = cells_dim0;
        dims[1] = cells_dim1;

        H5::DataSpace dataspace = H5::DataSpace(RANK, dims);
        H5::Group solution;
        H5::DataSet dataset;

        file.openFile(file_name, H5F_ACC_RDONLY);
        solution = file.openGroup(group_name);
        dataset = solution.openDataSet(cells_dataset);
        dataset.read(cells, H5T_NATIVE_INT);
        
        dataset.close();
        solution.close();
        dataspace.close();
        file.close();

        // Read in all cells to the cells_vector
        for(int i = 0; i<cells_dim0; i++)
        {cell_vector.push_back(make_cell(cells[(i*4)], cells[(i*4)+1], cells[(i*4)+2], cells[(i*4)+3]));}
        return 0;
    }

    int readVelocity()
    {   
        H5::H5File file(file_name_velocity, H5F_ACC_RDONLY);
        hsize_t dims[2];
        dims[0] = verticies_dim0;
        dims[1] = verticies_dim1;

        H5::DataSpace dataspace = H5::DataSpace(RANK, dims);
        H5::Group solution;
        H5::DataSet dataset;

        file.openFile(file_name_velocity, H5F_ACC_RDONLY);
        solution = file.openGroup(group_name_velocity);
        dataset = solution.openDataSet(velocity_dataset);
        dataset.read(velocity, H5T_NATIVE_DOUBLE);
        
        dataset.close();
        solution.close();
        dataspace.close();
        file.close();

        // Read in all nodal velocities to velocity_vector
        for(int i = 0; i<verticies_dim0; i++)
        {velocity_vector.push_back(make_verticie(velocity[(i*3)], velocity[(i*3)+1],velocity[(i*3)+2]));}
        return 0;
    }

    std::vector<int> getVetexConnectivity(int vertexNum)
    {
        /* 
        For each vertex go throuhg every cell and find what cells contain this vertex
        if the cell has the vertex it is one of its neightboors, add all of the verticies to
        the vertex connectivity vector such that there are not duplicates
        */
        std::vector<std::vector<int>> vertexConnectivity;
        std::vector<int> connectivityVerticies;
        std::vector<int> connectivityCells;
        connectivityVerticies.push_back(vertexNum);
        // Find all cells that contain a common vertex
        int i, j;
        for(i = 0; i<cells_dim0; i++)
        {
            for(j = 0; j<4; j++)
            {
                if(cell_vector[i][j] == vertexNum)
                {
                    connectivityCells.push_back(i);
                }
            }
        }
        // Using the cells that have a common vertex add all nodes of these cells
        // to the vertex connectivity map
        for(i = 0; i<connectivityCells.size(); i++)
        {
            for(j = 0; j<4; j++)
            {
                if(cell_vector[connectivityCells[i]][j] != vertexNum)
                {
                    connectivityVerticies.push_back(cell_vector[connectivityCells[i]][j]);
                }
            }
        }
        // Remove duplicate verticies from the connectivity list
        std::sort(connectivityVerticies.begin(), connectivityVerticies.end()); 
        auto last = std::unique(connectivityVerticies.begin(), connectivityVerticies.end());
        connectivityVerticies.erase(last, connectivityVerticies.end());
        // for (const auto& i : connectivityVerticies) std::cout << i << " ";
        // std::cout << std::endl;
        return connectivityVerticies;
    }
};

#endif