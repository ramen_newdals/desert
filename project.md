# Desert 
This repository acts as my mono-repo for all work im doing durring my MASc at UofT in the biomedial simulations lab

It has the following core componenets
- Meshing
- Pre processing
- FEM Solver
- Post processing
- Visulization

This project is managed using my own private runners for orchistration of jobs and data movment to securre location aswell as managing theingress and exgress points for using compute canada resources, so to get the CI/CD working might not be push button for other labs, but please feel free to submit an issue on the porject page and ill do my best to help

# Deployment
The project is built for the native architexture of compute canada Niagara and for my private runners CPU's (Private dont ask model for security reasons), this build contains all of the nessecary project binaries that will perform hte following tasks

## HPC Deployment
- Generate computational meshes of varying fideliy bassed on input heuristic file that comply with the FENICS mesh format
- Generate the appropriate ufcx compliant C++ files using ffcx
- Generate system marticices using the generated ufcx functions
- Solve these system matricies producing temporary data in memory/ssd/hdd cached information
- Use the temporary data to compute relevant quantities of interset (QOIS)
- Move the temporary data from high-spped cache location to temporary bulk storage
- Upon program completion (or at specified interval) move files to long term storage and  purge temporary bulk stoarage 
- Clean up all reminants of the program running except for log files that will be stored in the on-server long term storage

## End User Deployment
- user provides nessecary information to access HPC resources and they are stored in a secure database
- user logs in with relevant credentials and gains access to study scheudling tool
- specify the desired cases to be analysed or upload new case data






## Oct, 11th notes,

- Does maintinng a thicker boundary layer proportional to element size make a real difference?
	- Same ratio and 4 elements, but the total thickness of BL should be proportional to
	the volume element charecteristic EL
- Thiner BL is beceuase the flow will have thiner shear layer so the layer should
be thinner for smaler EL
- Check dans directories for the camera point and camera paramaters

## Goals by Oct, 31st
- Need to workout QOI and visulization pipeline push button as part of this study
