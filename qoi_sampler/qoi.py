from scipy.fft import fft, fftfreq
import h5py
import numpy as np
import pyvista
import matplotlib.pyplot as plt

def read_mesh(fileName):
	# FIX: hardcoded dataset names sucks, but its nessecary
	verticies_path = 'Mesh/mesh/geometry'
	cells_path = 'Mesh/mesh/topology'
	
	file = h5py.File(fileName, "r")
	verticies = file[verticies_path]
	cells =file[cells_path]
	verticies = np.array(verticies)
	verticies = np.append(verticies, np.zeros((np.shape(verticies)[0], 1)), axis=1)
	cells = np.array(cells)
	del file
	return verticies, cells;

def read_velocity(fileName, timestep):
	velocity_path = 'Function/u'
	file = h5py.File(fileName, "r")
	timesteps = np.array(file[velocity_path])
	velocity = np.array(file[velocity_path+'/'+str(timesteps[timestep])])
	del file
	return velocity

def sample_interval(int_start, int_end, num_intervals, num_samples, dist):
	# use numpy to sample random value zeta in the interval int_start + i*(dx) for num_intervals/num_samples
	dx = int_end - int_start
	sample_int_start = 0
	m = int(num_intervals/num_samples) # m is the number of samples per interval
	samples = np.array((m, num_intervals))
	for i in range(num_intervals):
		for j in range(m):
			# sample uniform dist in the region that
			samples[i][j] = np.random


# verticies, cells = read_mesh("results/0.21_0.8_0.005.h5")
# celltypes = np.ones((np.shape(cells)[0], 1))*9

# velocity = read_velocity("results/0.21_0.8_0.005.h5", 150)
# velocity = np.sqrt(np.sum(np.power(velocity, 2), axis=1))

# velocity_f = fft(velocity)
# x_f = fftfreq(np.shape(velocity)[0])[:(np.shape(velocity)[0])//2]
# plt.plot(x_f, 2/(np.shape(velocity)[0])*np.abs(velocity_f[0:(np.shape(velocity)[0])//2]))
# plt.grid()
# plt.show()