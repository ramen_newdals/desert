import sys
import os
import subprocess as sp

def sample_qoi(u_max, cylinder_height, mesh_resolution):
    sp.run(['python3 generateMesh.py', str(u_max), str(cylinder_height), str(mesh_resolution)])

sample_qoi(0.7, 0.21, 0.1)