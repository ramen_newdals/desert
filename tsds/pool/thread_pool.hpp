#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <iostream>
#include <functional>

#ifndef __thread_pool_h
#define __thread_pool_

class ThreadPool {
public:
    void Start();
    void QueueJob(const std::function<void()>& job);
    void Stop();
    bool Busy();

private:
    void ThreadLoop();

    bool should_terminate = false;           // Tells threads to stop looking for jobs
    std::mutex queue_mutex;                  // Prevents data races to the job queue
    std::condition_variable mutex_condition; // Allows threads to wait on new jobs or termination 
    std::vector<std::thread> threads;
    std::queue<std::function<void()>> jobs;
};

void ThreadPool::Start() {
	const uint32_t num_threads = std::thread::hardware_concurrency();
	for(uint32_t ii = 0; ii<num_threads; ii++) {
		threads.emplace_back(std::thread(&ThreadPool::ThreadLoop, this));
	}
}

void ThreadPool::ThreadLoop() {
	while(true) {
		std::function<void()> job;
		{
			std::unique_lock<std::mutex> lock(queue_mutex);
			mutex_condition.wait(lock, [this] {
				return !jobs.empty() || should_terminate;
			});
			if (should_terminate) {
				return;
			}
			job = jobs.front();
			jobs.pop();
		}
		job();
	}
}

void ThreadPool::QueueJob(const std::function<void()>& job) {
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		jobs.push(job);
	}
	mutex_condition.notify_one();
}

bool ThreadPool::Busy() {
	bool pool_busy;
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		pool_busy = !jobs.empty();
	}
	return pool_busy;
}

void ThreadPool::Stop() {
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		should_terminate = true;
	}
	mutex_condition.notify_all();
	for(std::thread& activ_thread : threads) {
		activ_thread.join();
	}
	threads.clear();
}
#endif
