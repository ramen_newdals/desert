# TSDS (Thread Safe Data Structures)
- This is just a directory to contain all of the data structures that were 
reimlemented for C++ beceuase they needed to be thread safe
- Theses structutres do there best to be as genreal purpuse and templated such 
that there re-use throught the whole project is maximized

## TS Queue:
- Simple thread-safe queue that is typically used in the context of the thread 
pool where each of the worker threads will pop and item from the queue and then 
perform the work that the queue prescribes throuhg a function pointer
- This implementaion is bassed on the one proposed in C++ concurancy in action 