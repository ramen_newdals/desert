#include "ts_queue.hpp"
#include <iostream>
#include <functional>
#include <thread>
#include "thread_pool.hpp"
#include <Eigen/SparseCore>

void cg() {

}

int main()
{
	std::vector<double> hello;
	ts_queue<std::string> test;
	std::cout << test.get_queue_size() << std::endl; 
	test.push("We are on the beam");
	test.push("Hell yeah brother");
	std::cout << test.get_queue_size() << std::endl;
	std::string value;
	while(test.try_pop(value))
	{
		std::cout << value << std::endl;
	}	

	std::cout << "===========TESTING THREAD POOL===========" << std::endl;
	ThreadPool thread_pool_test;
	thread_pool_test.Start();
	thread_pool_test.QueueJob([] {
		std::cout << "Thread Says Hello!" << std::endl;});
	thread_pool_test.QueueJob([] {
		std::cout << "Thread Says BOO!" << std::endl;});
	while(thread_pool_test.Busy()) {};
	thread_pool_test.Stop();

	return 0;
}